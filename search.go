package api

// SearchService is
type SearchService interface {
	AddCompany(c *SearchCompany) error
	UpdateCompany(c *SearchCompany) error
	AddProfessional(c *SearchProfessional) error
	AddCustomer(c *SearchCustomer) error
	UpdateCustomer(c *SearchCustomer) error
	RemoveCompanyCustomer(customerID, companyID string) error
	AddCompanyCustomer(customerID, companyID string) error
	UpdateProfessionalServices(id string) error
}

// SearchCompany is
type SearchCompany struct {
	ID       string
	Name     string
	Address  string
	CoverURL string
}

// SearchProfessional is
type SearchProfessional struct {
	ID        string
	CompanyID string
	FirstName string
	LastName  string
	Services  []*Service
}

// SearchCustomer is
type SearchCustomer struct {
	ID             string
	CompanyID      string
	FirstName      string
	LastName       string
	Email          string
	PhoneNumber    string
	ProfessionalID string
}
