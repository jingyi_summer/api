package bitly

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"
)

//Response is
type Response struct {
	StatusCode int `json:"status_code"`
	Data       struct {
		LinkSave struct {
			Link          string `json:"link"`
			AggregateLink string `json:"aggregate_link"`
			LongURL       string `json:"long_url"`
			NewLink       int    `json:"new_link"`
			UserHash      string `json:"user_hash"`
		} `json:"link_save"`
	} `json:"data"`
	StatusTxt string `json:"status_txt"`
}

// ErrBadStatusCode is returned when the API returns a non 200 error code
type ErrBadStatusCode struct {
	OriginalBody string
	Code         int
}

func (e *ErrBadStatusCode) Error() string {
	return fmt.Sprintf("Invalid status code: %d", e.Code)
}

// GetShortURL is
func GetShortURL(longURL string) (string, error) {
	encodedURL := url.QueryEscape(longURL)
	link := fmt.Sprintf("https://api-ssl.bitly.com/v3/user/link_save?access_token=%s&longUrl=%s", os.Getenv("BITLY_ACCESS_TOKEN"), encodedURL)

	resp, err := http.Get(link)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	var b bytes.Buffer
	if _, err := io.Copy(&b, resp.Body); err != nil {
		return "", err
	}
	debug := b.String()

	if resp.StatusCode != http.StatusOK {
		return "", &ErrBadStatusCode{
			OriginalBody: debug,
			Code:         resp.StatusCode,
		}
	}

	res := new(Response)
	if err := json.NewDecoder(&b).Decode(res); err != nil {
		return "", err
	}

	fmt.Println(res)

	return res.Data.LinkSave.Link, nil
}
