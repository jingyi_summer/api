package main

import (
	"log"
	"os"

	"gitlab.com/vanoapp/api"
	"gitlab.com/vanoapp/api/algolia"
	"gitlab.com/vanoapp/api/bandwidth"
	"gitlab.com/vanoapp/api/bolt"
	"gitlab.com/vanoapp/api/http"
	"gitlab.com/vanoapp/api/jwt"
	vanologger "gitlab.com/vanoapp/api/logger"
	"gitlab.com/vanoapp/api/mailgun"
	"gitlab.com/vanoapp/api/notification"
	"gitlab.com/vanoapp/api/postgre"
	"gitlab.com/vanoapp/api/validation"

	"github.com/algolia/algoliasearch-client-go/algoliasearch"
	"github.com/joho/godotenv"
	mailgunlib "github.com/mailgun/mailgun-go"
	"github.com/rs/cors"
	"github.com/rs/zerolog"
	apisplash "gitlab.com/vanoapp/api/splash"
	"gitlab.com/vanoapp/splash"
	pg "gopkg.in/pg.v5"
)

func main() {
	if err := godotenv.Load("./.env"); err != nil {
		log.Println("Error loading .env file")
	}

	db := pg.Connect(&pg.Options{
		User:     os.Getenv("DB_USER"),
		Addr:     os.Getenv("DB_ADDR"),
		Database: os.Getenv("DB_NAME"),
		Password: os.Getenv("DB_PASSWORD"),
	})
	defer db.Close()

	boltDB, err := bolt.Open("my.db", 0600, nil)
	if err != nil {
		log.Fatal(err)
	}
	defer boltDB.Close()

	// Set the httpAddress
	httpAddress := ":8000"
	if os.Getenv("PORT") != "" {
		httpAddress = ":" + os.Getenv("PORT")
	}

	// set up Bandwidth
	bandwidthClient, err := bandwidth.New(os.Getenv("BANDWIDTH_USER_ID"), os.Getenv("BANDWIDTH_API_TOKEN"), os.Getenv("BANDWIDTH_API_SECRET"))
	if err != nil {
		log.Fatal(err)
	}

	smsService := &bandwidth.SMSService{
		Client: bandwidthClient,
	}

	// Set up MailGun
	mg := mailgunlib.NewMailgun(os.Getenv("MAILGUN_DOMAIN"), os.Getenv("MAILGUN_API_KEY"), os.Getenv("MAILGUN_PUBLIC_KEY"))

	zeroLogger := zerolog.New(os.Stderr).With().Timestamp().Logger()

	emailService := &mailgun.EmailService{
		MailClient: mg,
	}

	searchService := &algolia.SearchService{
		AlgoliaClient: algoliasearch.NewClient(os.Getenv("ALGOLIA_APP_ID"), os.Getenv("ALGOLIA_SECRET_KEY")),
	}

	companyService := &postgre.CompanyService{
		DB:            db,
		SearchService: searchService,
		Logger:        zeroLogger,
	}

	professionalService := &postgre.ProfessionalService{
		DB:             db,
		CompanyService: companyService,
		SearchService:  searchService,
	}

	searchService.ProfessionalService = professionalService

	customerService := &postgre.CustomerService{
		DB:            db,
		SearchService: searchService,
	}

	notificationService := &notification.Service{
		EmailService:        emailService,
		SMSService:          smsService,
		ProfessionalService: professionalService,
		CustomerService:     customerService,
		CompanyService:      companyService,
	}

	professionalService.NotificationService = notificationService

	reminderService := &postgre.ReminderService{
		DB: db,
	}

	var appointmentService api.AppointmentService
	appointmentService = &postgre.AppointmentService{
		DB:                  db,
		NotificationService: notificationService,
		ProfessionalService: professionalService,
		CustomerService:     customerService,
		Logger:              zeroLogger,
	}
	appointmentService = &vanologger.AppointmentServiceLogger{
		Logger:             zeroLogger,
		AppointmentService: appointmentService,
	}

	serviceService := &postgre.ServiceService{
		DB:            db,
		SearchService: searchService,
	}

	authService := &jwt.AuthService{
		CustomerService:     customerService,
		ProfessionalService: professionalService,
		NotificationService: notificationService,
	}

	validationService := &validation.Service{
		AppointmentService: appointmentService,
	}

	merchantService := &postgre.MerchantService{
		DB: db,
	}

	analyticsService := &postgre.AnalyticsService{
		DB:                  db,
		ProfessionalService: professionalService,
		AppointmentService:  appointmentService,
	}

	transactionService := &postgre.TransactionService{
		DB:              db,
		CustomerService: customerService,
	}

	billingService := &apisplash.BillingService{
		Client: &splash.Client{
			HTTPClient: http.DefaultClient(),
			BaseURL:    splash.BaseTestAPIURL,
			APIKey:     os.Getenv("SPLASH_API_KEY"),
		},
		MerchantService:     merchantService,
		ProfessionalService: professionalService,
		CustomerService:     customerService,
		TransactionService:  transactionService,
	}

	authCache := &bolt.AuthCache{
		DB: boltDB,
	}

	services := http.Services{
		CompanyService:      companyService,
		ProfessionalService: professionalService,
		CustomerService:     customerService,
		AuthService:         authService,
		ReminderService:     reminderService,
		AppointmentService:  appointmentService,
		ServiceService:      serviceService,
		ValidationService:   validationService,
		MerchantService:     merchantService,
		BillingService:      billingService,
		AuthCache:           authCache,
		AnalyticsService:    analyticsService,
		TransactionService:  transactionService,
	}

	h := http.NewHandler(&services)
	h.Logger = zeroLogger
	lw := http.LoggerMiddleware{Logger: zeroLogger}
	vh := lw.LogginMiddleware(h)
	vh = http.ContextMiddleware(vh)

	zeroLogger.Info().Str("Starting server on port", httpAddress)

	opts := cors.Options{
		AllowedMethods: []string{"PUT", "POST", "GET", "DELETE", "PATCH"},
		AllowedHeaders: []string{"Origin", "Accept", "Content-Type", "Authorization"},
	}

	if os.Getenv("DEBUG") != "true" {
		opts.AllowedOrigins = []string{"https://vanoapp.com", "https://professionals.vanoapp.com", "https://clients.vanoapp.com"}
	}

	c := cors.New(opts)

	ch := c.Handler(vh)
	log.Fatal(http.ListenAndServe(httpAddress, ch))
}
