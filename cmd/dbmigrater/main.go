package main

import (
	"fmt"
	"log"
	"os"

	"github.com/joho/godotenv"
	"gitlab.com/vanoapp/api/hum"
	"gitlab.com/vanoapp/api/hum/postgre"
	pg "gopkg.in/pg.v5"
)

func main() {
	if err := godotenv.Load("./.env"); err != nil {
		log.Println("Error loading .env file")
	}

	db := pg.Connect(&pg.Options{
		User:     os.Getenv("DB_USER"),
		Addr:     os.Getenv("DB_ADDR"),
		Database: os.Getenv("DB_NAME"),
		Password: os.Getenv("DB_PASSWORD"),
	})
	defer db.Close()

	migrater := postgre.NewMigrater(db)

	createExtMigration := &hum.Migration{
		Name: "create extensions",
		Up:   getExtSQL(),
	}
	migrater.AppendMigration(createExtMigration)

	createEnumsMigration := &hum.Migration{
		Name: "create enums",
		Up:   getEnumsSQL(),
	}
	migrater.AppendMigration(createEnumsMigration)

	createCompaniesMigration := &hum.Migration{
		Name: "create companies table",
		Up:   getCompaniesSQL(),
	}
	migrater.AppendMigration(createCompaniesMigration)

	createCompaniesImagesMigration := &hum.Migration{
		Name: "create company images table",
		Up:   getCompanyImagesSQL(),
	}
	migrater.AppendMigration(createCompaniesImagesMigration)

	createProfessionalsMigration := &hum.Migration{
		Name: "create professionals table",
		Up:   getProfessionalsSQL(),
	}
	migrater.AppendMigration(createProfessionalsMigration)

	createServicesMigration := &hum.Migration{
		Name: "create services table",
		Up:   getServicesSQL(),
	}
	migrater.AppendMigration(createServicesMigration)

	createCustomersMigration := &hum.Migration{
		Name: "create customers table",
		Up:   getCustomersSQL(),
	}
	migrater.AppendMigration(createCustomersMigration)

	createCustomerCardsMigration := &hum.Migration{
		Name: "create customer cards table",
		Up:   getCustomerCardsSQL(),
	}
	migrater.AppendMigration(createCustomerCardsMigration)

	createCustomerNotesMigration := &hum.Migration{
		Name: "create customer notes table",
		Up:   getCustomerNotesSQL(),
	}
	migrater.AppendMigration(createCustomerNotesMigration)

	createAppointmentsMigration := &hum.Migration{
		Name: "create appointments table",
		Up:   getAppointmentsSQL(),
	}
	migrater.AppendMigration(createAppointmentsMigration)

	createAppointmentServiceMigration := &hum.Migration{
		Name: "create appointment service table",
		Up:   getAppointmentServiceSQL(),
	}
	migrater.AppendMigration(createAppointmentServiceMigration)

	createCompanyCustomerMigration := &hum.Migration{
		Name: "create company customer table",
		Up:   getCompanyCustomerSQL(),
	}
	migrater.AppendMigration(createCompanyCustomerMigration)

	createRemindersMigration := &hum.Migration{
		Name: "create reminders table",
		Up:   getRemindersSQL(),
	}
	migrater.AppendMigration(createRemindersMigration)

	createMerchantsMigration := &hum.Migration{
		Name: "create merchants table",
		Up:   getMerchantsSQL(),
	}
	migrater.AppendMigration(createMerchantsMigration)

	createTransactionsMigration := &hum.Migration{
		Name: "create transactions table",
		Up:   getTransactionsSQL(),
	}
	migrater.AppendMigration(createTransactionsMigration)

	createManagedProfessionalsMigration := &hum.Migration{
		Name: "create managed professionals table",
		Up:   getManagedProfessionalsSQL(),
	}
	migrater.AppendMigration(createManagedProfessionalsMigration)

	createForgotPasswordRequestsMigration := &hum.Migration{
		Name: "create forgot password requests table",
		Up:   getForgotPasswordRequestsSQL(),
	}
	migrater.AppendMigration(createForgotPasswordRequestsMigration)

	createAddRequireCreditCardToProfessionalsMigration := &hum.Migration{
		Name: "create add require credit card to professionals table",
		Up:   getAddRequireCreditCardToProfessionalsSQL(),
	}
	migrater.AppendMigration(createAddRequireCreditCardToProfessionalsMigration)

	addNoShowToAppointmentsMigration := &hum.Migration{
		Name: "add no show to appointments table",
		Up:   getAddNoShowAppointmentsSQL(),
	}
	migrater.AppendMigration(addNoShowToAppointmentsMigration)

	createPostAppointmentsMigration := &hum.Migration{
		Name: "create post appointments table",
		Up:   getCreatePostAppointmentsTable(),
	}
	migrater.AppendMigration(createPostAppointmentsMigration)

	createFeaturesMigration := &hum.Migration{
		Name: "create features table",
		Up:   getCreateFeaturesTable(),
	}
	migrater.AppendMigration(createFeaturesMigration)

	createFeatureProfessionalMigration := &hum.Migration{
		Name: "create feature_professional table",
		Up:   getCreateFeatureProfessionalTable(),
	}
	migrater.AppendMigration(createFeatureProfessionalMigration)

	createClocInsMigration := &hum.Migration{
		Name: "create cloc_ins table",
		Up:   getCreateClocInsTable(),
	}
	migrater.AppendMigration(createClocInsMigration)

	if err := hum.Migrate(migrater); err != nil {
		fmt.Println(err)
		return
	}
}

func getExtSQL() string {
	return `
	CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
	CREATE EXTENSION IF NOT EXISTS "btree_gist";
	`
}

func getEnumsSQL() string {
	return `
		DO $$
		BEGIN
			IF NOT EXISTS (SELECT 1 FROM pg_type WHERE typname = 'invitation_status') THEN
				CREATE TYPE invitation_status AS ENUM ('pending', 'accepted', 'declined');
			END IF;
		END$$;
	`
}

func getCompaniesSQL() string {
	return `
	CREATE TABLE "companies" (
		"id" text NOT NULL,
		"name" text NOT NULL,
		"address_line1" text NOT NULL,
		"address_line2" text,
		"city" text NOT NULL,
		"zip_code" int NOT NULL,
		"state" text NOT NULL,
		"country" text NOT NULL,
		"hours" jsonb NOT NULL,
		"logo_url" text NOT NUll DEFAULT 'https://res.cloudinary.com/solabuddy/image/upload/v1499454181/thumbs-up_c2bgve.jpg',
		"cover_url" text NOT NULL DEFAULT 'https://res.cloudinary.com/solabuddy/image/upload/v1504749789/sample.jpg',
		"description" text,
		"phone_number" text NOT NULL UNIQUE,
		"email" text NOT NULL UNIQUE,
		"merchant_integrated" boolean NOT NULL DEFAULT false,
		"timezone" text NOT NULL,
		"created_at" timestamp with time zone NOT NULL,
		"updated_at" timestamp with time zone NOT NULL,
		PRIMARY KEY ("id")
	);

	INSERT INTO "public"."companies"("id", "name", "address_line1", "city", "zip_code", "state", "country", "hours", "timezone", "description", "phone_number", "email", "created_at", "updated_at") VALUES('36ec09ec-6b07-45e3-ae2d-a77bfe381baa', 'Demo Company', '4220 milky way dr', 'dab city', 42002, 'KY', 'USA', '{"friday": {"open": "09:00", "close": "17:00", "operating": true}, "monday": {"open": "09:00", "close": "17:00", "operating": true}, "sunday": {"open": "09:00", "close": "17:00", "operating": false}, "tuesday": {"open": "09:00", "close": "17:00", "operating": true}, "saturday": {"open": "09:00", "close": "17:00", "operating": false}, "thursday": {"open": "09:00", "close": "17:00", "operating": true}, "wednesday": {"open": "09:00", "close": "17:00", "operating": true}}', 'America/New_York', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sapien neque, convallis in ullamcorper nec, accumsan et orci. Nulla interdum facilisis pretium. Nulla sit amet laoreet dui.
	
	Pellentesque nec urna arcu. Morbi purus quam, dapibus nec urna non, fringilla congue enim. Sed ultrices tellus et ipsum auctor, semper hendrerit ipsum mollis. Duis sit amet nisl congue, ultrices lorem quis, dapibus odio. 
	
	Nullam id tortor dignissim, posuere urna ut, luctus nulla. Vivamus dignissim, nisi quis malesuada tempor, erat purus dictum sem, vel tempus dui sapien vitae ante. Fusce at euismod ipsum, quis vestibulum neque.', '9543030759', 'rodrigo@threeaccents.com', 'now()', 'now()') RETURNING "id", "name", "address_line1", "address_line2", "city", "zip_code", "state", "country", "hours", "created_at", "updated_at";
	`
}

func getCompanyImagesSQL() string {
	return `
	CREATE TABLE "company_images" (
		"id" text NOT NULL,
		"company_id" text NOT NULL REFERENCES companies ON DELETE CASCADE,
		"url" text NOT NULL,
		"created_at" timestamp with time zone NOT NULL,
		"updated_at" timestamp with time zone NOT NULL,
		PRIMARY KEY ("id")
	);
	`
}

func getProfessionalsSQL() string {
	return `
		CREATE TABLE "professionals" (
			"id" text NOT NULL,
			"company_id" text NOT NULL REFERENCES companies ON DELETE CASCADE,
			"first_name" text NOT NULL,
			"last_name" text NOT NULL,
			"email" text NOT NULL UNIQUE,
			"password" text NOT NUll,
			"phone_number" text NOT NULL UNIQUE,
			"notification" jsonb NOT NULL CHECK (notification ? 'sms') CHECK (notification ? 'email') CHECK (notification ? 'phone'),
			"permission" jsonb NOT NULL CHECK (permission ? 'manage_appointments') CHECK (permission ? 'manage_company') CHECK (permission ? 'manage_professionals') CHECK (permission ? 'manage_analytics'), 
			"ip_address" text,
			"password_set" boolean NOT NULL DEFAULT false,
			"admin_generated" boolean NOT NULL DEFAULT true,
			"last_login" timestamp,
			"onboarded" boolean NOT NULL DEFAULT false,
			"created_at" timestamp with time zone NOT NULL DEFAULT now(),
			"updated_at" timestamp with time zone NOT NULL DEFAULT now(),
			PRIMARY KEY ("id")
		);

		INSERT INTO "public"."professionals"("id", "company_id", "first_name", "last_name", "email", "password", "phone_number", "notification", "permission", "admin_generated", "password_set", "created_at", "updated_at") VALUES('36ec09ec-6b07-45e3-ae2d-a77bfe381bab', '36ec09ec-6b07-45e3-ae2d-a77bfe381baa', 'Jane', 'Doe', 'demo@email.com', '$2a$10$.f3oWdyrjpvnN1NLkvkenuJQ3Xt6H6OWhDpxu8m89yQmKcR1ob.la', '9545554444', '{"sms": true, "email": true, "phone": false}', '{"manage_appointments": true, "manage_company": true, "manage_analytics": true, "manage_professionals": true}', false, true, 'now()', 'now()') RETURNING "id", "company_id", "first_name", "last_name", "email", "password", "phone_number", "notification", "ip_address", "last_login", "created_at", "updated_at";
	`
}

func getServicesSQL() string {
	return `
	    CREATE TABLE "services" (
	        "id" text NOT NULL,
	        "professional_id" text NOT NULL REFERENCES professionals ON DELETE CASCADE,
	        "name" text NOT NULL,
	        "description" text,
	        "length" int NOT NULL,
	        "price" int NOT NULL,
	        "require_phone_call" bool NOT NULL default false,
	        "created_at" timestamp with time zone NOT NULL,
	        "updated_at" timestamp with time zone NOT NULL,
	        PRIMARY KEY ("id")
	    );

		INSERT INTO "public"."services"("id", "professional_id", "name", "description", "length", "price", "created_at", "updated_at") VALUES('36ec09ec-6b07-45e3-ae2d-a77bfe381ba3', '36ec09ec-6b07-45e3-ae2d-a77bfe381bab', 'Men''s Haircut', 'Food truck banjo gastropub, vegan echo park kogi green juice.', 30, 3000, 'now()', 'now()') RETURNING "id", "professional_id", "name", "description", "length", "price", "created_at", "updated_at";
		INSERT INTO "public"."services"("id", "professional_id", "name", "description", "length", "price", "created_at", "updated_at") VALUES('36ec09ec-6b07-45e3-ae2d-a77bfe381ba1', '36ec09ec-6b07-45e3-ae2d-a77bfe381bab', 'Woman''s Haircut', 'Food truck banjo gastropub, vegan echo park kogi green juice.', 60, 6000, 'now()', 'now()') RETURNING "id", "professional_id", "name", "description", "length", "price", "created_at", "updated_at";
		INSERT INTO "public"."services"("id", "professional_id", "name", "description", "length", "price", "created_at", "updated_at") VALUES('36ec09ec-6b07-45e3-ae2d-a77bfe381ba2', '36ec09ec-6b07-45e3-ae2d-a77bfe381bab', 'Nails', 'Food truck banjo gastropub, vegan echo park kogi green juice.', 25, 4500, 'now()', 'now()') RETURNING "id", "professional_id", "name", "description", "length", "price", "created_at", "updated_at";
		INSERT INTO "public"."services"("id", "professional_id", "name", "description", "length", "price", "created_at", "updated_at") VALUES('36ec09ec-6b07-45e3-ae2d-a77bfe381ba6', '36ec09ec-6b07-45e3-ae2d-a77bfe381bab', 'Kid''s Haircut', 'Food truck banjo gastropub, vegan echo park kogi green juice.', 20, 1500, 'now()', 'now()') RETURNING "id", "professional_id", "name", "description", "length", "price", "created_at", "updated_at";
		INSERT INTO "public"."services"("id", "professional_id", "name", "description", "length", "price", "created_at", "updated_at") VALUES('36ec09ec-6b07-45e3-ae2d-a77bfe381ba9', '36ec09ec-6b07-45e3-ae2d-a77bfe381bab', 'Massage', 'Food truck banjo gastropub, vegan echo park kogi green juice.', 60, 12000, 'now()', 'now()') RETURNING "id", "professional_id", "name", "description", "length", "price", "created_at", "updated_at";
	`
}

func getCustomersSQL() string {
	return `
	CREATE TABLE "customers" (
		"id" text NOT NULL,
		"selected_company_id" text REFERENCES companies,
		"first_name" text NOT NULL,
		"last_name" text NOT NULL,
		"email" text UNIQUE,
		"phone_number" text NOT NULL UNIQUE,
		"password" text,
		"notification" jsonb NOT NULL,
		"ip_address" text,
		"last_login" timestamp,
		"splash_id" text,
		"created_at" timestamp with time zone NOT NULL,
		"updated_at" timestamp with time zone NOT NULL,
		PRIMARY KEY ("id")
	);
	`
}

func getCustomerCardsSQL() string {
	return `
	CREATE TABLE "customer_cards" (
		"id" text NOT NULL,
		"customer_id" text NOT NULL  REFERENCES customers ON DELETE CASCADE,
		"last_four" int NOT NULL,
		"name" text NOT NULL,
		"exp" text NOT NULL,
		"token" text,
		"method" text NOT NULL,
		"created_at" timestamp with time zone NOT NULL,
		"updated_at" timestamp with time zone NOT NULL,
		PRIMARY KEY ("id")
	);
	`
}

func getCustomerNotesSQL() string {
	return `
	CREATE TABLE "customer_notes" (
		"id" text NOT NULL,
		"company_id" text NOT NULL REFERENCES companies ON DELETE CASCADE,
		"customer_id" text NOT NULL REFERENCES customers ON DELETE CASCADE,
		"value" text NOT NULL,
		"created_at" timestamp with time zone NOT NULL,
		"updated_at" timestamp with time zone NOT NULL,
		PRIMARY KEY ("id")
	);
	`
}

func getAppointmentsSQL() string {
	return `
	CREATE TABLE "appointments" (
		"id" text NOT NULL,
		"customer_id" text NOT NULL REFERENCES customers ON DELETE CASCADE ON UPDATE CASCADE,
		"professional_id" text NOT NULL REFERENCES professionals ON DELETE CASCADE ON UPDATE CASCADE,
		"start_time" bigint NOT NULL,
		"end_time" bigint NOT NULL,
		EXCLUDE USING gist (
			professional_id WITH =,
			int8range(start_time, end_time) WITH &&
		) WHERE (NOT cancelled),
		"notes" text,
		"cancelled" boolean NOT NULL DEFAULT false,
		"notified_customer" boolean not null DEFAULT false,
		"notified_professional" boolean not null DEFAULT false,
		"created_at" timestamp with time zone NOT NULL,
		"updated_at" timestamp with time zone NOT NULL,
		PRIMARY KEY ("id")
	);
`
}

func getAppointmentServiceSQL() string {
	return `
	CREATE TABLE "appointment_service" (
		"id" text NOT NULL,
		"service_id" text NOT NULL REFERENCES services ON DELETE CASCADE,
		"appointment_id" text NOT NULL REFERENCES appointments ON DELETE CASCADE,
		"length" integer NOT NULL,
		PRIMARY KEY ("id")
	);
`
}

func getCompanyCustomerSQL() string {
	return `
	CREATE TABLE "company_customer" (
		"id" text NOT NULL,
		"customer_id" text NOT NULL REFERENCES customers ON DELETE CASCADE,
		"company_id" text NOT NULL REFERENCES companies ON DELETE CASCADE,
		PRIMARY KEY ("id")
	);
	`
}

func getRemindersSQL() string {
	return `
	CREATE TABLE "reminders" (
		"id" text NOT NULL,
		"professional_id" text NOT NULL REFERENCES professionals ON DELETE CASCADE,
		"text" text NOT NULL,
		"description" text,
		"due_date" bigint NOT NULL,
		"notified" bool DEFAULT false NOT NULL,
		"completed" bool DEFAULT false NOT NULL,
		"created_at" timestamp with time zone NOT NULL,
		"updated_at" timestamp with time zone NOT NULL,
		PRIMARY KEY ("id")
	);

	`
}

func getMerchantsSQL() string {
	return `
	CREATE TABLE "merchants" (
		"id" text NOT NULL,
		"company_id" text UNIQUE NOT NULL REFERENCES companies ON DELETE CASCADE,
		"splash_id" text NOT NULL,
		"ein" text,
		"annual_cc_sales" int NOT NULL,
		"account_number" text NOT NULL,
		"routing_number" text NOT NULL,
		"account_type" text NOT NULL,
		"dl_number" text NOT NULL,
		"dl_state" text NOT NULL,
		"created_at" timestamp with time zone NOT NULL,
		"updated_at" timestamp with time zone NOT NULL,
		PRIMARY KEY ("id")
	);

	`
}

func getTransactionsSQL() string {
	return `
	DROP TYPE IF EXISTS transaction_type;
	CREATE TYPE transaction_type AS ENUM ('token', 'card');
	CREATE TABLE "transactions" (
		"id" text NOT NULL,
		"professional_id" text NOT NULL REFERENCES professionals,
		"customer_id" text REFERENCES customers,
		"payment_type" transaction_type NOT NULL, 
		"total" int NOT NULL,
		"splash_id" text NOT NULL,
		"appointment_id" text NOT NULL,
		"last_four" int NOT NULL,
		"card_method" text NOT NULL,
		"refunded" bool DEFAULT false NOT NULL,
		"created_at" timestamp with time zone NOT NULL,
		"updated_at" timestamp with time zone NOT NULL,
		PRIMARY KEY ("id")
	);

	`
}

func getManagedProfessionalsSQL() string {
	return `
	CREATE TABLE "managed_professionals" (
		"id" text NOT NULL,
		"manager" text NOT NULL REFERENCES professionals ON DELETE CASCADE,
		"managee" text NOT NULL REFERENCES professionals ON DELETE CASCADE,
		"permission" jsonb NOT NULL CHECK (permission ? 'manage_appointments') CHECK (permission ? 'manage_company') CHECK (permission ? 'manage_professionals') CHECK (permission ? 'manage_analytics'), 
		"status" invitation_status DEFAULT 'pending' not null,
		"created_at" timestamp with time zone NOT NULL,
		"updated_at" timestamp with time zone NOT NULL,
		PRIMARY KEY ("id")
	);

	`
}

func getForgotPasswordRequestsSQL() string {
	return `
	CREATE TABLE "forgot_password_requests" (
		"id" text NOT NULL,
		"hash" text NOT NULL,
		"professional_id" text REFERENCES professionals ON DELETE CASCADE,
		"customer_id" text REFERENCES customers ON DELETE CASCADE,
		"used" boolean NOT NULL DEFAULT false,
		"created_at" timestamp  NOT NULL,
		"updated_at" timestamp  NOT NULL,
		PRIMARY KEY ("id")
	);
`
}

func getAddRequireCreditCardToProfessionalsSQL() string {
	return `
		ALTER TABLE "professionals" ADD COLUMN "require_cc" boolean NOT NULL DEFAULT false;
	`
}

func getAddNoShowAppointmentsSQL() string {
	return `
		ALTER TABLE "appointments" ADD COLUMN "no_show" boolean NOT NULL DEFAULT false;
	`
}

func getCreatePostAppointmentsTable() string {
	return `
	CREATE TABLE "post_appointments" (
		"id" text,
		"appointment_id" text REFERENCES appointments ON DELETE CASCADE NOT NULL,
		"color_formula" text,
		"highlight_formula" text,
		"total_service_length" text,
		"notes" text,
		"created_at" timestamp NOT NULL,
		"updated_at" timestamp NOT NULL,
		PRIMARY KEY ("id")
	);
	`
}

func getCreateFeaturesTable() string {
	return `
	CREATE TABLE "features" (
		"id" text,
		"name" text not null,
		"description" text not null,
		"company_wide" bool default false not null,
		"price" int not null,
		"sale_price" int,
		"created_at" timestamp NOT NULL,
		"updated_at" timestamp NOT NULL,
		PRIMARY KEY ("id")
	);

	INSERT INTO "features" ("id", "name", "description", "company_wide", "price", "created_at", "updated_at") VALUES(uuid_generate_v4(), 'require_credit_card', 'The Require Credit Card feature will require clients to input their credit card information before being able to book their appointment. While this may seem like a useful tool to avoid no-shows, it is not recommended as a good business practice.', FALSE, 499, 'now()', 'now()') RETURNING "id", "name", "description", "company_wide", "price", "sale_price", "created_at", "updated_at";

	INSERT INTO "features" ("id", "name", "description", "company_wide", "price", "created_at", "updated_at") VALUES(uuid_generate_v4(), 'manage_employee_professionals', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam dolor turpis, auctor at congue porta, faucibus vitae sapien. Donec molestie mattis enim interdum mattis. Sed convallis congue augue et ultrices.', FALSE, 499, 'now()', 'now()') RETURNING "id", "name", "description", "company_wide", "price", "sale_price", "created_at", "updated_at";

	INSERT INTO "features" ("id", "name", "description", "company_wide", "price", "created_at", "updated_at") VALUES(uuid_generate_v4(), 'manage_independent_professionals', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam dolor turpis, auctor at congue porta, faucibus vitae sapien. Donec molestie mattis enim interdum mattis. Sed convallis congue augue et ultrices.', FALSE, 499, 'now()', 'now()') RETURNING "id", "name", "description", "company_wide", "price", "sale_price", "created_at", "updated_at";

	INSERT INTO "features" ("id", "name", "description", "company_wide", "price", "created_at", "updated_at") VALUES(uuid_generate_v4(), 'credit_card_processing', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam dolor turpis, auctor at congue porta, faucibus vitae sapien. Donec molestie mattis enim interdum mattis. Sed convallis congue augue et ultrices.', FALSE, 499, 'now()', 'now()') RETURNING "id", "name", "description", "company_wide", "price", "sale_price", "created_at", "updated_at";
	`
}

func getCreateFeatureProfessionalTable() string {
	return `
	CREATE TABLE "feature_professional" (
		"id" text,
		"professional_id" text REFERENCES professionals ON DELETE CASCADE NOT NULL,
		"feature_id" text REFERENCES features ON DELETE CASCADE NOT NULL,
		"enabled" bool DEFAULT false NOT NULL,
		PRIMARY KEY ("id")
	);
	`
}

func getCreateClocInsTable() string {
	return `
	CREATE TABLE "cloc_ins" (
		"id" text,
		"professional_id" text NOT NULL,
		"cloc_in_time" timestamp NOT NULL,
		"cloc_out_time" timestamp,
		"created_at" timestamp NOT NULL DEFAULT now(),
		"updated_at" timestamp NOT NULL DEFAULT now(),
		PRIMARY KEY ("id"),
		FOREIGN KEY ("professional_id") REFERENCES "professionals"("id") ON DELETE CASCADE
	);

	INSERT INTO "features" ("id", "name", "description", "company_wide", "price", "created_at", "updated_at") VALUES(uuid_generate_v4(), 'cloc_in_cloc_out', 'The Require Credit Card feature will require clients to input their credit card information before being able to book their appointment. While this may seem like a useful tool to avoid no-shows, it is not recommended as a good business practice.', FALSE, 499, 'now()', 'now()') RETURNING "id", "name", "description", "company_wide", "price", "sale_price", "created_at", "updated_at";
	`
}

func merchantFeatureMigration() string {
	return `
	ALTER TABLE "professionals" ADD COLUMN "independent" boolean NOT NULL DEFAULT false;
	
	ALTER TABLE "merchants"
	DROP CONSTRAINT "merchants_company_id_key",
	ADD COLUMN "professional_id" text,
	ADD FOREIGN KEY ("professional_id") REFERENCES "professionals"("id") ON DELETE CASCADE;
	
	ALTER TABLE "professionals" ADD COLUMN "merchant_integrated" boolean NOT NULL DEFAULT false;

	ALTER TABLE "professionals" ADD CHECK (permission ? 'manage_appointments'::text);
	`
}

func alterFeatures() string {
	return `
	func ALTER TABLE "feature_professional"
	DROP CONSTRAINT "feature_professional_feature_id_fkey",
	ADD FOREIGN KEY ("feature_id") REFERENCES "features"("id") ON DELETE CASCADE;
	
	ALTER TABLE "features"
	ADD COLUMN "order" int,
	ADD UNIQUE ("order");
	
	ALTER TABLE "features" ALTER COLUMN "order" SET NOT NULL;`
}

// CREATE TABLE customer_professional" (
//     "id" serial,
//     "customer_id" text NOT NULL,
//     "professional_id" text NOT NULL,
//     PRIMARY KEY ("id"),
//     FOREIGN KEY ("customer_id") REFERENCES "customers"("id") ON DELETE CASCADE,
//     FOREIGN KEY ("professional_id") REFERENCES "professionals"("id") ON DELETE CASCADE
// );
