package validation

import (
	"errors"

	"gitlab.com/vanoapp/api"
)

// ValidateCreateMerchant is
func (s *Service) ValidateCreateMerchant(p *api.Merchant) error {
	switch {
	case p.CompanyID == "":
		return errors.New("company id is required")
	case p.DLState == "":
		return errors.New("driver license state is required")
	case p.DLNumber == "":
		return errors.New("driver license number is required")
	case p.AnnualCCSales == 0:
		return errors.New("annual credit card sales is required")
	case p.RoutingNumber == "":
		return errors.New("routing number is required")
	case p.AccountNumber == "":
		return errors.New("account number is required")
	case p.SSN == "" && p.EIN == "":
		return errors.New("social security or ein number required")
	case p.AccountType == "":
		return errors.New("account type is required")
	}
	return nil
}
