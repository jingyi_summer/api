package api

import "testing"

func TestCompany_ValidateCreate(t *testing.T) {
	tests := []struct {
		p        *Company
		expected bool
	}{
		{&Company{Name: "test company", AddressLine1: "hey", City: "hello", State: "hello", ZipCode: 2000, Country: "country"}, true},
		{&Company{AddressLine1: "hey", City: "hello", State: "hello", ZipCode: 2000, Country: "country"}, false},
		{&Company{Name: "test company", City: "hello", State: "hello", ZipCode: 2000, Country: "country"}, false},
		{&Company{Name: "test company", AddressLine1: "hey", State: "hello", ZipCode: 2000, Country: "country"}, false},
		{&Company{Name: "test company", AddressLine1: "hey", City: "hello", ZipCode: 2000, Country: "country"}, false},
		{&Company{Name: "test company", AddressLine1: "hey", City: "hello", State: "hello", Country: "country"}, false},
		{&Company{Name: "test company", AddressLine1: "hey", City: "hello", State: "hello", ZipCode: 2000}, false},
	}

	for _, test := range tests {
		err := test.p.ValidateCreate()
		result := err == nil

		switch {
		case result != test.expected:
			t.Errorf("Expecting result to be %v, got %v", test.expected, result)
		}
	}
}

func TestCompany_ValidateUpdate(t *testing.T) {
	tests := []struct {
		p        *Company
		expected bool
	}{
		{&Company{ID: "sdsadsd", Name: "test company", AddressLine1: "hey", City: "hello", State: "hello", ZipCode: 2000, Country: "country"}, true},
		{&Company{Name: "test company", AddressLine1: "hey", City: "hello", State: "hello", ZipCode: 2000, Country: "country"}, false},
		{&Company{AddressLine1: "hey", City: "hello", State: "hello", ZipCode: 2000, Country: "country"}, false},
		{&Company{Name: "test company", City: "hello", State: "hello", ZipCode: 2000, Country: "country"}, false},
		{&Company{Name: "test company", AddressLine1: "hey", State: "hello", ZipCode: 2000, Country: "country"}, false},
		{&Company{Name: "test company", AddressLine1: "hey", City: "hello", ZipCode: 2000, Country: "country"}, false},
		{&Company{Name: "test company", AddressLine1: "hey", City: "hello", State: "hello", Country: "country"}, false},
		{&Company{Name: "test company", AddressLine1: "hey", City: "hello", State: "hello", ZipCode: 2000}, false},
	}
	for _, test := range tests {
		err := test.p.ValidateUpdate()
		result := err == nil
		switch {
		case result != test.expected:
			t.Errorf("Expecting result to be %v, got %v", test.expected, result)
		}
	}
}
