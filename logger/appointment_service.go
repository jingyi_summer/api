package logger

import (
	"github.com/rs/zerolog"
	"gitlab.com/vanoapp/api"
)

//AppointmentServiceLogger is
type AppointmentServiceLogger struct {
	Logger             zerolog.Logger
	AppointmentService api.AppointmentService
}

// CreateAppointment is
func (s *AppointmentServiceLogger) CreateAppointment(a *api.Appointment) error {
	return s.AppointmentService.CreateAppointment(a)
}

// ListProfessionalBlockoffs is
func (s *AppointmentServiceLogger) ListProfessionalBlockoffs(id string) ([]*api.Appointment, error) {
	return s.AppointmentService.ListProfessionalBlockoffs(id)
}

// ListProfessionalAppointments is
func (s *AppointmentServiceLogger) ListProfessionalAppointments(professionalID string, startTime, endTime int64, limit int) ([]*api.Appointment, error) {
	return s.AppointmentService.ListProfessionalAppointments(professionalID, startTime, endTime, limit)
}

// ListProfessionalAvaibaleAppointmentTimes is
func (s *AppointmentServiceLogger) ListProfessionalAvaibaleAppointmentTimes(professionalID string, serviceLengths, date int64) ([]string, error) {
	return s.AppointmentService.ListProfessionalAvaibaleAppointmentTimes(professionalID, serviceLengths, date)
}

// ListCustomerAppointments is
func (s *AppointmentServiceLogger) ListCustomerAppointments(customerID string, startTime, endTime int64, limit int) ([]*api.Appointment, error) {
	return s.AppointmentService.ListCustomerAppointments(customerID, startTime, endTime, limit)
}

// GetAppointment is
func (s *AppointmentServiceLogger) GetAppointment(id string) (*api.Appointment, error) {
	return s.AppointmentService.GetAppointment(id)
}

// UpdateAppointment is
func (s *AppointmentServiceLogger) UpdateAppointment(a *api.Appointment) error {
	return s.AppointmentService.UpdateAppointment(a)
}

// CancelAppointment is
func (s *AppointmentServiceLogger) CancelAppointment(id string) error {
	return s.AppointmentService.CancelAppointment(id)
}

// CheckAppointmentIsAvailable is used primarily in validation. So instead of sending back the user an internal server error when they try and book an appointment that has already been taken they get a bad request validation error.
func (s *AppointmentServiceLogger) CheckAppointmentIsAvailable(profID string, startTime, endTime int64) error {
	return s.AppointmentService.CheckAppointmentIsAvailable(profID, startTime, endTime)
}

// CreatePostAppointment is
func (s *AppointmentServiceLogger) CreatePostAppointment(p *api.PostAppointment) (*api.PostAppointment, error) {
	return s.AppointmentService.CreatePostAppointment(p)
}

// UpdatePostAppointment is
func (s *AppointmentServiceLogger) UpdatePostAppointment(p *api.PostAppointment) (*api.PostAppointment, error) {
	return s.AppointmentService.UpdatePostAppointment(p)
}

// DeletePostAppointment is
func (s *AppointmentServiceLogger) DeletePostAppointment(id string) error {
	return s.AppointmentService.DeletePostAppointment(id)
}
