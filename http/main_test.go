package http

import (
	"log"
	"os"
	"testing"

	"github.com/joho/godotenv"

	"gitlab.com/vanoapp/api"
	"gitlab.com/vanoapp/api/mock"
	"gitlab.com/vanoapp/api/validation"
)

var (
	testHandler      *Handler
	testNotification = &api.Notification{
		SMS:   true,
		Email: true,
		Phone: false,
	}
	testPermission = &api.ProfessionalPermission{
		ManageAppointments:  true,
		ManageCompany:       true,
		ManageAnalytics:     true,
		ManageProfessionals: true,
	}
	testCardNumber = "4444444444444444"
	correctid      = "correctid"
	testJWT        = "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1MDY2MTc5NTIsImlhdCI6MTUwNDAyNTk1MiwiaXNzIjoiaHR0cDovL2xvY2FsaG9zdDo4MDgwIiwianRpIjoia1VNbzFaN3BuYXZraUNJZWVvMnNHQT09IiwibmJmIjoxNTA0MDI1OTUyLCJyb2xlIjoicHJvZmVzc2lvbmFsIiwic2NvcGVzIjp7InRva2VucyI6eyJhY3Rpb25zIjpbImJsYWNrbGlzdCJdfX0sInN1YiI6ImIyOWNlNTU1LTk3ZDAtNGI4OS1hNDUxLTJlZjc3Y2NhM2I0ZiJ9.dmhfYN1eGBhS5hwY6DByESLTHYwL_JcVDOooCjiMhuI"
)

func TestMain(m *testing.M) {
	if err := godotenv.Load("../.env"); err != nil {
		log.Fatal("Error loading .env file")
	}

	services := Services{
		CompanyService:      &mock.CompanyService{},
		ProfessionalService: &mock.ProfessionalService{},
		CustomerService:     &mock.CustomerService{},
		ReminderService:     &mock.ReminderService{},
		AppointmentService:  &mock.AppointmentService{},
		ServiceService:      &mock.ServiceService{},
		ValidationService:   &validation.Service{&mock.AppointmentService{}},
		AuthCache:           &mock.AuthCache{},
		BillingService:      &mock.BillingService{},
		MerchantService:     &mock.MerchantService{},
		AnalyticsService:    &mock.AnalyticsService{},
	}
	testHandler = NewHandler(&services)

	t := m.Run()

	os.Exit(t)
}

func request() {

}
