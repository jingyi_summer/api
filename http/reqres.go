package http

import (
	"errors"

	"gitlab.com/vanoapp/api"
)

/**********************/
/* NEED TO MOVE STRUCTS OF THIS FILE TO THEIR RESPCTED HANDERLS I FEEL LIKE ITS EASIER TO REASON ABOUT THAN ONE HUGE REQQUEST RESPNSE FILE */
/**********************/

/***************************************/
/** COMPANY **/
/***************************************/

type companyRequest struct {
	Company api.Company `json:"company"`
}

type companyResponse struct {
	Company *api.Company `json:"company"`
}

type companiesResponse struct {
	Companies []*api.Company `json:"companies"`
}

type uploadCompanyImageRequest struct {
	CompanyImage *api.CompanyImage `json:"company_image"`
}

type uploadCompanyImageResponse struct {
	CompanyImage *api.CompanyImage `json:"company_image"`
}

type companyImagesResponse struct {
	CompanyImages []*api.CompanyImage `json:"company_images"`
}

/***************************************/
/** Professional **/
/***************************************/

type activateProfessionalRequest struct {
	ID       string `json:"id"`
	Password string `json:"password"`
}

type professionalRequest struct {
	Professional api.Professional `json:"professional"`
}

type updateProfessionalRequest struct {
	Professional api.UpdateProfessional `json:"professional"`
}

type professionalResponse struct {
	Professional *api.SanitizedProfessional `json:"professional"`
}

type professionalsResponse struct {
	Professionals []*api.SanitizedProfessional `json:"professionals"`
}

type managedProfessionalRequest struct {
	Manager    string                      `json:"manager"`
	Managee    string                      `json:"managee"`
	Permission *api.ProfessionalPermission `json:"permission"`
}

type updateManagedProfessionalRequest struct {
	ManagedProfessional *api.ManagedProfessional `json:"managed_professional"`
}

type managedProfessionalResponse struct {
	ManagedProfessional *api.ManagedProfessional `json:"managed_professional"`
}

type managedProfessionalsResponse struct {
	ManagedProfessionals []*api.ManagedProfessional `json:"managed_professionals"`
}

/***************************************/
/** SERVICE **/
/***************************************/

type serviceRequest struct {
	Service api.Service `json:"service"`
}

type serviceResposne struct {
	Service *api.Service `json:"service"`
}

/***************************************/
/** CUSTOMER **/
/***************************************/

type customerCompanyRequest struct {
	CustomerID string `json:"customer_id"`
	CompanyID  string `json:"company_id"`
}

type customerRequest struct {
	Customer api.Customer `json:"customer"`
}

type updateCustomerRequest struct {
	Customer api.Customer `json:"customer"`
}

type customerResponse struct {
	Customer *api.SanitizedCustomer `json:"customer"`
}

type customersResponse struct {
	Customers []*api.SanitizedCustomer `json:"customers"`
}

type cardRequest struct {
	Card api.CustomerCard `json:"card"`
}

type cardResponse struct {
	Card *api.SanitizedCustomerCard `json:"card"`
}

type cardsResponse struct {
	Cards []*api.SanitizedCustomerCard `json:"cards"`
}

type noteRequest struct {
	Note api.CustomerNote `json:"note"`
}

type noteResponse struct {
	Note *api.CustomerNote `json:"note"`
}

/***************************************/
/** APPOINTMENT **/
/***************************************/

type appointmentRequest struct {
	Appointment api.Appointment `json:"appointment"`
}

type appointmentsResponse struct {
	Appointments []*api.Appointment `json:"appointments"`
}

type appointmentResponse struct {
	Appointment *api.Appointment `json:"appointment"`
}

type postAppointmentRequest struct {
	PostAppointment *api.PostAppointment `json:"post_appointment"`
}

func (r *postAppointmentRequest) validateCreate() error {
	allFieldsNil := r.PostAppointment.ColorFormula == "" && r.PostAppointment.HighlightFormula == "" && r.PostAppointment.TotalServiceLength == "" && r.PostAppointment.Notes == ""

	switch {
	case r.PostAppointment.AppointmentID == "":
		return errors.New("appointment_id is required")
	case allFieldsNil:
		return errors.New("atleast 1 field is required to be filled out")
	}
	return nil
}

func (r *postAppointmentRequest) validateUpdate() error {
	allFieldsNil := r.PostAppointment.ColorFormula == "" && r.PostAppointment.HighlightFormula == "" && r.PostAppointment.TotalServiceLength == "" && r.PostAppointment.Notes == ""

	switch {
	case r.PostAppointment.ID == "":
		return errors.New("post appointment id is required")
	case r.PostAppointment.AppointmentID == "":
		return errors.New("appointment_id is required")
	case allFieldsNil:
		return errors.New("atleast 1 field is required to be filled out")
	}
	return nil
}

type postAppointmentResponse struct {
	PostAppointment *api.PostAppointment `json:"post_appointment"`
}

/***************************************/
/** REMINDER **/
/***************************************/

type reminderRequest struct {
	Reminder api.Reminder `json:"reminder"`
}

type reminderResponse struct {
	Reminder *api.Reminder `json:"reminder"`
}

type remindersResponse struct {
	Reminders []*api.Reminder `json:"reminders"`
}

type updateReminderRequest struct {
	Reminder api.UpdateReminder `json:"reminder"`
}

/***************************************/
/** ANALYTICS **/
/***************************************/
type analyticsResponse struct {
	Analytics *api.Analytics `json:"analytics"`
}

/***************************************/
/** TRANSACTION **/
/***************************************/
type transactionRequest struct {
	Transaction api.Transaction `json:"transaction"`
}

type transactionResponse struct {
	Transaction *api.Transaction `json:"transaction"`
}

type transactionsResponse struct {
	Transactions []*api.Transaction `json:"transactions"`
}

/***************************************/
/** GENERICS **/
/***************************************/
// errorResponse is a generic HTTP response body for errors.
type errorResponse struct {
	Error     string `json:"error,omitempty"`
	Type      string `json:"type"`
	RequestID string `json:"request_id"`
}

type messageResponse struct {
	Message string `json:"message"`
}

type resetPasswordRequest struct {
	ID          string `json:"id"`
	Password    string `json:"password"`
	NewPassword string `json:"new_password"`
}

type setAccountRequest struct {
	ID       string `json:"id"`
	Password string `json:"password"`
}
