package http

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"gitlab.com/vanoapp/api"
)

var testReminderID = "correctid"

func TestCreateReminder(t *testing.T) {
	// set up the test server
	server := httptest.NewServer(testHandler)
	// get the server url
	url := fmt.Sprintf("%s/v1/reminders", server.URL)

	tests := []struct {
		payload    interface{}
		authHeader bool
		respCode   int
	}{
		{reminderRequest{Reminder: api.Reminder{Text: "test", ProfessionalID: testProfessionalID, DueDate: time.Now().Unix()}}, true, http.StatusCreated},
		{reminderRequest{Reminder: api.Reminder{Text: "test", ProfessionalID: testProfessionalID}}, true, http.StatusBadRequest},
		{reminderRequest{Reminder: api.Reminder{Text: "test", ProfessionalID: testProfessionalID}}, false, http.StatusForbidden},
		{reminderRequest{Reminder: api.Reminder{Text: "test", ProfessionalID: testProfessionalID, DueDate: time.Now().Unix()}}, true, http.StatusCreated},
		{reminderRequest{Reminder: api.Reminder{Text: "test", DueDate: time.Now().Unix()}}, true, http.StatusBadRequest},
		{reminderRequest{Reminder: api.Reminder{ProfessionalID: testProfessionalID, DueDate: time.Now().Unix()}}, true, http.StatusBadRequest},
		{reminderRequest{Reminder: api.Reminder{Text: "test"}}, true, http.StatusBadRequest},
		{reminderRequest{Reminder: api.Reminder{ProfessionalID: testProfessionalID}}, true, http.StatusBadRequest},
	}

	for _, test := range tests {
		b, err := json.Marshal(test.payload)
		if err != nil {
			t.Error(err)
			return
		}
		// Build up our request
		req, err := http.NewRequest("POST", url, bytes.NewReader(b))
		if err != nil {
			t.Error(err)
		}
		if test.authHeader {
			req.Header.Add("Authorization", testJWT)
		}
		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			t.Error(err)
		}
		if resp.StatusCode != test.respCode {
			html, _ := ioutil.ReadAll(resp.Body)
			t.Errorf("Expected a %d response got: %d, resp %s", test.respCode, resp.StatusCode, string(html))
		}
	}
}

func TestUpdateReminder(t *testing.T) {
	// set up the test server
	server := httptest.NewServer(testHandler)

	tests := []struct {
		payload    updateReminderRequest
		authHeader bool
		respCode   int
	}{
		{updateReminderRequest{Reminder: api.UpdateReminder{ID: testReminderID, Text: "test", DueDate: time.Now().Unix(), Completed: true}}, true, http.StatusOK},
		{updateReminderRequest{Reminder: api.UpdateReminder{ID: testReminderID, Text: "test", DueDate: time.Now().Unix(), Completed: true}}, false, http.StatusForbidden},
		{updateReminderRequest{Reminder: api.UpdateReminder{ID: "nonexisting", Text: "test", DueDate: time.Now().Unix(), Completed: true}}, true, http.StatusNotFound},
		{updateReminderRequest{Reminder: api.UpdateReminder{ID: testReminderID, DueDate: time.Now().Unix(), Completed: true}}, true, http.StatusBadRequest},
		{updateReminderRequest{Reminder: api.UpdateReminder{Text: "test", DueDate: time.Now().Unix(), Completed: true}}, true, http.StatusNotFound},
	}

	for _, test := range tests {

		// get the server url
		url := fmt.Sprintf("%s/v1/reminders/%s", server.URL, test.payload.Reminder.ID)

		b, err := json.Marshal(test.payload)
		if err != nil {
			t.Error(err)
			return
		}
		// Build up our request
		req, err := http.NewRequest("PUT", url, bytes.NewReader(b))
		if err != nil {
			t.Error(err)
		}
		if test.authHeader {
			req.Header.Add("Authorization", testJWT)
		}
		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			t.Error(err)
		}
		if resp.StatusCode != test.respCode {
			html, _ := ioutil.ReadAll(resp.Body)
			t.Errorf("Expected a %d response got: %d, resp %s", test.respCode, resp.StatusCode, string(html))
		}
	}
}
