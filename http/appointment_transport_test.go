package http

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"time"

	"gitlab.com/vanoapp/api"
)

func TestHandleCreateAppointment(t *testing.T) {
	services := []*api.Service{
		&api.Service{
			ID: testServiceID,
		},
	}
	start := time.Now().Add(10 * time.Minute).Unix()
	end := time.Now().Add(40 * time.Minute).Unix()
	// set up the test server
	server := httptest.NewServer(testHandler)
	// get the server url
	url := fmt.Sprintf("%s/v1/appointments", server.URL)

	tests := []struct {
		payload    appointmentRequest
		authHeader bool
		respCode   int
	}{
		{appointmentRequest{Appointment: api.Appointment{ProfessionalID: testProfessionalID, CustomerID: testCustomerID, StartTime: start, EndTime: end, Services: services}}, true, http.StatusCreated},
		{appointmentRequest{Appointment: api.Appointment{ProfessionalID: testProfessionalID, CustomerID: testCustomerID, StartTime: start, EndTime: end, Services: services}}, false, http.StatusForbidden},
		{appointmentRequest{Appointment: api.Appointment{CustomerID: testCustomerID, StartTime: start, EndTime: end, Services: services}}, true, http.StatusBadRequest},
		{appointmentRequest{Appointment: api.Appointment{ProfessionalID: testProfessionalID, StartTime: start, EndTime: end, Services: services}}, true, http.StatusBadRequest},
		{appointmentRequest{Appointment: api.Appointment{ProfessionalID: testProfessionalID, CustomerID: testCustomerID, EndTime: end, Services: services}}, true, http.StatusBadRequest},
		{appointmentRequest{Appointment: api.Appointment{ProfessionalID: testProfessionalID, CustomerID: testCustomerID, StartTime: start, Services: services}}, true, http.StatusBadRequest},
		{appointmentRequest{Appointment: api.Appointment{ProfessionalID: testProfessionalID, CustomerID: testCustomerID, StartTime: start, EndTime: end, Services: nil}}, true, http.StatusBadRequest},
	}

	for _, test := range tests {
		b, err := json.Marshal(test.payload)
		if err != nil {
			t.Error(err)
			return
		}
		// Build up our request
		req, err := http.NewRequest("POST", url, bytes.NewReader(b))
		if err != nil {
			t.Error(err)
		}

		if test.authHeader {
			req.Header.Add("Authorization", testJWT)
		}

		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			t.Error(err)
		}
		if resp.StatusCode != test.respCode {
			html, _ := ioutil.ReadAll(resp.Body)
			t.Errorf("Expected a %d response got: %d, resp %s", test.respCode, resp.StatusCode, string(html))
		}
	}
}
