package http

import (
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/vanoapp/api"
)

func (h *Handler) handleGetProfessionalAnalytics() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		professionalID := mux.Vars(r)["professional_id"]

		analytics, err := h.AnalyticsService.GetProfessionalAnalytics(professionalID)
		if err != nil {
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		resp := analyticsResponse{
			Analytics: analytics,
		}

		OK(w, resp, api.GetReqID(r))
	})
}
