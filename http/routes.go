package http

func (h *Handler) getRoutes() {
	/* APPOINTMENT ROUTES */
	const listProfessionalAppointmentsPath = "/v1/professionals/{professional_id}/appointments"
	h.Handle(listProfessionalAppointmentsPath, h.authMiddleware(h.handleListProfessionalAppointments())).Methods("GET")

	const listProfessionalBlockoffsPath = "/v1/professionals/{professional_id}/blockoffs"
	h.Handle(listProfessionalBlockoffsPath, h.authMiddleware(h.handleListProfessionalBlockoffs())).Methods("GET")

	const listCustomerAppointmentsPath = "/v1/customers/{customer_id}/appointments"
	h.Handle(listCustomerAppointmentsPath, h.authMiddleware(h.handleListCustomerAppointments())).Methods("GET")

	const listProfessionalAvailableAppointmentTimesPath = "/v1/appointments/times"
	h.Handle(listProfessionalAvailableAppointmentTimesPath, h.authMiddleware(h.handleListProfessionalAvailableAppointmentTimes())).Methods("GET")

	const getAppointmentPath = "/v1/appointments/{id}"
	h.Handle(getAppointmentPath, h.authMiddleware(h.handleGetAppointment())).Methods("GET")

	const updateAppointmentPath = "/v1/appointments/{id}"
	h.Handle(updateAppointmentPath, h.authMiddleware(h.handleUpdateAppointment())).Methods("PUT")

	const cancelAppointmentPath = "/v1/appointments/{id}"
	h.Handle(cancelAppointmentPath, h.authMiddleware(h.handleCancelAppointment())).Methods("DELETE")

	const createAppointmentPath = "/v1/appointments"
	h.Handle(createAppointmentPath, h.authMiddleware(h.handleCreateAppointment())).Methods("POST")

	const createPostAppointmentPath = "/v1/appointments/{appointment_id}/post-appointments"
	h.Handle(createPostAppointmentPath, h.authMiddleware(h.handleCreatePostAppointment())).Methods("POST")

	const updatePostAppointmentPath = "/v1/appointments/{appointment_id}/post-appointments/{id}"
	h.Handle(updatePostAppointmentPath, h.authMiddleware(h.handleUpdatePostAppointment())).Methods("PUT")

	/* COMPANY ROUTES */
	const createCompanyPath = "/v1/companies"
	h.Handle(createCompanyPath, h.handleCreateCompany()).Methods("POST")

	const getCompanyPath = "/v1/companies/{id}"
	h.Handle(getCompanyPath, h.handleGetCompany()).Methods("GET")

	const uploadCompanyImagePath = "/v1/companies/{id}/images"
	h.Handle(uploadCompanyImagePath, h.handleUploadCompanyImage()).Methods("POST")

	const deleteCompanyImagePath = "/v1/companies/{id}/images/{image_id}"
	h.Handle(deleteCompanyImagePath, h.handleDeleteCompanyImage()).Methods("DELETE")

	const listCompanyImagePath = "/v1/companies/{id}/images"
	h.Handle(listCompanyImagePath, h.handleListCompanyImages()).Methods("GET")

	const listCompaniesPath = "/v1/companies"
	h.Handle(listCompaniesPath, h.handleListCompanies()).Methods("GET")

	const updateCompanyPath = "/v1/companies/{id}"
	h.Handle(updateCompanyPath, h.authMiddleware(h.handleUpdateCompany())).Methods("PUT")

	/* Professional ROUTES */
	const checkProfessionalExistsPath = "/v1/professionals/check-exists"
	h.Handle(checkProfessionalExistsPath, h.handleCheckProfessionalExists()).Methods("GET")

	const createProfessionalPath = "/v1/professionals"
	h.Handle(createProfessionalPath, h.handleCreateProfessional()).Methods("POST")

	const activateProfessionalPath = "/v1/professionals/{id}/activate"
	h.Handle(activateProfessionalPath, h.handleActivateProfessionals()).Methods("POST")

	const listProfessionalsPath = "/v1/professionals"
	h.Handle(listProfessionalsPath, h.handleListProfessionals()).Methods("GET")

	const listCompanyProfessionalsPath = "/v1/companies/{company_id}/professionals"
	h.Handle(listCompanyProfessionalsPath, h.handleListCompanyProfessionals()).Methods("GET")

	const getProfessionalPath = "/v1/professionals/{id}"
	h.Handle(getProfessionalPath, h.handleGetProfessional()).Methods("GET")

	const deleteProfessionalPath = "/v1/professionals/{id}"
	h.Handle(deleteProfessionalPath, h.handleDeleteProfessional()).Methods("DELETE")

	const updateProfessionalFeaturePath = "/v1/professionals/{id}/features/{feature_id}"
	h.Handle(updateProfessionalFeaturePath, h.authMiddleware(h.handleUpdateProfessionalFeature())).Methods("PUT")

	const resetProfessionalPasswordPath = "/v1/professionals/{id}/reset-password"
	h.Handle(resetProfessionalPasswordPath, h.authMiddleware(h.handleResetProfessionalPassword())).Methods("PUT")

	const listProfessionalManagedProfessionalsPath = "/v1/professionals/{professional_id}/managed-professionals"
	h.Handle(listProfessionalManagedProfessionalsPath, h.authMiddleware(h.handleListProfessionalManageProfessionals())).Methods("GET")

	const listProfessionalManageeInvitationsPath = "/v1/professionals/{professional_id}/managee-invitations"
	h.Handle(listProfessionalManageeInvitationsPath, h.authMiddleware(h.handleListProfessionalManageeInvitations())).Methods("GET")

	const createManagedProfessionalPath = "/v1/managed-professionals"
	h.Handle(createManagedProfessionalPath, h.authMiddleware(h.handleCreateManagedProfessional())).Methods("POST")

	const updateManagedProfessionalPath = "/v1/managed-professionals"
	h.Handle(updateManagedProfessionalPath, h.authMiddleware(h.handleUpdateManagedProfessional())).Methods("PUT")

	const deleteManagedProfessionalPath = "/v1/managed-professionals/{id}"
	h.Handle(deleteManagedProfessionalPath, h.authMiddleware(h.handleDeleteManagedProfessional())).Methods("DELETE")

	const updateProfessionalPath = "/v1/professionals/{id}"
	h.Handle(updateProfessionalPath, h.authMiddleware(h.handleUpdateProfessional())).Methods("PUT")

	const clocInProfessionalPath = "/v1/professionals/{id}/cloc-ins"
	h.Handle(clocInProfessionalPath, h.authMiddleware(h.handleClocInProfessional())).Methods("POST")

	const getclocInsProfessionalPath = "/v1/professionals/{id}/cloc-ins"
	h.Handle(getclocInsProfessionalPath, h.authMiddleware(h.handleListProfessionalClocIns())).Methods("GET")

	const clocOutProfessionalPath = "/v1/professionals/{id}/cloc-ins/{cloc_in_id}"
	h.Handle(clocOutProfessionalPath, h.authMiddleware(h.handleClocOutProfessional())).Methods("PUT")

	/* SERVICE ROUTES */
	const createServicePath = "/v1/services"
	h.Handle(createServicePath, h.authMiddleware(h.handleCreateService())).Methods("POST")

	const getServicePath = "/v1/services/{id}"
	h.Handle(getServicePath, h.authMiddleware(h.handleGetService())).Methods("GET")

	const updateServicePath = "/v1/services/{id}"
	h.Handle(updateServicePath, h.authMiddleware(h.handleUpdateService())).Methods("PUT")

	const deleteServicePath = "/v1/services/{id}"
	h.Handle(deleteServicePath, h.authMiddleware(h.handleDeleteService())).Methods("DELETE")

	/* CUSTOMER ROUTES */
	const createCustomerPath = "/v1/customers"
	h.Handle(createCustomerPath, h.handleCreateCustomer()).Methods("POST")

	const addCustomerCompanyPath = "/v1/companies/{company_id}/customers"
	h.Handle(addCustomerCompanyPath, h.authMiddleware(h.handleAddCustomerCompany())).Methods("POST")

	const removeCustomerCompanyPath = "/v1/companies/{company_id}/customers/{customer_id}"
	h.Handle(removeCustomerCompanyPath, h.authMiddleware(h.handleRemoveCustomerCompany())).Methods("DELETE")

	const checkCustomerExistsPath = "/v1/customers/check-exists"
	h.Handle(checkCustomerExistsPath, h.handleCheckCustomerExists()).Methods("GET")

	const resetCustomerPasswordPath = "/v1/customers/{id}/reset-password"
	h.Handle(resetCustomerPasswordPath, h.authMiddleware(h.handleResetCustomerPassword())).Methods("PUT")

	const updateCustomerPath = "/v1/customers/{id}"
	h.Handle(updateCustomerPath, h.authMiddleware(h.handleUpdateCustomer())).Methods("PUT")

	const setCustomerAccountPath = "/v1/customers/{id}/set-account"
	h.Handle(setCustomerAccountPath, h.handleSetCustomerAccount()).Methods("PUT")

	const getCustomerPath = "/v1/customers/{id}"
	h.Handle(getCustomerPath, h.handleGetCustomer()).Methods("GET")

	const listCompanyCustomersPath = "/v1/companies/{company_id}/customers"
	h.Handle(listCompanyCustomersPath, h.handleListCompanyCustomers()).Methods("GET")

	const listProfessionalCustomersPath = "/v1/professionals/{professional_id}/customers"
	h.Handle(listProfessionalCustomersPath, h.handleListProfessionalCustomers()).Methods("GET")

	const addCustomerServicePath = "/v1/customers/{id}/services"
	h.Handle(addCustomerServicePath, h.authMiddleware(h.handleAddCustomerService())).Methods("POST")

	const addCustomerProfessionalPath = "/v1/professionals/{professional_id}/customers/{id}"
	h.Handle(addCustomerProfessionalPath, h.authMiddleware(h.handleAddProfessionalCustomer())).Methods("POST")

	const removeCustomerServicePath = "/v1/customers/{id}/services/{customer_service_id}"
	h.Handle(removeCustomerServicePath, h.authMiddleware(h.handleRemoveCustomerService())).Methods("DELETE")

	const listCustomerServicesPath = "/v1/customers/{id}/services"
	h.Handle(listCustomerServicesPath, h.authMiddleware(h.handleGetCustomerServices())).Methods("GET")

	/* CARDS ROUTES */
	const createCustomerCardPath = "/v1/cards"
	h.Handle(createCustomerCardPath, h.authMiddleware(h.handleAddCustomerCard())).Methods("POST")

	const deleteCustomerCardPath = "/v1/cards/{id}"
	h.Handle(deleteCustomerCardPath, h.authMiddleware(h.handleRemoveCustomerCard())).Methods("DELETE")

	const getCustomerCardPath = "/v1/customers/{id}/cards"
	h.Handle(getCustomerCardPath, h.authMiddleware(h.handleGetCustomerCards())).Methods("GET")

	/* NOTES ROUTES */
	const createCustomerNotePath = "/v1/notes"
	h.Handle(createCustomerNotePath, h.authMiddleware(h.handleCreateCustomerNote())).Methods("POST")

	const updateCustomerNotePath = "/v1/notes/{id}"
	h.Handle(updateCustomerNotePath, h.authMiddleware(h.handleUpdateCustomerNote())).Methods("PUT")

	const deleteCustomerNotePath = "/v1/notes/{id}"
	h.Handle(deleteCustomerNotePath, h.authMiddleware(h.handleDeleteCustomerNote())).Methods("DELETE")

	/* REMINDER ROUTES */
	const createReminderPath = "/v1/reminders"
	h.Handle(createReminderPath, h.authMiddleware(h.handleCreateReminder())).Methods("POST")

	const updateReminderPath = "/v1/reminders/{id}"
	h.Handle(updateReminderPath, h.authMiddleware(h.handleUpdateReminder())).Methods("PUT")

	const getReminderPath = "/v1/reminders/{id}"
	h.Handle(getReminderPath, h.authMiddleware(h.handleGetReminder())).Methods("GET")

	const deleteReminderPath = "/v1/reminders/{id}"
	h.Handle(deleteReminderPath, h.authMiddleware(h.handleDeleteReminder())).Methods("DELETE")

	const getProfessionalRemindersPath = "/v1/professionals/{professional_id}/reminders"
	h.Handle(getProfessionalRemindersPath, h.authMiddleware(h.handleListProfessionalReminders())).Methods("GET")

	/* AUTH ROUTES */
	const authenticatePath = "/v1/authenticate"
	h.Handle(authenticatePath, h.handleAuthenticate()).Methods("POST")

	const resetPasswordPath = "/v1/reset-password"
	h.Handle(resetPasswordPath, h.handleResetPassword()).Methods("POST")

	const resetPasswordRequestPath = "/v1/reset-password-request"
	h.Handle(resetPasswordRequestPath, h.handleForgotPassword()).Methods("POST")

	const refreshTokenPath = "/v1/refresh-token"
	h.Handle(refreshTokenPath, h.authMiddleware(h.handleRefreshToken())).Methods("POST")

	const blacklistTokenPath = "/v1/blacklists/tokens"
	h.Handle(blacklistTokenPath, h.authMiddleware(h.handleBlacklistToken())).Methods("POST")

	/* MERCHANT ROUTES */
	const createMerchantPath = "/v1/merchants"
	h.Handle(createMerchantPath, h.authMiddleware(h.handleCreateMerchant())).Methods("POST")

	const createTransactionPath = "/v1/transactions"
	h.Handle(createTransactionPath, h.authMiddleware(h.handleCreateTransaction())).Methods("POST")

	const getTransactionPath = "/v1/transactions/{id}"
	h.Handle(getTransactionPath, h.authMiddleware(h.handleGetTransaction())).Methods("GET")

	const refundTransactionPath = "/v1/transactions/{id}"
	h.Handle(refundTransactionPath, h.authMiddleware(h.handleRefundTransaction())).Methods("DELETE")

	const getCompanyTransactionPath = "/v1/companies/{company_id}/transactions"
	h.Handle(getCompanyTransactionPath, h.authMiddleware(h.handleGetCompanyTransactions())).Methods("GET")

	const getProfessionalTransactionsPath = "/v1/professionals/{professional_id}/transactions"
	h.Handle(getProfessionalTransactionsPath, h.authMiddleware(h.handleGetProfessionalTransactions())).Methods("GET")

	/* ANALYTICS ROUTES */
	const getProfessionalAnalyticsPath = "/v1/professionals/{professional_id}/analytics"
	h.Handle(getProfessionalAnalyticsPath, h.authMiddleware(h.handleGetProfessionalAnalytics())).Methods("GET")
}
