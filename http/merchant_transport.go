package http

import (
	"encoding/json"
	"errors"
	"net/http"

	"gitlab.com/vanoapp/api"
)

type merchantRequest struct {
	Merchant api.Merchant `json:"merchant"`
}

type merchantResponse struct {
	Merchant *api.Merchant `json:"merchant"`
}

func (h *Handler) handleCreateMerchant() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var payload merchantRequest
		if err := json.NewDecoder(r.Body).Decode(&payload); err != nil {
			Error(w, err, http.StatusBadRequest, api.GetReqID(r))
			return
		}

		m := &payload.Merchant

		// validate request
		if err := h.ValidationService.ValidateCreateMerchant(m); err != nil {
			Error(w, err, http.StatusBadRequest, api.GetReqID(r))
			return
		}

		c, err := h.CompanyService.GetCompany(m.CompanyID)
		if err != nil {
			if err == api.ErrNotFound {
				Error(w, err, http.StatusNotFound, api.GetReqID(r))
				return
			}
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		var s *api.Professional
		if m.ProfessionalID != "" {
			s, err = h.ProfessionalService.GetProfessional(m.ProfessionalID)
			if err != nil {
				Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
				return
			}

			if !s.Independent {
				Error(w, errors.New("Trying to create a merchant account for a non independent professional"), http.StatusBadRequest, api.GetReqID(r))
				return
			}
		} else {
			s, err = h.ProfessionalService.GetProfessionalQuery("select * from professionals where company_id = ? and admin_generated = ? limit 1", c.ID, false)
			if err != nil {
				Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
				return
			}
		}

		if err := h.BillingService.OnboardMerchant(m, c, s); err != nil {
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		if err := h.MerchantService.CreateMerchant(m); err != nil {
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		if m.ProfessionalID != "" {
			updated := &api.UpdateProfessional{
				ID:                 s.ID,
				MerchantIntegrated: true,
				FirstName:          s.FirstName,
				LastName:           s.LastName,
				Email:              s.Email,
				PhoneNumber:        s.PhoneNumber,
				Notification:       s.Notification,
				IPAddress:          s.IPAddress,
				Permission:         s.Permission,
				LastLogin:          s.LastLogin,
			}
			if _, err := h.ProfessionalService.UpdateProfessional(updated); err != nil {
				Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
				return
			}
		} else {
			c.MerchantIntegrated = true
			if err := h.CompanyService.UpdateCompany(c); err != nil {
				Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
				return
			}
		}

		resp := merchantResponse{Merchant: m}

		Created(w, resp, api.GetReqID(r))
	})
}
