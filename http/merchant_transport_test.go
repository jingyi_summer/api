package http

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/vanoapp/api"
)

func TestHandleCreateMerchant(t *testing.T) {
	// set up the test server
	server := httptest.NewServer(testHandler)
	// get the server url
	url := fmt.Sprintf("%s/v1/merchants", server.URL)

	tests := []struct {
		payload    merchantRequest
		authHeader bool
		respCode   int
	}{
		{merchantRequest{api.Merchant{CompanyID: testCompanyID}}, false, http.StatusForbidden},
		{merchantRequest{api.Merchant{CompanyID: testCompanyID}}, true, http.StatusBadRequest},
	}

	for i, test := range tests {
		b, err := json.Marshal(test.payload)
		if err != nil {
			t.Error(err)
			return
		}
		// Build up our request
		req, err := http.NewRequest("POST", url, bytes.NewReader(b))
		if err != nil {
			t.Error(err)
		}
		if test.authHeader {
			req.Header.Add("Authorization", testJWT)
		}
		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			t.Error(err)
		}
		if resp.StatusCode != test.respCode {
			html, _ := ioutil.ReadAll(resp.Body)
			t.Errorf("Expected a %d response got: %d, resp %s, id %d", test.respCode, resp.StatusCode, string(html), i)
		}
	}
}
