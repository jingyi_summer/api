package http

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/vanoapp/api"
)

var testProfessionalID = "correctid"

type badProfessionalRequest struct {
	Professional string
}

type removeServiceInput struct {
	ProfessionalID string
	ServiceID      string
}

type addServiceInput struct {
	ProfessionalID string
	ServiceID      string
}

func TestHandleCreateProfessional(t *testing.T) {
	// set up the test server
	server := httptest.NewServer(testHandler)
	// get the server url
	url := fmt.Sprintf("%s/v1/professionals", server.URL)

	tests := []struct {
		payload  interface{}
		respCode int
	}{
		{professionalRequest{Professional: api.Professional{FirstName: "hello", LastName: "ln", Email: "e", PhoneNumber: "pp", Password: "sss", CompanyID: "1", Permission: testPermission}}, http.StatusCreated},
		{professionalRequest{Professional: api.Professional{FirstName: "hello", LastName: "ln", Email: "e", PhoneNumber: "pp", Password: "sss", CompanyID: "1"}}, http.StatusBadRequest},
		{professionalRequest{Professional: api.Professional{LastName: "ln", Email: "e", PhoneNumber: "pp", Password: "s", CompanyID: "1", Permission: testPermission}}, http.StatusBadRequest},
		{professionalRequest{Professional: api.Professional{FirstName: "hello", Email: "e", PhoneNumber: "pp", Password: "s", CompanyID: "1", Permission: testPermission}}, http.StatusBadRequest},
		{professionalRequest{Professional: api.Professional{FirstName: "hello", LastName: "ln", PhoneNumber: "pp", Password: "s", CompanyID: "1", Permission: testPermission}}, http.StatusBadRequest},
		{professionalRequest{Professional: api.Professional{FirstName: "hello", LastName: "ln", Email: "e", Password: "s", CompanyID: "1", Permission: testPermission}}, http.StatusBadRequest},
		{professionalRequest{Professional: api.Professional{FirstName: "hello", LastName: "ln", Email: "e", PhoneNumber: "pp", CompanyID: "1", Permission: testPermission}}, http.StatusBadRequest},
		{professionalRequest{Professional: api.Professional{FirstName: "hello", LastName: "ln", Email: "e", PhoneNumber: "pp", Password: "s", Permission: testPermission}}, http.StatusBadRequest},
		{badProfessionalRequest{Professional: "muahahha"}, http.StatusBadRequest},
	}

	for _, test := range tests {
		b, err := json.Marshal(test.payload)
		if err != nil {
			t.Error(err)
			return
		}
		// Build up our request
		req, err := http.NewRequest("POST", url, bytes.NewReader(b))
		if err != nil {
			t.Error(err)
		}
		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			t.Error(err)
		}
		if resp.StatusCode != test.respCode {
			html, _ := ioutil.ReadAll(resp.Body)
			t.Errorf("Expected a %d response got: %d, resp %s", test.respCode, resp.StatusCode, string(html))
		}
	}
}

type testActivateRequest struct {
	ID       string `json:"id"`
	Password string `json:"password"`
}

func TestHandleActivateProfessional(t *testing.T) {
	// set up the test server
	server := httptest.NewServer(testHandler)

	tests := []struct {
		payload  testActivateRequest
		respCode int
	}{
		{testActivateRequest{testProfessionalID, "newpass"}, http.StatusOK},
		{testActivateRequest{"incorrect", "newpass"}, http.StatusNotFound},
		{testActivateRequest{"", "newpass"}, http.StatusBadRequest},
		{testActivateRequest{testProfessionalID, ""}, http.StatusBadRequest},
	}

	for _, test := range tests {
		b, err := json.Marshal(test.payload)
		if err != nil {
			t.Error(err)
			return
		}

		id := test.payload.ID
		if test.payload.ID == "" {
			id = "temp"
		}

		// get the server url
		url := fmt.Sprintf("%s/v1/professionals/%s/activate", server.URL, id)
		// Build up our request
		req, err := http.NewRequest("POST", url, bytes.NewReader(b))
		if err != nil {
			t.Error(err)
		}
		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			t.Error(err)
		}
		if resp.StatusCode != test.respCode {
			html, _ := ioutil.ReadAll(resp.Body)
			t.Errorf("Expected a %d response got: %d, resp %s", test.respCode, resp.StatusCode, string(html))
		}
	}
}

func TestHandleUpdateProfessional(t *testing.T) {
	// set up the test server
	server := httptest.NewServer(testHandler)
	// get the server url
	url := fmt.Sprintf("%s/v1/professionals/{id}", server.URL)

	tests := []struct {
		payload    interface{}
		authHeader bool
		respCode   int
	}{
		{professionalRequest{Professional: api.Professional{ID: testProfessionalID, FirstName: "hello", LastName: "ln", Email: "e", PhoneNumber: "pp", Notification: testNotification, Permission: testPermission}}, true, http.StatusOK},
		{professionalRequest{Professional: api.Professional{ID: testProfessionalID, FirstName: "hello", LastName: "ln", Email: "e", PhoneNumber: "pp", Notification: testNotification}}, true, http.StatusBadRequest},
		{professionalRequest{Professional: api.Professional{ID: testProfessionalID, FirstName: "hello", LastName: "ln", Email: "e", PhoneNumber: "pp", Notification: testNotification, Permission: testPermission}}, false, http.StatusForbidden},
		{professionalRequest{Professional: api.Professional{ID: testProfessionalID, FirstName: "hello", LastName: "ln", Email: "e", PhoneNumber: "pp", Permission: testPermission}}, true, http.StatusBadRequest},
		{professionalRequest{Professional: api.Professional{ID: testProfessionalID, FirstName: "hello", LastName: "ln", Email: "e", PhoneNumber: "pp", Permission: testPermission}}, false, http.StatusForbidden},
		{professionalRequest{Professional: api.Professional{ID: testProfessionalID, FirstName: "hello", LastName: "ln", Email: "e", Notification: testNotification, Permission: testPermission}}, true, http.StatusBadRequest},
		{professionalRequest{Professional: api.Professional{ID: "nop", FirstName: "hello", LastName: "ln", Email: "e", PhoneNumber: "pp", Notification: testNotification, Permission: testPermission}}, true, http.StatusNotFound},
		{professionalRequest{Professional: api.Professional{FirstName: "hello", LastName: "ln", Email: "e", PhoneNumber: "pp", Notification: testNotification, Permission: testPermission}}, true, http.StatusBadRequest},
		{professionalRequest{Professional: api.Professional{ID: testProfessionalID, LastName: "ln", Email: "e", PhoneNumber: "pp", Notification: testNotification, Permission: testPermission}}, true, http.StatusBadRequest},
		{professionalRequest{Professional: api.Professional{ID: testProfessionalID, FirstName: "hello", Email: "e", PhoneNumber: "pp", Notification: testNotification, Permission: testPermission}}, true, http.StatusBadRequest},
		{professionalRequest{Professional: api.Professional{ID: testProfessionalID, FirstName: "hello", LastName: "ln", PhoneNumber: "pp", Notification: testNotification, Permission: testPermission}}, true, http.StatusBadRequest},
		{badProfessionalRequest{Professional: "muahahha"}, true, http.StatusBadRequest},
	}

	for i, test := range tests {
		b, err := json.Marshal(test.payload)
		if err != nil {
			t.Error(err)
			return
		}
		// Build up our request
		req, err := http.NewRequest("PUT", url, bytes.NewReader(b))
		if err != nil {
			t.Error(err)
		}
		if test.authHeader {
			req.Header.Add("Authorization", testJWT)
		}
		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			t.Error(err)
		}
		if resp.StatusCode != test.respCode {
			html, _ := ioutil.ReadAll(resp.Body)
			t.Errorf("Expected a %d response got: %d, resp %s, id is %d", test.respCode, resp.StatusCode, string(html), i+1)
		}
	}
}

func TestListProfessional(t *testing.T) {
	// set up the test server
	server := httptest.NewServer(testHandler)

	tests := []struct {
		limit    int
		respCode int
	}{
		{5, http.StatusOK},
	}

	for _, test := range tests {
		url := fmt.Sprintf("%s/v1/professionals?limit=%d", server.URL, test.limit)
		// Build up our request
		req, err := http.NewRequest("GET", url, nil)
		if err != nil {
			t.Error(err)
		}
		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			t.Error(err)
		}
		if resp.StatusCode != test.respCode {
			html, _ := ioutil.ReadAll(resp.Body)
			t.Errorf("Expected a %d response got: %d, resp %s", test.respCode, resp.StatusCode, string(html))
		}
	}
}

func TestListCompanyProfessional(t *testing.T) {
	// set up the test server
	server := httptest.NewServer(testHandler)

	tests := []struct {
		companyID  string
		authHeader bool
		respCode   int
	}{
		{testCompanyID, true, http.StatusOK},
		{testCompanyID, false, http.StatusForbidden},
		{"iconrectid", true, http.StatusNotFound},
		{"iconrectid", false, http.StatusForbidden},
	}

	for _, test := range tests {
		url := fmt.Sprintf("%s/v1/companies/%s/professionals", server.URL, test.companyID)
		// Build up our request
		req, err := http.NewRequest("GET", url, nil)
		if err != nil {
			t.Error(err)
		}
		if test.authHeader {
			req.Header.Add("Authorization", testJWT)
		}
		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			t.Error(err)
		}
		if resp.StatusCode != test.respCode {
			html, _ := ioutil.ReadAll(resp.Body)
			t.Errorf("Expected a %d response got: %d, resp %s", test.respCode, resp.StatusCode, string(html))
		}
	}
}

func TestGetProfessional(t *testing.T) {
	// set up the test server
	server := httptest.NewServer(testHandler)

	tests := []struct {
		id       string
		respCode int
	}{
		{testProfessionalID, http.StatusOK},
		{"incorrectid", http.StatusNotFound},
	}

	for _, test := range tests {
		url := fmt.Sprintf("%s/v1/professionals/%s", server.URL, test.id)
		// Build up our request
		req, err := http.NewRequest("GET", url, nil)
		if err != nil {
			t.Error(err)
		}
		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			t.Error(err)
		}
		if resp.StatusCode != test.respCode {
			html, _ := ioutil.ReadAll(resp.Body)
			t.Errorf("Expected a %d response got: %d, resp %s", test.respCode, resp.StatusCode, string(html))
		}
	}
}

func TestHandleResetProfessionalPassword(t *testing.T) {
	// set up the test server
	server := httptest.NewServer(testHandler)

	tests := []struct {
		req        resetPasswordRequest
		authHeader bool
		respCode   int
	}{
		{resetPasswordRequest{testProfessionalID, "correctpassword", "newpassword"}, true, http.StatusOK},
		{resetPasswordRequest{testProfessionalID, "correctpassword", "newpassword"}, false, http.StatusForbidden},
		{resetPasswordRequest{"incorrect", "cp", "np"}, true, http.StatusNotFound},
		{resetPasswordRequest{"incorrect", "cp", "np"}, false, http.StatusForbidden},
	}

	for _, test := range tests {
		url := fmt.Sprintf("%s/v1/professionals/%s/reset-password", server.URL, test.req.ID)

		b, err := json.Marshal(test.req)
		if err != nil {
			t.Error(err)
			return
		}
		// Build up our request
		req, err := http.NewRequest("PUT", url, bytes.NewReader(b))
		if err != nil {
			t.Error(err)
		}
		if test.authHeader {
			req.Header.Add("Authorization", testJWT)
		}
		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			t.Error(err)
		}
		if resp.StatusCode != test.respCode {
			html, _ := ioutil.ReadAll(resp.Body)
			t.Errorf("Expected a %d response got: %d, resp %s", test.respCode, resp.StatusCode, string(html))
		}
	}
}

func TestHandleCheckProfessionalExists(t *testing.T) {
	// set up the test server
	server := httptest.NewServer(testHandler)

	tests := []struct {
		req      testCheckExistsRequest
		respCode int
	}{
		{testCheckExistsRequest{Email: "correctemail", PhoneNumber: "correctnumber"}, http.StatusOK},
		{testCheckExistsRequest{Email: "correctemail", PhoneNumber: "incorrectnumber"}, http.StatusOK},
		{testCheckExistsRequest{Email: "incorrect", PhoneNumber: "correctnumber"}, http.StatusOK},
		{testCheckExistsRequest{Email: "incorrect", PhoneNumber: "incorrectnumber"}, http.StatusNotFound},
		{testCheckExistsRequest{Email: "", PhoneNumber: "incorrectnumber"}, http.StatusBadRequest},
		{testCheckExistsRequest{Email: "incorrect", PhoneNumber: ""}, http.StatusBadRequest},
	}

	for _, test := range tests {
		url := fmt.Sprintf("%s/v1/professionals/check-exists?phone_number=%s&email=%s", server.URL, test.req.PhoneNumber, test.req.Email)
		// Build up our request
		req, err := http.NewRequest("GET", url, nil)
		if err != nil {
			t.Error(err)
		}
		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			t.Error(err)
		}
		if resp.StatusCode != test.respCode {
			html, _ := ioutil.ReadAll(resp.Body)
			t.Errorf("Expected a %d response got: %d, resp %s", test.respCode, resp.StatusCode, string(html))
		}
	}
}
