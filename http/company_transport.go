package http

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"

	"googlemaps.github.io/maps"

	"github.com/gorilla/mux"

	"strconv"

	"os"

	"gitlab.com/vanoapp/api"
)

func (h *Handler) handleCreateCompany() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var payload companyRequest
		if err := json.NewDecoder(r.Body).Decode(&payload); err != nil {
			Error(w, api.ErrInvalidJSON, http.StatusBadRequest, api.GetReqID(r))
			return
		}

		c := &payload.Company

		if err := c.ValidateCreate(); err != nil {
			Error(w, err, http.StatusBadRequest, api.GetReqID(r))
			return
		}

		// Really hacky way to get a timezone
		timezone, err := getTimezone(c.City, c.State)
		if err != nil {
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		c.Timezone = timezone

		if err := h.CompanyService.CreateCompany(c); err != nil {
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		resp := companyResponse{
			Company: c,
		}

		Created(w, resp, api.GetReqID(r))
	})
}

func (h *Handler) handleGetCompany() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		p := mux.Vars(r)
		id, ok := p["id"]
		if !ok {
			Error(w, errors.New("id parameter is required"), http.StatusBadRequest, api.GetReqID(r))
			return
		}

		c, err := h.CompanyService.GetCompany(id)
		if err != nil {
			if err == api.ErrNotFound {
				Error(w, err, http.StatusNotFound, api.GetReqID(r))
				return
			}
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		resp := companyResponse{
			Company: c,
		}

		OK(w, resp, api.GetReqID(r))
	})
}

func (h *Handler) handleListCompanies() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		q := r.URL.Query()
		limitQuery := q.Get("limit")

		limit := 20
		if len(limitQuery) > 0 {
			l, err := strconv.Atoi(limitQuery)
			if err != nil {
				Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
				return
			}

			limit = l
			if l > 20 {
				limit = 20
			}
		}

		c, err := h.CompanyService.ListCompanies(limit)
		if err != nil {
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		if c == nil {
			c = []*api.Company{}
		}

		resp := companiesResponse{
			Companies: c,
		}

		OK(w, resp, api.GetReqID(r))
	})
}

func (h *Handler) handleUpdateCompany() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		p := mux.Vars(r)
		id, ok := p["id"]
		if !ok {
			Error(w, errors.New("id parameter is required"), http.StatusBadRequest, api.GetReqID(r))
			return
		}

		var payload companyRequest
		if err := json.NewDecoder(r.Body).Decode(&payload); err != nil {
			Error(w, api.ErrInvalidJSON, http.StatusBadRequest, api.GetReqID(r))
			return
		}

		if id != payload.Company.ID {
			Error(w, errors.New("id parameter and payload id do not match"), http.StatusBadRequest, api.GetReqID(r))
			return
		}

		c := &payload.Company

		if err := c.ValidateUpdate(); err != nil {
			Error(w, err, http.StatusBadRequest, api.GetReqID(r))
			return
		}

		if err := h.CompanyService.UpdateCompany(c); err != nil {
			if err == api.ErrNotFound {
				Error(w, err, http.StatusNotFound, api.GetReqID(r))
				return
			}
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		resp := companyResponse{
			Company: c,
		}

		OK(w, resp, api.GetReqID(r))
	})
}

func (h *Handler) handleUploadCompanyImage() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var payload uploadCompanyImageRequest
		if err := json.NewDecoder(r.Body).Decode(&payload); err != nil {
			Error(w, api.ErrInvalidJSON, http.StatusBadRequest, api.GetReqID(r))
			return
		}

		if payload.CompanyImage.CompanyID == "" {
			Error(w, errors.New("company id is required"), http.StatusBadRequest, api.GetReqID(r))
			return
		}

		if payload.CompanyImage.URL == "" {
			Error(w, errors.New("url is required"), http.StatusBadRequest, api.GetReqID(r))
			return
		}

		c := payload.CompanyImage

		img, err := h.CompanyService.AddCompanyImage(c)
		if err != nil {
			if err == api.ErrNotFound {
				Error(w, err, http.StatusNotFound, api.GetReqID(r))
				return
			}
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		resp := uploadCompanyImageResponse{
			CompanyImage: img,
		}

		Created(w, resp, api.GetReqID(r))
	})
}

func (h *Handler) handleDeleteCompanyImage() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		imageID := mux.Vars(r)["image_id"]

		if err := h.CompanyService.RemoveCompanyImage(imageID); err != nil {
			if err == api.ErrNotFound {
				Error(w, err, http.StatusNotFound, api.GetReqID(r))
				return
			}
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		resp := messageResponse{
			Message: "image was deleted",
		}

		OK(w, resp, api.GetReqID(r))
	})
}

func (h *Handler) handleListCompanyImages() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		companyID := mux.Vars(r)["id"]

		imgs, err := h.CompanyService.ListCompanyImages(companyID)
		if err != nil {
			if err == api.ErrNotFound {
				Error(w, err, http.StatusNotFound, api.GetReqID(r))
				return
			}
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		if imgs == nil {
			imgs = []*api.CompanyImage{}
		}

		resp := companyImagesResponse{
			CompanyImages: imgs,
		}

		OK(w, resp, api.GetReqID(r))
	})
}

func getTimezone(city, state string) (string, error) {
	c, err := maps.NewClient(maps.WithAPIKey(os.Getenv("GOOGLE_MAPS_API_KEY")))
	if err != nil {
		return "", err
	}

	req := &maps.GeocodingRequest{
		Address: fmt.Sprintf("%s %s", city, state),
	}

	resp, err := c.Geocode(context.Background(), req)
	if err != nil {
		return "", err
	}

	location := resp[0].Geometry.Location

	timezoneReq := &maps.TimezoneRequest{
		Location: &location,
	}

	tResp, err := c.Timezone(context.Background(), timezoneReq)
	if err != nil {
		return "", nil
	}

	return tResp.TimeZoneID, nil
}
