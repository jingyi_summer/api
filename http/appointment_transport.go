package http

import (
	"encoding/json"
	"errors"
	"net/http"
	"strconv"

	"gitlab.com/vanoapp/api"

	"github.com/gorilla/mux"
)

func (h *Handler) handleCreateAppointment() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var payload appointmentRequest
		if err := json.NewDecoder(r.Body).Decode(&payload); err != nil {
			Error(w, api.ErrInvalidJSON, http.StatusBadRequest, api.GetReqID(r))
			return
		}

		a := &payload.Appointment

		if err := h.ValidationService.ValidateCreateAppointment(a); err != nil {
			Error(w, err, http.StatusBadRequest, api.GetReqID(r))
			return
		}

		if err := h.AppointmentService.CreateAppointment(a); err != nil {
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		resp := appointmentResponse{
			Appointment: a,
		}

		Created(w, resp, api.GetReqID(r))
	})
}

func (h *Handler) handleListProfessionalAppointments() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		professionalID := mux.Vars(r)["professional_id"]
		q := r.URL.Query()
		startTime := q.Get("start_time")
		endTime := q.Get("end_time")
		limit := q.Get("limit")
		h.Logger.Info().Str("req_id", api.GetReqID(r)).Str("professional_id", professionalID).Str("start_time", startTime).Str("end_time", endTime).Str("limit", limit).Msg("query parameters")

		// clean this up fiure out a better validation method since validating on the struct like `api.Appointment` doesn't work in this scenario

		if startTime == "" || endTime == "" || limit == "" {
			Error(w, errors.New("start_time, end_time, and limit are required"), http.StatusBadRequest, api.GetReqID(r))
			return
		}

		if !isValidTimestamp(startTime) || !isValidTimestamp(endTime) {
			Error(w, errors.New("start_time, end_time must be valid timestamps"), http.StatusBadRequest, api.GetReqID(r))
			return
		}

		convertedStartTime, err := strconv.Atoi(startTime)
		if err != nil {
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		convertedEndTime, err := strconv.Atoi(endTime)
		if err != nil {
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		convertedLimit, err := strconv.Atoi(limit)
		if err != nil {
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		appointments, err := h.AppointmentService.ListProfessionalAppointments(professionalID, int64(convertedStartTime), int64(convertedEndTime), convertedLimit)
		if err != nil {
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}
		h.Logger.Info().Str("req_id", api.GetReqID(r)).Interface("appointments", appointments).Msg("database appointments response")

		if appointments == nil {
			appointments = []*api.Appointment{}
		}

		resp := appointmentsResponse{
			Appointments: appointments,
		}

		OK(w, resp, api.GetReqID(r))
	})
}

func (h *Handler) handleListProfessionalBlockoffs() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		id := mux.Vars(r)["professional_id"]

		a, err := h.AppointmentService.ListProfessionalBlockoffs(id)
		if err != nil {
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		if a == nil {
			a = []*api.Appointment{}
		}

		resp := appointmentsResponse{Appointments: a}

		OK(w, resp, api.GetReqID(r))
	})
}

type avaibaleTimesResponse struct {
	Times []string `json:"times"`
}

func (h *Handler) handleListProfessionalAvailableAppointmentTimes() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		q := r.URL.Query()
		date := q.Get("date")
		length := q.Get("length")
		professionalID := q.Get("professional_id")

		if date == "" || date == "0" || length == "" || length == "0" || professionalID == "" {
			Error(w, errors.New("date, length, and professional_id are required"), http.StatusBadRequest, api.GetReqID(r))
			return
		}

		intLength, err := strconv.ParseInt(length, 10, 64)
		if err != nil {
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		intDate, err := strconv.ParseInt(date, 10, 64)
		if err != nil {
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		times, err := h.AppointmentService.ListProfessionalAvaibaleAppointmentTimes(professionalID, intLength, intDate)
		if err != nil {
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		resp := &avaibaleTimesResponse{Times: times}

		OK(w, resp, api.GetReqID(r))
	})
}

func (h *Handler) handleListCustomerAppointments() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		customerID := mux.Vars(r)["customer_id"]

		q := r.URL.Query()
		startTime := q.Get("start_time")
		endTime := q.Get("end_time")
		limit := q.Get("limit")

		// clean this up fiure out a better validation method since validating on the struct like `api.Appointment` doesn't work in this scenario

		if startTime == "" || endTime == "" || limit == "" {
			Error(w, errors.New("start_time, end_time, and limit are required"), http.StatusBadRequest, api.GetReqID(r))
			return
		}

		if !isValidTimestamp(startTime) || !isValidTimestamp(endTime) {
			Error(w, errors.New("start_time, end_time must be valid timestamps"), http.StatusBadRequest, api.GetReqID(r))
			return
		}

		convertedStartTime, err := strconv.Atoi(startTime)
		if err != nil {
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		convertedEndTime, err := strconv.Atoi(endTime)
		if err != nil {
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		convertedLimit, err := strconv.Atoi(limit)
		if err != nil {
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		appointments, err := h.AppointmentService.ListCustomerAppointments(customerID, int64(convertedStartTime), int64(convertedEndTime), convertedLimit)
		if err != nil {
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		if appointments == nil {
			appointments = []*api.Appointment{}
		}

		resp := appointmentsResponse{
			Appointments: appointments,
		}

		OK(w, resp, api.GetReqID(r))
	})

}

func (h *Handler) handleGetAppointment() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		id := mux.Vars(r)["id"]

		appointment, err := h.AppointmentService.GetAppointment(id)
		if err != nil {
			if err == api.ErrNotFound {
				Error(w, err, http.StatusNotFound, api.GetReqID(r))
				return
			}
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		resp := appointmentResponse{
			Appointment: appointment,
		}

		OK(w, resp, api.GetReqID(r))
	})
}

func (h *Handler) handleUpdateAppointment() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var payload appointmentRequest
		if err := json.NewDecoder(r.Body).Decode(&payload); err != nil {
			Error(w, api.ErrInvalidJSON, http.StatusBadRequest, api.GetReqID(r))
			return
		}

		a := &payload.Appointment

		if err := a.ValidateUpdate(); err != nil {
			Error(w, err, http.StatusBadRequest, api.GetReqID(r))
			return
		}

		if err := h.AppointmentService.UpdateAppointment(a); err != nil {
			if err == api.ErrNotFound {
				Error(w, err, http.StatusNotFound, api.GetReqID(r))
				return
			}
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		resp := appointmentResponse{
			Appointment: a,
		}

		OK(w, resp, api.GetReqID(r))
	})
}

func (h *Handler) handleCancelAppointment() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		id := mux.Vars(r)["id"]

		if err := h.AppointmentService.CancelAppointment(id); err != nil {
			if err == api.ErrNotFound {
				Error(w, err, http.StatusNotFound, api.GetReqID(r))
				return
			}
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		resp := messageResponse{
			Message: "Appointment was cancelled",
		}

		OK(w, resp, api.GetReqID(r))
	})
}

func (h *Handler) handleCreatePostAppointment() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var payload postAppointmentRequest
		if err := json.NewDecoder(r.Body).Decode(&payload); err != nil {
			Error(w, api.ErrInvalidJSON, http.StatusBadRequest, api.GetReqID(r))
			return
		}

		if err := payload.validateCreate(); err != nil {
			Error(w, err, http.StatusBadRequest, api.GetReqID(r))
			return
		}

		p, err := h.AppointmentService.CreatePostAppointment(payload.PostAppointment)
		if err != nil {
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		resp := postAppointmentResponse{
			PostAppointment: p,
		}

		Created(w, resp, api.GetReqID(r))
	})
}

func (h *Handler) handleUpdatePostAppointment() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var payload postAppointmentRequest
		if err := json.NewDecoder(r.Body).Decode(&payload); err != nil {
			Error(w, api.ErrInvalidJSON, http.StatusBadRequest, api.GetReqID(r))
			return
		}

		if err := payload.validateUpdate(); err != nil {
			Error(w, err, http.StatusBadRequest, api.GetReqID(r))
			return
		}

		p, err := h.AppointmentService.UpdatePostAppointment(payload.PostAppointment)
		if err != nil {
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		resp := postAppointmentResponse{
			PostAppointment: p,
		}

		OK(w, resp, api.GetReqID(r))
	})
}
