package http

import (
	"encoding/json"
	"errors"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"

	"gitlab.com/vanoapp/api"
)

func (h *Handler) handleCreateCustomer() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var payload api.NewCustomerRequest
		if err := json.NewDecoder(r.Body).Decode(&payload); err != nil {
			Error(w, api.ErrInvalidJSON, http.StatusBadRequest, api.GetReqID(r))
			return
		}

		if err := payload.Validate(); err != nil {
			Error(w, err, http.StatusBadRequest, api.GetReqID(r))
			return
		}

		c := &api.Customer{
			FirstName:   payload.Customer.FirstName,
			LastName:    payload.Customer.LastName,
			Email:       payload.Customer.Email,
			PhoneNumber: payload.Customer.PhoneNumber,
		}

		if err := h.BillingService.CreateMerchantCustomer(c); err != nil {
			if err != api.ErrMerchantNotIntegrated {
				Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
				return
			}
		}

		payload.Customer.SplashID = c.SplashID
		customer, err := h.CustomerService.CreateCustomer(payload.Customer)
		if err != nil {
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		resp := customerResponse{Customer: sanitizeCustomer(customer)}

		Created(w, resp, api.GetReqID(r))
	})
}

func (h *Handler) handleAddCustomerCompany() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var payload customerCompanyRequest
		if err := json.NewDecoder(r.Body).Decode(&payload); err != nil {
			Error(w, api.ErrInvalidJSON, http.StatusBadRequest, api.GetReqID(r))
			return
		}

		if payload.CompanyID == "" || payload.CustomerID == "" {
			Error(w, errors.New("company id and customer id are required"), http.StatusBadRequest, api.GetReqID(r))
			return
		}

		if err := h.CustomerService.AddCustomerCompany(payload.CustomerID, payload.CompanyID); err != nil {
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		resp := messageResponse{Message: "Added company to customer"}

		Created(w, resp, api.GetReqID(r))
	})
}

type professionalCustomerRequest struct {
	CustomerID     string `json:"customer_id"`
	ProfessionalID string `json:"professional_id"`
}

func (h *Handler) handleAddProfessionalCustomer() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var payload professionalCustomerRequest
		if err := json.NewDecoder(r.Body).Decode(&payload); err != nil {
			Error(w, api.ErrInvalidJSON, http.StatusBadRequest, api.GetReqID(r))
			return
		}

		if payload.ProfessionalID == "" || payload.CustomerID == "" {
			Error(w, errors.New("professional id and customer id are required"), http.StatusBadRequest, api.GetReqID(r))
			return
		}

		if err := h.CustomerService.AddProfessionalCustomer(payload.CustomerID, payload.ProfessionalID); err != nil {
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		resp := messageResponse{Message: "Added professional to customer"}

		Created(w, resp, api.GetReqID(r))
	})
}

func (h *Handler) handleRemoveCustomerCompany() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		customerID := mux.Vars(r)["customer_id"]
		companyID := mux.Vars(r)["company_id"]

		if companyID == "" || customerID == "" {
			Error(w, errors.New("company id and customer id are required"), http.StatusBadRequest, api.GetReqID(r))
			return
		}

		if err := h.CustomerService.RemoveCustomerCompany(customerID, companyID); err != nil {
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		resp := messageResponse{Message: "Removed company customer"}

		OK(w, resp, api.GetReqID(r))
	})
}

func (h *Handler) handleUpdateCustomer() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var payload customerRequest
		if err := json.NewDecoder(r.Body).Decode(&payload); err != nil {
			Error(w, api.ErrInvalidJSON, http.StatusBadRequest, api.GetReqID(r))
			return
		}

		c := &payload.Customer

		if err := c.ValidateUpdate(); err != nil {
			Error(w, err, http.StatusBadRequest, api.GetReqID(r))
			return
		}

		if err := h.CustomerService.UpdateCustomer(c); err != nil {
			if err == api.ErrNotFound {
				Error(w, err, http.StatusNotFound, api.GetReqID(r))
				return
			}
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		resp := customerResponse{Customer: sanitizeCustomer(c)}

		OK(w, resp, api.GetReqID(r))
	})
}

func (h *Handler) handleCreateCustomerNote() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var payload noteRequest
		if err := json.NewDecoder(r.Body).Decode(&payload); err != nil {
			Error(w, api.ErrInvalidJSON, http.StatusBadRequest, api.GetReqID(r))
			return
		}

		n := &payload.Note

		if err := n.ValidateCreate(); err != nil {
			Error(w, err, http.StatusBadRequest, api.GetReqID(r))
			return
		}

		if err := h.CustomerService.AddCustomerNote(n); err != nil {
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		resp := noteResponse{Note: n}

		Created(w, resp, api.GetReqID(r))
	})
}

func (h *Handler) handleGetCustomer() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		p := mux.Vars(r)
		id, ok := p["id"]
		if !ok {
			Error(w, errors.New("id parameter is required"), http.StatusBadRequest, api.GetReqID(r))
			return
		}

		c, err := h.CustomerService.GetCustomer(id)
		if err != nil {
			if err == api.ErrNotFound {
				Error(w, err, http.StatusNotFound, api.GetReqID(r))
				return
			}
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		resp := customerResponse{Customer: sanitizeCustomer(c)}

		OK(w, resp, api.GetReqID(r))
	})
}

func (h *Handler) handleListCompanyCustomers() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		page := 1
		q := r.URL.Query()
		pageParam := q.Get("page")
		if len(pageParam) > 0 {
			intPage, err := strconv.Atoi(pageParam)
			if err != nil {
				Error(w, errors.New("invalid page query format"), http.StatusBadRequest, api.GetReqID(r))
				return
			}
			page = intPage
		}

		p := mux.Vars(r)
		companyID, ok := p["company_id"]
		if !ok {
			Error(w, errors.New("company id parameter is required"), http.StatusBadRequest, api.GetReqID(r))
			return
		}

		c, err := h.CustomerService.ListCompanyCustomers(companyID, page)
		if err != nil {
			if err == api.ErrNotFound {
				Error(w, err, http.StatusNotFound, api.GetReqID(r))
				return
			}
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		if c == nil {
			c = []*api.Customer{}
		}

		resp := customersResponse{Customers: sanitizeCustomers(c)}

		OK(w, resp, api.GetReqID(r))
	})
}

func (h *Handler) handleListProfessionalCustomers() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		page := 1
		q := r.URL.Query()
		pageParam := q.Get("page")
		if len(pageParam) > 0 {
			intPage, err := strconv.Atoi(pageParam)
			if err != nil {
				Error(w, errors.New("invalid page query format"), http.StatusBadRequest, api.GetReqID(r))
				return
			}
			page = intPage
		}

		p := mux.Vars(r)
		professionalID, ok := p["professional_id"]
		if !ok {
			Error(w, errors.New("company id parameter is required"), http.StatusBadRequest, api.GetReqID(r))
			return
		}

		c, err := h.CustomerService.ListProfessionalCustomers(professionalID, page)
		if err != nil {
			if err == api.ErrNotFound {
				Error(w, err, http.StatusNotFound, api.GetReqID(r))
				return
			}
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		if c == nil {
			c = []*api.Customer{}
		}

		resp := customersResponse{Customers: sanitizeCustomers(c)}

		OK(w, resp, api.GetReqID(r))
	})
}

func (h *Handler) handleUpdateCustomerNote() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var payload noteRequest
		if err := json.NewDecoder(r.Body).Decode(&payload); err != nil {
			Error(w, api.ErrInvalidJSON, http.StatusBadRequest, api.GetReqID(r))
			return
		}

		n := &payload.Note

		if err := n.ValidateUpdate(); err != nil {
			Error(w, err, http.StatusBadRequest, api.GetReqID(r))
			return
		}

		if err := h.CustomerService.UpdateCustomerNote(n); err != nil {
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		resp := noteResponse{Note: n}

		OK(w, resp, api.GetReqID(r))
	})
}

func (h *Handler) handleDeleteCustomerNote() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		id := mux.Vars(r)["id"]

		if err := h.CustomerService.RemoveCustomerNote(id); err != nil {
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		resp := messageResponse{Message: "note deleted"}

		OK(w, resp, api.GetReqID(r))
	})
}

func (h *Handler) handleCheckCustomerExists() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		q := r.URL.Query()
		phoneNumber := q.Get("phone_number")
		email := q.Get("email")

		if email == "" || phoneNumber == "" {
			Error(w, errors.New("phone number and email address are required"), http.StatusBadRequest, api.GetReqID(r))
			return
		}

		c, err := h.CustomerService.CheckCustomerExists(email, phoneNumber)
		if err != nil {
			if err == api.ErrNotFound {
				Error(w, err, http.StatusNotFound, api.GetReqID(r))
				return
			}
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		resp := customerResponse{Customer: sanitizeCustomer(c)}

		OK(w, resp, api.GetReqID(r))
	})
}

func (h *Handler) handleAddCustomerCard() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var payload cardRequest
		if err := json.NewDecoder(r.Body).Decode(&payload); err != nil {
			Error(w, api.ErrInvalidJSON, http.StatusBadRequest, api.GetReqID(r))
			return
		}

		c := &payload.Card

		if err := c.ValidateCreate(); err != nil {
			Error(w, err, http.StatusBadRequest, api.GetReqID(r))
			return
		}

		if err := h.BillingService.CreateToken(c); err != nil {
			if err != api.ErrMerchantNotIntegrated {
				Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
				return
			}
		}

		if err := h.CustomerService.AddCustomerCard(c); err != nil {
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		resp := cardResponse{Card: sanitizeCustomerCard(c)}

		Created(w, resp, api.GetReqID(r))
	})
}

func (h *Handler) handleRemoveCustomerCard() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		id := mux.Vars(r)["id"]

		if err := h.CustomerService.RemoveCustomerCard(id); err != nil {
			if err == api.ErrNotFound {
				Error(w, err, http.StatusNotFound, api.GetReqID(r))
				return
			}
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		resp := messageResponse{Message: "card deleted"}

		OK(w, resp, api.GetReqID(r))
	})
}

func (h *Handler) handleGetCustomerCards() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		id := mux.Vars(r)["id"]

		c, err := h.CustomerService.GetCustomerCards(id)
		if err != nil {
			if err == api.ErrNotFound {
				Error(w, err, http.StatusNotFound, api.GetReqID(r))
				return
			}
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		if c == nil {
			c = []*api.CustomerCard{}
		}

		resp := cardsResponse{Cards: sanitizeCustomerCards(c)}

		OK(w, resp, api.GetReqID(r))
	})
}

func (h *Handler) handleResetCustomerPassword() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var payload resetPasswordRequest
		if err := json.NewDecoder(r.Body).Decode(&payload); err != nil {
			Error(w, api.ErrInvalidJSON, http.StatusBadRequest, api.GetReqID(r))
			return
		}

		// take this out of here!!!!!! I gotta figure out a validation method. Either middleware or adding a method to the reuqest structs
		switch {
		case payload.ID == "":
			Error(w, errors.New("id is required"), http.StatusBadRequest, api.GetReqID(r))
			return
		case payload.Password == "":
			Error(w, errors.New("password is required"), http.StatusBadRequest, api.GetReqID(r))
			return
		case payload.NewPassword == "":
			Error(w, errors.New("new password is required"), http.StatusBadRequest, api.GetReqID(r))
			return
		}

		if err := h.CustomerService.ResetPassword(payload.ID, payload.Password, payload.NewPassword); err != nil {
			if err == api.ErrNotFound {
				Error(w, err, http.StatusNotFound, api.GetReqID(r))
				return
			}
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		resp := messageResponse{
			Message: "password was updated",
		}

		OK(w, resp, api.GetReqID(r))
	})
}

func (h *Handler) handleSetCustomerAccount() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var payload setAccountRequest
		if err := json.NewDecoder(r.Body).Decode(&payload); err != nil {
			Error(w, api.ErrInvalidJSON, http.StatusBadRequest, api.GetReqID(r))
			return
		}

		// take this out of here!!!!!! I gotta figure out a validation method. Either middleware or adding a method to the reuqest structs
		switch {
		case payload.ID == "":
			Error(w, errors.New("id is required"), http.StatusBadRequest, api.GetReqID(r))
			return
		case payload.Password == "":
			Error(w, errors.New("password is required"), http.StatusBadRequest, api.GetReqID(r))
			return
		}

		if err := h.CustomerService.SetAccount(payload.ID, payload.Password); err != nil {
			if err == api.ErrNotFound {
				Error(w, err, http.StatusNotFound, api.GetReqID(r))
				return
			}
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		resp := messageResponse{
			Message: "account was set",
		}

		OK(w, resp, api.GetReqID(r))
	})
}

type customerServiceRequest struct {
	CustomerID string `json:"customer_id"`
	ServiceID  string `json:"service_id"`
	Length     int    `json:"length"`
}

type customerServicesResponse struct {
	CustomerServices []*api.CustomerServicesTable `json:"customer_services"`
}

type customerServiceResponse struct {
	CustomerService *api.CustomerServicesTable `json:"customer_service"`
}

func (h *Handler) handleAddCustomerService() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var payload customerServiceRequest
		if err := json.NewDecoder(r.Body).Decode(&payload); err != nil {
			Error(w, api.ErrInvalidJSON, http.StatusBadRequest, api.GetReqID(r))
			return
		}

		if payload.CustomerID == "" || payload.ServiceID == "" || payload.Length == 0 {
			Error(w, errors.New("customer id, service id, and length are required"), http.StatusBadRequest, api.GetReqID(r))
			return
		}

		s, err := h.CustomerService.AddServiceToCustomer(payload.CustomerID, payload.ServiceID, payload.Length)
		if err != nil {
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		resp := customerServiceResponse{CustomerService: s}

		Created(w, resp, api.GetReqID(r))
	})
}

func (h *Handler) handleRemoveCustomerService() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		customerServiceID := mux.Vars(r)["customer_service_id"]

		if err := h.CustomerService.RemoveServiceFromCustomer(customerServiceID); err != nil {
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		resp := messageResponse{Message: "service remove from client"}

		OK(w, resp, api.GetReqID(r))
	})
}

func (h *Handler) handleGetCustomerServices() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		customerID := mux.Vars(r)["id"]

		s, err := h.CustomerService.ListCustomerServices(customerID)
		if err != nil {
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		if s == nil {
			s = []*api.CustomerServicesTable{}
		}

		resp := customerServicesResponse{CustomerServices: s}

		OK(w, resp, api.GetReqID(r))
	})
}

func sanitizeCustomer(c *api.Customer) *api.SanitizedCustomer {
	var ids []string
	for _, company := range c.Companies {
		ids = append(ids, company.ID)
	}

	if c.Notes == nil {
		c.Notes = []*api.CustomerNote{}
	}

	if c.Cards == nil {
		c.Cards = []*api.CustomerCard{}
	}

	sc := &api.SanitizedCustomer{
		ID:           c.ID,
		Companies:    ids,
		FirstName:    c.FirstName,
		LastName:     c.LastName,
		Email:        c.Email,
		PhoneNumber:  c.PhoneNumber,
		Notes:        c.Notes,
		Cards:        c.Cards,
		Notification: c.Notification,
	}

	if c.Password != "" {
		sc.PasswordSet = true
	}

	return sc
}

func sanitizeCustomers(c []*api.Customer) []*api.SanitizedCustomer {
	sc := make([]*api.SanitizedCustomer, len(c))
	for x, u := range c {
		nu := sanitizeCustomer(u)
		sc[x] = nu
	}

	return sc
}

func sanitizeCustomerCard(c *api.CustomerCard) *api.SanitizedCustomerCard {
	return &api.SanitizedCustomerCard{
		ID:         c.ID,
		CustomerID: c.CustomerID,
		LastFour:   c.LastFour,
		Exp:        c.Exp,
		Name:       c.Name,
		Token:      c.Token,
		Method:     c.Method,
	}
}

func sanitizeCustomerCards(c []*api.CustomerCard) []*api.SanitizedCustomerCard {
	sc := make([]*api.SanitizedCustomerCard, len(c))
	for x, u := range c {
		nu := sanitizeCustomerCard(u)
		sc[x] = nu
	}

	return sc
}
