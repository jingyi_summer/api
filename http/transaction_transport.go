package http

import (
	"encoding/json"
	"errors"
	"net/http"
	"strconv"
	"time"

	"gitlab.com/vanoapp/api"

	"github.com/gorilla/mux"
)

func (h *Handler) handleCreateTransaction() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var payload transactionRequest
		if err := json.NewDecoder(r.Body).Decode(&payload); err != nil {
			Error(w, err, http.StatusBadRequest, api.GetReqID(r))
			return
		}

		t := &payload.Transaction

		if err := t.ValidateCreate(); err != nil {
			Error(w, err, http.StatusBadRequest, api.GetReqID(r))
			return
		}

		if err := h.BillingService.Charge(t); err != nil {
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		if err := h.TransactionService.CreateTransaction(t); err != nil {
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		resp := transactionResponse{
			Transaction: t,
		}

		Created(w, resp, api.GetReqID(r))
	})
}

func (h *Handler) handleGetCompanyTransactions() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		companyID := mux.Vars(r)["company_id"]

		q := r.URL.Query()
		fromDate := time.Now().Add(-8760 * time.Hour).Format("2006-01-02")
		if len(q.Get("from_date")) > 0 {
			i, err := strconv.ParseInt(q.Get("from_date"), 10, 64)
			if err != nil {
				Error(w, errors.New("invalid from_date value"), http.StatusBadRequest, api.GetReqID(r))
				return
			}
			fromDate = time.Unix(i, 0).Format("2006-01-02")
		}

		resp, err := h.BillingService.GetCompanyTransactions(companyID, fromDate)
		if err != nil {
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		OK(w, resp, api.GetReqID(r))
	})
}

func (h *Handler) handleGetProfessionalTransactions() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		pID := mux.Vars(r)["professional_id"]

		q := r.URL.Query()
		fromDate := time.Now().Add(-8760 * time.Hour).Unix()
		if len(q.Get("from_date")) > 0 {
			i, err := strconv.ParseInt(q.Get("from_date"), 10, 64)
			if err != nil {
				Error(w, errors.New("invalid from_date value"), http.StatusBadRequest, api.GetReqID(r))
				return
			}
			fromDate = i
		}

		t, err := h.TransactionService.ListProfessionalTransactions(pID, fromDate)
		if err != nil {
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		if t == nil {
			t = []*api.Transaction{}
		}

		resp := transactionsResponse{Transactions: t}

		OK(w, resp, api.GetReqID(r))
	})
}

func (h *Handler) handleGetTransaction() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		id := mux.Vars(r)["id"]

		t, err := h.TransactionService.GetTransactionByID(id)
		if err != nil {
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		resp := transactionResponse{Transaction: t}

		OK(w, resp, api.GetReqID(r))
	})
}

func (h *Handler) handleRefundTransaction() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		id := mux.Vars(r)["id"]

		if err := h.BillingService.Refund(id); err != nil {
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		if err := h.TransactionService.RefundTransaction(id); err != nil {
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		resp := messageResponse{Message: "transaction was refunded"}

		OK(w, resp, api.GetReqID(r))
	})
}
