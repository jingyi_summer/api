package http

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/vanoapp/api"
)

var testServiceID = "correctid"

func TestHandleCreateService(t *testing.T) {
	// set up the test server
	server := httptest.NewServer(testHandler)
	// get the server url
	url := fmt.Sprintf("%s/v1/services", server.URL)

	tests := []struct {
		payload    interface{}
		authHeader bool
		respCode   int
	}{
		{&serviceRequest{Service: api.Service{ProfessionalID: testProfessionalID, Name: "test name", Description: "test desc", Length: 20, Price: 50}}, true, http.StatusCreated},
		{&serviceRequest{Service: api.Service{ProfessionalID: testProfessionalID, Name: "test name", Description: "test desc", Length: 20, Price: 50}}, false, http.StatusForbidden},
		{&serviceRequest{Service: api.Service{Name: "test name", Description: "test desc", Length: 20, Price: 50}}, true, http.StatusBadRequest},
		{&serviceRequest{Service: api.Service{Name: "test name", Description: "test desc", Length: 20, Price: 50}}, false, http.StatusForbidden},
		{&serviceRequest{Service: api.Service{ProfessionalID: testProfessionalID, Description: "test desc", Length: 20, Price: 50}}, true, http.StatusBadRequest},
		{&serviceRequest{Service: api.Service{ProfessionalID: testProfessionalID, Name: "test name", Length: 20, Price: 50}}, true, http.StatusBadRequest},
		{&serviceRequest{Service: api.Service{ProfessionalID: testProfessionalID, Name: "test name", Description: "test desc", Price: 50}}, true, http.StatusBadRequest},
		{&serviceRequest{Service: api.Service{ProfessionalID: testProfessionalID, Name: "test name", Description: "test desc", Length: 20}}, true, http.StatusBadRequest},
	}

	for _, test := range tests {
		b, err := json.Marshal(test.payload)
		if err != nil {
			t.Error(err)
			return
		}
		// Build up our request
		req, err := http.NewRequest("POST", url, bytes.NewReader(b))
		if err != nil {
			t.Error(err)
		}
		if test.authHeader {
			req.Header.Add("Authorization", testJWT)
		}
		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			t.Error(err)
		}
		if resp.StatusCode != test.respCode {
			html, _ := ioutil.ReadAll(resp.Body)
			t.Errorf("Expected a %d response got: %d, resp %s", test.respCode, resp.StatusCode, string(html))
		}
	}
}

func TestHandleGetService(t *testing.T) {
	// set up the test server
	server := httptest.NewServer(testHandler)

	tests := []struct {
		id         string
		authHeader bool
		respCode   int
	}{
		{testProfessionalID, true, http.StatusOK},
		{testProfessionalID, false, http.StatusForbidden},
		{"incorrectid", true, http.StatusNotFound},
		{"incorrectid", false, http.StatusForbidden},
	}

	for _, test := range tests {
		url := fmt.Sprintf("%s/v1/services/%s", server.URL, test.id)
		// Build up our request
		req, err := http.NewRequest("GET", url, nil)
		if err != nil {
			t.Error(err)
		}
		if test.authHeader {
			req.Header.Add("Authorization", testJWT)
		}
		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			t.Error(err)
		}
		if resp.StatusCode != test.respCode {
			html, _ := ioutil.ReadAll(resp.Body)
			t.Errorf("Expected a %d response got: %d, resp %s", test.respCode, resp.StatusCode, string(html))
		}
	}
}

func TestHandleUpdateService(t *testing.T) {
	// set up the test server
	server := httptest.NewServer(testHandler)
	// get the server url
	url := fmt.Sprintf("%s/v1/services/{id}", server.URL)

	tests := []struct {
		tid        int
		payload    interface{}
		authHeader bool
		respCode   int
	}{
		{1, &serviceRequest{Service: api.Service{ID: testServiceID, ProfessionalID: testProfessionalID, Name: "test name", Description: "test desc", Length: 20, Price: 50}}, true, http.StatusOK},
		{1, &serviceRequest{Service: api.Service{ID: testServiceID, ProfessionalID: testProfessionalID, Name: "test name", Description: "test desc", Length: 20, Price: 50}}, false, http.StatusForbidden},
		{2, &serviceRequest{Service: api.Service{ID: "incorrectid", ProfessionalID: testProfessionalID, Name: "test name", Description: "test desc", Length: 20, Price: 50}}, true, http.StatusNotFound},
		{3, &serviceRequest{Service: api.Service{ProfessionalID: testProfessionalID, Name: "test name", Description: "test desc", Length: 20, Price: 50}}, true, http.StatusBadRequest},
		{3, &serviceRequest{Service: api.Service{ProfessionalID: testProfessionalID, Name: "test name", Description: "test desc", Length: 20, Price: 50}}, false, http.StatusForbidden},
		{4, &serviceRequest{Service: api.Service{ID: testServiceID, Name: "test name", Description: "test desc", Length: 20, Price: 50}}, true, http.StatusBadRequest},
		{5, &serviceRequest{Service: api.Service{ID: testServiceID, ProfessionalID: testProfessionalID, Description: "test desc", Length: 20, Price: 50}}, true, http.StatusBadRequest},
		{6, &serviceRequest{Service: api.Service{ID: testServiceID, ProfessionalID: testProfessionalID, Name: "test name", Length: 20, Price: 50}}, true, http.StatusBadRequest},
		{7, &serviceRequest{Service: api.Service{ID: testServiceID, ProfessionalID: testProfessionalID, Name: "test name", Description: "test desc", Price: 50}}, true, http.StatusBadRequest},
		{8, &serviceRequest{Service: api.Service{ID: testServiceID, ProfessionalID: testProfessionalID, Name: "test name", Description: "test desc", Length: 20}}, true, http.StatusBadRequest},
	}

	for _, test := range tests {
		b, err := json.Marshal(test.payload)
		if err != nil {
			t.Error(err)
			return
		}
		// Build up our request
		req, err := http.NewRequest("PUT", url, bytes.NewReader(b))
		if err != nil {
			t.Error(err)
		}
		if test.authHeader {
			req.Header.Add("Authorization", testJWT)
		}
		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			t.Error(err)
		}
		if resp.StatusCode != test.respCode {
			html, _ := ioutil.ReadAll(resp.Body)
			t.Errorf("Expected a %d response got: %d, resp %s, id is %d", test.respCode, resp.StatusCode, string(html), test.tid)
		}
	}
}

func TestHandleRemoveService(t *testing.T) {
	// set up the test server
	server := httptest.NewServer(testHandler)

	tests := []struct {
		id         string
		authHeader bool
		respCode   int
	}{
		{testProfessionalID, true, http.StatusOK},
		{testProfessionalID, false, http.StatusForbidden},
		{"incorrectid", true, http.StatusNotFound},
		{"incorrectid", false, http.StatusForbidden},
	}

	for _, test := range tests {
		url := fmt.Sprintf("%s/v1/services/%s", server.URL, test.id)
		// Build up our request
		req, err := http.NewRequest("DELETE", url, nil)
		if err != nil {
			t.Error(err)
		}
		if test.authHeader {
			req.Header.Add("Authorization", testJWT)
		}
		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			t.Error(err)
		}
		if resp.StatusCode != test.respCode {
			html, _ := ioutil.ReadAll(resp.Body)
			t.Errorf("Expected a %d response got: %d, resp %s", test.respCode, resp.StatusCode, string(html))
		}
	}
}
