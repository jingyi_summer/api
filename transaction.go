package api

import (
	"errors"
	"os"
	"strconv"
	"time"

	uuid "github.com/satori/go.uuid"
)

// TransactionService is
type TransactionService interface {
	CreateTransaction(t *Transaction) error
	GetTransactionByID(id string) (*Transaction, error)
	ListProfessionalTransactions(id string, fromDate int64) ([]*Transaction, error)
	RefundTransaction(id string) error
}

// Transaction is
type Transaction struct {
	ID             string           `json:"id"`
	CustomerID     string           `json:"customer_id"`
	ProfessionalID string           `json:"professional_id"`
	Card           *TransactionCard `json:"card" sql:"-"`
	Token          string           `json:"token" sql:"-"`
	Total          int              `json:"total"`
	PaymentType    string           `json:"-"`
	SplashID       string           `json:"-"`
	AppointmentID  string           `json:"appointment_id"`
	LastFour       int              `json:"last_four"`
	CardMethod     string           `json:"card_method"`
	Refunded       bool             `json:"refunded" sql:",notnull"`
	CreatedAt      time.Time        `json:"created_at"`
	UpdatedAt      time.Time        `json:"updated_at"`
}

// New is
func (t *Transaction) New() {
	t.ID = uuid.NewV4().String()
	t.CreatedAt = time.Now()
	t.UpdatedAt = time.Now()
}

// ValidateCreate is
func (t *Transaction) ValidateCreate() error {
	switch {
	case t.ProfessionalID == "":
		return errors.New("customer id is required")
	case t.Total == 0:
		return errors.New("total is required")
	case t.Card == nil && t.Token == "":
		return errors.New("card or token is required")
	case t.Token != "" && t.CustomerID == "":
		return errors.New("customer id is required")
	}

	if t.Card != nil && t.Token == "" {
		switch {
		case t.Card.Name == "":
			return errors.New("cardholder name is required")
		case t.Card.Method == "":
			return errors.New("card method is required")
		case t.Card.Number == "":
			return errors.New("card number is required")
		case t.Card.Year == "":
			return errors.New("card expiration is required")
		case t.Card.Month == "":
			return errors.New("card expiration is required")
		case t.Card.CVV == "":
			return errors.New("card cvv is required")
		}
		allowTest := os.Getenv("DEBUG") == "true"
		return t.Card.Validate(allowTest)
	}

	return nil
}

// TransactionCard is
type TransactionCard struct {
	Name    string `json:"name"`
	Number  string `json:"number"`
	Year    string `json:"year"`
	Month   string `json:"month"`
	ZipCode int    `json:"zip_code"`
	CVV     string `json:"cvv"`
	Method  string `json:"method"`
}

// Validate returns nil or an error describing why the credit card didn't validate
// this method checks for expiration date, CCV/CVV and the credit card's numbers.
// For allowing test cards to go through, simply pass true.(bool) as the first argument
func (c *TransactionCard) Validate(test bool) error {
	month, err := strconv.Atoi(c.Month)
	if err != nil {
		return err
	}

	year, err := strconv.Atoi(c.Year)
	if err != nil {
		return err
	}

	if month < 1 || 12 < month {
		return errors.New("invalid month")
	}

	if year < time.Now().UTC().Year() {
		return errors.New("credit card has expired")
	}

	if year == time.Now().UTC().Year() && month < int(time.Now().UTC().Month()) {
		return errors.New("credit card has expired")
	}

	if len(c.CVV) < 3 || len(c.CVV) > 4 {
		return errors.New("invalid CVV")
	}

	if len(c.Number) < 13 {
		return errors.New("invalid credit card number")
	}

	switch c.Number {
	// test cards: https://stripe.com/docs/testing
	case "4242424242424242",
		"4012888888881881",
		"4111111111111111",
		"4000056655665556",
		"5555555555554444",
		"5200828282828210",
		"5105105105105100",
		"378282246310005",
		"371449635398431",
		"6011111111111117",
		"6011000990139424",
		"30569309025904",
		"38520000023237",
		"3530111333300000",
		"3566002020360505":
		if test {
			return nil
		}

		return errors.New("Test numbers are not allowed")
	}

	if !c.validateNumber() {
		return errors.New("Invalid credit card number")
	}

	return nil
}

func (c *TransactionCard) validateNumber() bool {
	var sum int
	var alternate bool

	numberLen := len(c.Number)

	if numberLen < 13 || numberLen > 19 {
		return false
	}

	for i := numberLen - 1; i > -1; i-- {
		mod, _ := strconv.Atoi(string(c.Number[i]))
		if alternate {
			mod *= 2
			if mod > 9 {
				mod = (mod % 10) + 1
			}
		}

		alternate = !alternate

		sum += mod
	}

	return sum%10 == 0
}
