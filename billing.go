package api

// BillingService deals with communication with external billing services. Splash, Stripe etc.
type BillingService interface {
	OnboardMerchant(m *Merchant, c *Company, p *Professional) error
	Charge(t *Transaction) error
	Refund(transactionID string) error
	CreateMerchantCustomer(c *Customer) error
	CreateToken(c *CustomerCard) error
	GetCompanyTransactions(merchantID, date string) (interface{}, error)
}
