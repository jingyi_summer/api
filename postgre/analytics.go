package postgre

import (
	"fmt"
	"log"
	"strconv"
	"strings"
	"time"

	"gitlab.com/vanoapp/api"
	pg "gopkg.in/pg.v5"
)

// AnalyticsService is
type AnalyticsService struct {
	DB                  *pg.DB
	ProfessionalService api.ProfessionalService
	AppointmentService  api.AppointmentService
}

// GetProfessionalAnalytics is
func (s *AnalyticsService) GetProfessionalAnalytics(professionalID string) (*api.Analytics, error) {
	var a api.Analytics
	var operatingTime api.Time
	day := time.Now().Weekday()

	log.Println("day is", day)

	p, err := s.ProfessionalService.GetProfessional(professionalID)
	if err != nil {
		return nil, err
	}

	var c api.Company
	if err := s.DB.Model(&c).Where("id = ?", p.CompanyID).First(); err != nil {
		return nil, err
	}

	log.Println("hours", c.Hours)

	switch strings.ToLower(day.String()) {
	case "monday":
		operatingTime = c.Hours.Monday
		break
	case "tuesday":
		operatingTime = c.Hours.Tuesday
		break
	case "wednesday":
		operatingTime = c.Hours.Wednesday
		break
	case "thursday":
		operatingTime = c.Hours.Thursday
		break
	case "friday":
		operatingTime = c.Hours.Friday
		break
	case "saturday":
		operatingTime = c.Hours.Saturday
		break
	case "sunday":
		operatingTime = c.Hours.Sunday
		break
	}

	log.Println("opertatingtime", operatingTime)

	closeHour, err := strconv.Atoi(strings.Split(operatingTime.Close, ":")[0])
	if err != nil {
		return nil, fmt.Errorf("could not convert closeHour %v. Close hour is %s", err, operatingTime.Close)
	}
	closeMinute, err := strconv.Atoi(strings.Split(operatingTime.Close, ":")[1])
	if err != nil {
		return nil, fmt.Errorf("could not convert closeMinute %v", err)
	}

	zone, err := time.LoadLocation(c.Timezone)
	if err != nil {
		return nil, err
	}

	// transform the date into a GO time.Time
	tm := time.Now().In(zone)

	midnight := tm.Add(time.Duration(-tm.Hour()) * time.Hour).Add(time.Duration(-tm.Minute()) * time.Minute)

	closeStamp := midnight.Add(time.Duration(closeHour) * time.Hour).Add(time.Duration(closeMinute) * time.Minute).Add(10 * time.Minute).Unix()

	weekOperatingTime, err := calcOperatingTime(c.Hours)
	if err != nil {
		return nil, err
	}
	// monthOperatingTime := weekOperatingTime * 4
	// yearOperatingTime := weekOperatingTime * 52

	weekAppointments, err := s.AppointmentService.ListProfessionalAppointments(p.ID, time.Now().Unix(), time.Now().Add(168*time.Hour).Unix(), 150)
	if err != nil {
		return nil, err
	}

	var totalWorkHours int64
	for _, appointment := range weekAppointments {
		totalWorkHours += appointment.EndTime - appointment.StartTime
	}
	log.Println("hours booked", totalWorkHours)
	var percentageBooked float32
	if totalWorkHours > 0 {
		percentageBooked = float32(totalWorkHours) / float32(weekOperatingTime)
	}

	dayQuery := `
	SELECT 
		( 
			SELECT count(id) 
			FROM   appointments 
			WHERE  professional_id = ? 
			AND    cancelled = ?
			AND    To_timestamp(appointments.start_time) >= Date_trunc('day', CURRENT_DATE) 
			AND    To_timestamp(appointments.start_time) < Date_trunc('day', CURRENT_DATE) + INTERVAL '1 day'
		)
		AS new_appointments,
		( 
			SELECT count(company_id) 
			FROM   company_customer 
			WHERE  company_id = ? 
			AND    created_at >= date_trunc('day', CURRENT_DATE) 
			AND    created_at < date_trunc('day', CURRENT_DATE) + INTERVAL '1 day'
		) 
		AS new_customers,
		( 
			SELECT count(id) 
			FROM   appointments 
			WHERE  professional_id = ? 
			AND    start_time < ? 
			AND    (to_timestamp(appointments.start_time)) > (now())
		) 
		AS appointments_left
	FROM   appointments 
	LIMIT  1	
	`

	if _, err := s.DB.Query(&a.Day, dayQuery, p.ID, false, p.CompanyID, p.ID, closeStamp); err != nil {
		return nil, err
	}

	weekQuery := `
	SELECT 
		( 
		   SELECT count(id) 
		   FROM   appointments 
		   WHERE  professional_id = ? 
		   AND    cancelled = ?
		   AND    To_timestamp(appointments.start_time) >= Date_trunc('week', CURRENT_DATE) 
		   AND    To_timestamp(appointments.start_time) < Date_trunc('week', CURRENT_DATE) + INTERVAL '1 week'
		)
		AS new_appointments,
		( 
		   SELECT count(company_id) 
		   FROM   company_customer 
		   WHERE  company_id = ? 
		   AND    created_at >= date_trunc('week', CURRENT_DATE) 
		   AND    created_at < date_trunc('week', CURRENT_DATE) + INTERVAL '1 week'
		) 
		AS new_customers,
		( 
			SELECT count(id) 
			FROM   appointments 
			WHERE  professional_id = ? 
			AND    start_time > round(extract(epoch from now()))
			AND    start_time > round(extract(epoch from now())) + round(EXTRACT(epoch FROM INTERVAL '1 week'))
		) 
		AS appointments_left
	FROM   appointments 
	LIMIT  1	
	`

	if _, err := s.DB.Query(&a.Week, weekQuery, p.ID, false, p.CompanyID, p.ID); err != nil {
		return nil, err
	}
	a.Week.PercentageBooked = percentageBooked

	monthQuery := `
	SELECT 
		( 
		   SELECT Count(id) 
		   FROM   appointments 
		   WHERE  professional_id = ? 
		   AND    To_timestamp(appointments.start_time) >= Date_trunc('month', CURRENT_DATE) 
		   AND    To_timestamp(appointments.start_time) < Date_trunc('month', CURRENT_DATE) + INTERVAL '1 month'
		) 
		AS new_appointments,
		( 
		   SELECT count(company_id) 
		   FROM   company_customer 
		   WHERE  company_id = ? 
		   AND    created_at >= date_trunc('month', CURRENT_DATE) 
		   AND    created_at < date_trunc('month', CURRENT_DATE) + INTERVAL '1 month'
		) 
		AS new_customers,
		( 
			SELECT count(id) 
			FROM   appointments 
			WHERE  professional_id = ? 
			AND    start_time > round(extract(epoch from now()))
			AND    start_time > round(extract(epoch from now())) + round(EXTRACT(epoch FROM INTERVAL '1 month'))
		) 
		AS appointments_left
	FROM   appointments 
	LIMIT  1		
	`

	if _, err := s.DB.Query(&a.Month, monthQuery, p.ID, p.CompanyID, p.ID); err != nil {
		return nil, err
	}

	yearQuery := `
	SELECT 
	( 
		   SELECT Count(id) 
		   FROM   appointments 
		   WHERE  professional_id = ? 
		   AND    To_timestamp(appointments.start_time) >= Date_trunc('year', CURRENT_DATE) 
		   AND    To_timestamp(appointments.start_time) < Date_trunc('year', CURRENT_DATE) + INTERVAL '1 year') AS new_appointments,
	( 
		   SELECT count(company_id) 
		   FROM   company_customer 
		   WHERE  company_id = ? 
		   AND    created_at >= date_trunc('year', CURRENT_DATE) 
		   AND    created_at < date_trunc('year', CURRENT_DATE) + INTERVAL '1 year') 
		   AS new_customers,
		   ( 
			SELECT count(id) 
			FROM   appointments 
			WHERE  professional_id = ? 
			AND    start_time > round(extract(epoch from now()))
			AND    start_time > round(extract(epoch from now())) + round(EXTRACT(epoch FROM INTERVAL '1 year'))
		) 
		AS appointments_left
FROM   appointments 
LIMIT  1	
	`

	if _, err := s.DB.Query(&a.Year, yearQuery, p.ID, p.CompanyID, p.ID); err != nil {
		return nil, err
	}
	return &a, nil
}

func calcOperatingTime(h *api.Hours) (int, error) {
	mh, err := convertHourToTimeStamp(h.Monday)
	if err != nil {
		return 0, err
	}
	if !h.Monday.Operating {
		mh = 0
	}
	tuh, err := convertHourToTimeStamp(h.Tuesday)
	if err != nil {
		return 0, err
	}
	if !h.Tuesday.Operating {
		tuh = 0
	}
	wh, err := convertHourToTimeStamp(h.Wednesday)
	if err != nil {
		return 0, err
	}
	if !h.Wednesday.Operating {
		wh = 0
	}
	thh, err := convertHourToTimeStamp(h.Thursday)
	if err != nil {
		return 0, err
	}
	if !h.Thursday.Operating {
		thh = 0
	}
	frh, err := convertHourToTimeStamp(h.Friday)
	if err != nil {
		return 0, err
	}
	if !h.Friday.Operating {
		frh = 0
	}
	sah, err := convertHourToTimeStamp(h.Saturday)
	if err != nil {
		return 0, err
	}
	if !h.Saturday.Operating {
		sah = 0
	}
	suh, err := convertHourToTimeStamp(h.Sunday)
	if err != nil {
		return 0, err
	}
	if !h.Sunday.Operating {
		suh = 0
	}

	return mh + tuh + wh + thh + frh + sah + suh, nil
}

func convertHourToTimeStamp(t api.Time) (int, error) {
	sanitizedOpen := strings.Replace(t.Open, ":", "", -1)
	sanitizedClose := strings.Replace(t.Close, ":", "", -1)

	intOpen, err := strconv.Atoi(sanitizedOpen)
	if err != nil {
		return 0, fmt.Errorf("could not convert intOpen %v", err)
	}

	intClose, err := strconv.Atoi(sanitizedClose)
	if err != nil {
		return 0, fmt.Errorf("could not convert intClose %v", err)
	}

	hour := (intClose - intOpen) / 100

	return hour * 3600, nil
}
