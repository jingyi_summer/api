package postgre

import (
	"testing"

	"gitlab.com/vanoapp/api"
)

func TestCreateTransaction(t *testing.T) {
	tests := []struct {
		t        *api.Transaction
		expected bool
	}{
		{&api.Transaction{ProfessionalID: testProfessional.ID, CustomerID: testCustomer.ID, Total: 8000, SplashID: "g12345678"}, true},
		{&api.Transaction{ProfessionalID: testProfessional.ID, CustomerID: testCustomer.ID, Total: 8000}, false},
		{&api.Transaction{ProfessionalID: testProfessional.ID, CustomerID: testCustomer.ID}, false},
		{&api.Transaction{CustomerID: testCustomer.ID, Total: 8000}, false},
		{&api.Transaction{ProfessionalID: testProfessional.ID, Total: 8000}, false},
		{&api.Transaction{ProfessionalID: testProfessional.ID, Total: 8000, SplashID: "g12345678"}, true},
	}

	for i, test := range tests {
		err := testTransactionSvc.CreateTransaction(test.t)
		result := err == nil

		if result != test.expected {
			t.Errorf("unexpected result when id is %d, and err is %v", i+1, err)
		}
	}
}
