package postgre

import (
	"fmt"
	"log"
	"strings"
	"time"

	uuid "github.com/satori/go.uuid"
	"gitlab.com/vanoapp/api"
	pg "gopkg.in/pg.v5"
)

// ServiceService is
type ServiceService struct {
	DB            *pg.DB
	SearchService api.SearchService
}

// CreateService is
func (s *ServiceService) CreateService(p *api.Service) (*api.Service, error) {
	p.ID = uuid.NewV4().String()
	p.CreatedAt = time.Now()
	p.UpdatedAt = time.Now()

	// Professional values
	values := []interface{}{
		newNullString(p.ID),
		newNullString(p.ProfessionalID),
		newNullString(p.Name),
		newNullString(p.Description),
		newNullInt(p.Length),
		newNullInt(p.Price),
		p.CreatedAt,
		p.UpdatedAt,
	}

	if len(p.Steps) == 0 {
		if err := s.DB.Insert(p); err != nil {
			return nil, err
		}

		go func(profID string) {
			if err := s.SearchService.UpdateProfessionalServices(profID); err != nil {
				log.Println("error updating sp services", err)
			}
		}(p.ProfessionalID)

		return p, nil
	}

	q := `
	WITH services as (INSERT INTO services(id, professional_id, name, description, length, price, created_at, updated_at) VALUES (?, ?, ?, ?, ?, ?, ?, ?) RETURNING *) INSERT INTO services(id, name, length, service_id, is_gap, created_at, updated_at) VALUES
`

	for _, row := range p.Steps {
		q += "(uuid_generate_v4(), ?, ?, (select services.id from services), ?, now(), now()),"
		values = append(values, newNullString(row.Name), newNullInt(row.Length), row.IsGap)
	}

	q = strings.TrimSuffix(q, ",")

	if _, err := s.DB.Query(p, q, values...); err != nil {
		return nil, err
	}

	go func(profID string) {
		if err := s.SearchService.UpdateProfessionalServices(profID); err != nil {
			log.Println("error updating sp services", err)
		}
	}(p.ProfessionalID)

	svc, err := s.GetService(p.ID)
	if err != nil {
		return nil, fmt.Errorf("err getting service %v", err)
	}

	return svc, nil
}

// GetService is
func (s *ServiceService) GetService(id string) (*api.Service, error) {
	var p api.Service
	if err := s.DB.Model(&p).Column("*", "Steps").Where("id = ?", id).First(); err != nil {
		if err == pg.ErrNoRows {
			return nil, api.ErrNotFound
		}

		return nil, err
	}

	return &p, nil
}

// UpdateService is
func (s *ServiceService) UpdateService(p *api.Service) error {
	if _, err := s.GetService(p.ID); err != nil {
		return err
	}

	p.UpdatedAt = time.Now()
	if _, err := s.DB.Model(&p).Column("name", "description", "price", "length", "updated_at", "require_phone_call").Update(); err != nil {
		return err
	}

	go func(profID string) {
		if err := s.SearchService.UpdateProfessionalServices(profID); err != nil {
			log.Print("error updating sp services", err)
		}
	}(p.ProfessionalID)

	return nil
}

// RemoveService is
func (s *ServiceService) RemoveService(id string) error {
	svc, err := s.GetService(id)
	if err != nil {
		return err
	}

	if err := s.DB.Delete(svc); err != nil {
		return err
	}

	go func(profID string) {
		if err := s.SearchService.UpdateProfessionalServices(profID); err != nil {
			log.Println("error updating sp services", err)
		}
	}(svc.ProfessionalID)

	return nil
}
