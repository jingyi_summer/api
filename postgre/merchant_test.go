package postgre

import (
	"testing"

	"gitlab.com/vanoapp/api"
)

func TestCreateMerchant(t *testing.T) {
	tests := []struct {
		m        *api.Merchant
		expected bool
	}{
		{&api.Merchant{CompanyID: testCompany.ID, SplashID: "testsplashid", EIN: "jijdfidf", AnnualCCSales: 9000000, AccountNumber: "1234567", RoutingNumber: "123456789", AccountType: "checking", DLNumber: "12345678", DLState: "FL"}, true},
		{&api.Merchant{SplashID: "testsplashid", EIN: "jijdfidf", AnnualCCSales: 9000000, AccountNumber: "1234567", RoutingNumber: "123456789", AccountType: "checking", DLNumber: "12345678", DLState: "FL"}, false},
		{&api.Merchant{CompanyID: testCompany.ID, EIN: "jijdfidf", AnnualCCSales: 9000000, AccountNumber: "1234567", RoutingNumber: "123456789", AccountType: "checking", DLNumber: "12345678", DLState: "FL"}, false},
		{&api.Merchant{CompanyID: testCompany.ID, SplashID: "testsplashid", AnnualCCSales: 9000000, AccountNumber: "1234567", RoutingNumber: "123456789", AccountType: "checking", DLNumber: "12345678", DLState: "FL"}, false},
		{&api.Merchant{CompanyID: testCompany.ID, SplashID: "testsplashid", EIN: "jijdfidf", AccountNumber: "1234567", RoutingNumber: "123456789", AccountType: "checking", DLNumber: "12345678", DLState: "FL"}, false},
		{&api.Merchant{CompanyID: testCompany.ID, SplashID: "testsplashid", EIN: "jijdfidf", AnnualCCSales: 9000000, RoutingNumber: "123456789", AccountType: "checking", DLNumber: "12345678", DLState: "FL"}, false},
		{&api.Merchant{CompanyID: testCompany.ID, SplashID: "testsplashid", EIN: "jijdfidf", AnnualCCSales: 9000000, AccountNumber: "1234567", AccountType: "checking", DLNumber: "12345678", DLState: "FL"}, false},
		{&api.Merchant{CompanyID: testCompany.ID, SplashID: "testsplashid", EIN: "jijdfidf", AnnualCCSales: 9000000, AccountNumber: "1234567", RoutingNumber: "123456789", DLNumber: "12345678", DLState: "FL"}, false},
		{&api.Merchant{CompanyID: testCompany.ID, SplashID: "testsplashid", EIN: "jijdfidf", AnnualCCSales: 9000000, AccountNumber: "1234567", RoutingNumber: "123456789", AccountType: "checking", DLState: "FL"}, false},
		{&api.Merchant{CompanyID: testCompany.ID, SplashID: "testsplashid", EIN: "jijdfidf", AnnualCCSales: 9000000, AccountNumber: "1234567", RoutingNumber: "123456789", AccountType: "checking", DLNumber: "12345678"}, false},
	}

	for i, test := range tests {
		testID := i + 1
		err := testMerchantSVC.CreateMerchant(test.m)
		result := err == nil

		if result != test.expected {
			t.Errorf("Unexpected result in test id %d with an error of %v", testID, err)
		}

		if err == nil {
			switch {
			case test.m.ID == "":
				t.Errorf("expecting merchant id to be set when err is nil. Test id %d", testID)
			}
		}
	}
}
