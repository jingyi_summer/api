package postgre

import (
	"time"

	"gitlab.com/vanoapp/api"
	pg "gopkg.in/pg.v5"
)

// ReminderService is
type ReminderService struct {
	DB *pg.DB
}

// CreateReminder is
func (s *ReminderService) CreateReminder(r *api.Reminder) error {
	r.New()

	return s.DB.Insert(r)
}

// Get is
func (s *ReminderService) Get(id string) (*api.Reminder, error) {
	var r api.Reminder
	if err := s.DB.Model(&r).Where("id = ?", id).First(); err != nil {
		if err == pg.ErrNoRows {
			return nil, api.ErrNotFound
		}
		return nil, err
	}

	return &r, nil
}

// UpdateReminder is
func (s *ReminderService) UpdateReminder(r *api.UpdateReminder) (*api.Reminder, error) {
	re, err := s.Get(r.ID)
	if err != nil {
		return nil, err
	}

	re.Completed = r.Completed
	re.DueDate = r.DueDate
	re.Text = r.Text
	re.Description = r.Description
	re.UpdatedAt = time.Now()

	if _, err := s.DB.Model(re).Column("text", "due_date", "updated_at", "completed", "description").Update(); err != nil {
		return nil, err
	}

	return re, nil
}

// GetProfessionalReminders is
func (s *ReminderService) GetProfessionalReminders(professionalID string, limit int, completed bool) ([]*api.Reminder, error) {
	var r []*api.Reminder
	if err := s.DB.Model(&r).Where("professional_id = ?", professionalID).Where("completed = ?", completed).Order("due_date DESC").Limit(limit).Select(); err != nil {
		return nil, err
	}

	return r, nil
}

// GetReminder is
func (s *ReminderService) GetReminder(id string) (*api.Reminder, error) {
	r := new(api.Reminder)
	if err := s.DB.Model(r).Where("id = ?", id).First(); err != nil {
		if err == pg.ErrNoRows {
			return nil, api.ErrNotFound
		}
		return nil, err
	}

	return r, nil
}

// DeleteReminder is
func (s *ReminderService) DeleteReminder(id string) error {
	r, err := s.GetReminder(id)
	if err != nil {
		return err
	}

	return s.DB.Delete(r)
}
