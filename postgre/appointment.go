package postgre

import (
	"log"
	"strings"
	"time"

	"github.com/rs/zerolog"
	uuid "github.com/satori/go.uuid"

	"strconv"

	"gitlab.com/vanoapp/api"
	pg "gopkg.in/pg.v5"
)

// AppointmentService is
type AppointmentService struct {
	DB                  *pg.DB
	NotificationService api.NotificationService
	CustomerService     api.CustomerService
	ProfessionalService api.ProfessionalService
	Logger              zerolog.Logger
}

// CreateAppointment is
func (s *AppointmentService) CreateAppointment(a *api.Appointment) error {
	a.New()

	// Appointment values
	values := []interface{}{
		newNullString(a.ID),
		newNullString(a.ProfessionalID),
		newNullString(a.CustomerID),
		newNullInt64(a.StartTime),
		newNullInt64(a.EndTime),
		newNullString(a.Notes),
		a.IsBlockoff,
		a.CreatedAt,
		a.UpdatedAt,
	}

	// Everything under here is to add to the appointment_service table all in 1 transaction
	q := `
		WITH appointment as (INSERT INTO appointments(id, professional_id, customer_id, start_time, end_time, notes, is_blockoff, created_at, updated_at) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?) RETURNING *) INSERT INTO appointment_service(id, appointment_id, service_id, length, start_time, end_time, professional_id, is_gap) VALUES
	`

	var svcLen int64
	for _, service := range a.Services {
		startTime := a.StartTime + svcLen
		endTime := a.StartTime + svcLen + (int64(service.Length) * 60)
		svcLen += int64(service.Length) * 60
		q += "(?, (select appointment.id from appointment), ?, ?, ?, ?, ?, ?),"
		values = append(values, newNullString(uuid.NewV4().String()), newNullString(service.ID), newNullInt(service.Length), newNullInt64(startTime), newNullInt64(endTime), newNullString(a.ProfessionalID), service.IsGap)
	}

	q = strings.TrimSuffix(q, ",")
	if _, err := s.DB.Query(a, q, values...); err != nil {
		return err
	}

	go func() {
		customer, err := s.CustomerService.GetCustomer(a.CustomerID)
		if err != nil {
			log.Println("[ERROR] getting customer", err)
			return
		}
		if len(customer.Companies) == 0 {
			sp, err := s.ProfessionalService.GetProfessional(a.ProfessionalID)
			if err != nil {
				log.Println("[ERROR] getting sp", err)
				return
			}
			if err := s.CustomerService.AddCustomerCompany(a.CustomerID, sp.CompanyID); err != nil {
				log.Println("[ERROR] adding customer to company", err)
			}
			return
		}
		sp, err := s.ProfessionalService.GetProfessional(a.ProfessionalID)
		if err != nil {
			log.Println("[ERROR] getting sp", err)
			return
		}

		var ids []string
		for _, comp := range customer.Companies {
			ids = append(ids, comp.ID)
		}
		if !stringInSlice(sp.CompanyID, ids) {
			if err := s.CustomerService.AddCustomerCompany(a.CustomerID, sp.CompanyID); err != nil {
				log.Println("[ERROR] adding customer to company", err)
			}
		}
	}()

	if !a.IsBlockoff {
		go func() {
			if err := s.NotificationService.NotifyNewAppointment(a); err != nil {
				s.Logger.Error().Err(err).Msg("[ERROR] notifying users of new appointment")
			}
		}()
	}

	return nil
}

func stringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}

// ListProfessionalBlockoffBetween is
func (s *AppointmentService) ListProfessionalBlockoffBetween(professionalID string, startTime, endTime int64, limit int) ([]*api.Appointment, error) {
	var appointments []*api.Appointment

	if err := s.DB.Model(&appointments).Column("Services").Where("professional_id = ?", professionalID).Where("cancelled = ?", false).Where("start_time >= ?", startTime).Where("end_time <= ?", endTime).Where("is_blockoff = ?", true).Order("start_time").Limit(limit).Select(); err != nil {
		return nil, err
	}

	if len(appointments) == 0 {
		return appointments, nil
	}

	// We do this here to include the appointment service length and not the default service length
	var appointmentIds []interface{}
	for _, a := range appointments {
		appointmentIds = append(appointmentIds, a.ID)
	}

	var appSvcs []*api.ServiceAppointment
	if err := s.DB.Model(&appSvcs).WhereIn("appointment_id IN (?)", appointmentIds...).Select(); err != nil {
		return nil, err
	}

	for _, appt := range appointments {
		for _, appSvc := range appSvcs {
			if appSvc.AppointmentID == appt.ID {
				for _, svc := range appt.Services {
					if svc.ID == appSvc.ServiceID {
						svc.Length = appSvc.Length
						break
					}
				}
			}
		}
	}

	return appointments, nil
}

// ListProfessionalAppointments is
func (s *AppointmentService) ListProfessionalAppointments(professionalID string, startTime, endTime int64, limit int) ([]*api.Appointment, error) {
	var appointments []*api.Appointment

	if err := s.DB.Model(&appointments).Column("Services", "PostAppointment", "Services.Steps").Where("professional_id = ?", professionalID).Where("cancelled = ?", false).Where("start_time >= ?", startTime).Where("end_time <= ?", endTime).Order("start_time").Limit(limit).Select(); err != nil {
		return nil, err
	}

	if len(appointments) == 0 {
		return appointments, nil
	}

	// We do this here to include the appointment service length and not the default service length
	var appointmentIds []interface{}
	for _, a := range appointments {
		appointmentIds = append(appointmentIds, a.ID)
	}

	var appSvcs []*api.ServiceAppointment
	if err := s.DB.Model(&appSvcs).WhereIn("appointment_id IN (?)", appointmentIds...).Select(); err != nil {
		return nil, err
	}

	for _, appt := range appointments {
		for _, appSvc := range appSvcs {
			if appSvc.AppointmentID == appt.ID {
				for _, svc := range appt.Services {
					if svc.ID == appSvc.ServiceID {
						svc.Length = appSvc.Length
						break
					}
				}
			}
		}
	}

	return appointments, nil
}

// ListProfessionalBlockoffs is
func (s *AppointmentService) ListProfessionalBlockoffs(id string) ([]*api.Appointment, error) {
	var a []*api.Appointment
	if err := s.DB.Model(&a).Where("is_blockoff = ? and cancelled = ? and professional_id = ?", true, false, id).Select(); err != nil {
		return nil, err
	}
	return a, nil
}

// ListProfessionalAvaibaleAppointmentTimes is
func (s *AppointmentService) ListProfessionalAvaibaleAppointmentTimes(professionalID string, serviceLengths, date int64) ([]string, error) {
	var times []string

	var professional api.Professional
	if err := s.DB.Model(&professional).Where("id = ?", professionalID).First(); err != nil {
		if err == pg.ErrNoRows {
			return nil, api.ErrNotFound
		}
		return nil, err
	}

	var c api.Company
	if err := s.DB.Model(&c).Where("id = ?", professional.CompanyID).First(); err != nil {
		if err == pg.ErrNoRows {
			return nil, api.ErrNotFound
		}
		return nil, err
	}

	zone, err := time.LoadLocation(c.Timezone)
	if err != nil {
		return nil, err
	}

	// transform the date into a GO time.Time
	tm := time.Unix(date, 0).In(zone)

	// Subtract all the hours from the time so we get the midnight timestamp value
	midnight := tm.Add(time.Duration(-tm.Hour()) * time.Hour).Add(time.Duration(-tm.Minute()) * time.Minute)

	// get the days operating time
	operatingTime := getOpenCloseTimes(&c, tm)

	if !operatingTime.Operating {
		return []string{}, nil
	}

	// Here we just need to transforms all the strings to ints seems bloated but there is not a lot going on
	openHour, err := strconv.Atoi(strings.Split(operatingTime.Open, ":")[0])
	if err != nil {
		return nil, err
	}
	closeHour, err := strconv.Atoi(strings.Split(operatingTime.Close, ":")[0])
	if err != nil {
		return nil, err
	}
	openMinute, err := strconv.Atoi(strings.Split(operatingTime.Open, ":")[1])
	if err != nil {
		return nil, err
	}
	closeMinute, err := strconv.Atoi(strings.Split(operatingTime.Close, ":")[1])
	if err != nil {
		return nil, err
	}

	// create the open and close timestamps for the appointment date
	openStamp := midnight.Add(time.Duration(openHour) * time.Hour).Add(time.Duration(openMinute) * time.Minute).Unix()
	closeStamp := midnight.Add(time.Duration(closeHour) * time.Hour).Add(time.Duration(closeMinute) * time.Minute).Add(10 * time.Minute).Unix()

	// generate all the possible appointment times. We add a 15 minute gap window per appointment.
	for i := openStamp; i < closeStamp; i++ {
		if time.Now().In(zone).Unix() <= i {
			times = append(times, strconv.FormatInt(i, 10))
		}
		i += 15 * 60
	}
	if len(times) == 0 {
		times = []string{}
	}

	appointments, err := s.ListProfessionalAppointments(professionalID, openStamp, closeStamp, 50)
	if err != nil {
		return nil, err
	}

	blockoffs, err := s.ListProfessionalBlockoffBetween(professionalID, openStamp, closeStamp, 50)
	if err != nil {
		return nil, err
	}

	appointments = append(appointments, blockoffs...)

	sanitizedTimes, err := sanitizeTimes(times, appointments, serviceLengths, closeStamp)
	if err != nil {
		return nil, err
	}

	return sanitizedTimes, nil
}

// ListCustomerAppointments is
func (s *AppointmentService) ListCustomerAppointments(customerID string, startTime, endTime int64, limit int) ([]*api.Appointment, error) {
	var appointments []*api.Appointment

	if err := s.DB.Model(&appointments).Column("Services", "PostAppointment", "Services.Steps").Where("customer_id = ?", customerID).Where("cancelled = ?", false).Where("start_time >= ?", startTime).Where("end_time <= ?", endTime).Limit(limit).Select(); err != nil {
		return nil, err
	}

	if len(appointments) == 0 {
		return appointments, nil
	}

	// We do this here to include the appointment service length and not the default service length
	var appointmentIds []interface{}
	for _, a := range appointments {
		appointmentIds = append(appointmentIds, a.ID)
	}

	var appSvcs []*api.ServiceAppointment
	if err := s.DB.Model(&appSvcs).WhereIn("appointment_id IN (?)", appointmentIds...).Select(); err != nil {
		return nil, err
	}

	for _, appt := range appointments {
		for _, appSvc := range appSvcs {
			if appSvc.AppointmentID == appt.ID {
				for _, svc := range appt.Services {
					if svc.ID == appSvc.ServiceID {
						svc.Length = appSvc.Length
						break
					}
				}
			}
		}
	}

	return appointments, nil
}

// GetAppointment is
func (s *AppointmentService) GetAppointment(id string) (*api.Appointment, error) {
	var a api.Appointment

	if err := s.DB.Model(&a).Column("appointment.*", "Services", "Services.Steps").Where("id = ?", id).First(); err != nil {
		if err == pg.ErrNoRows {
			return nil, api.ErrNotFound
		}
		return nil, err
	}

	// We do this here to include the appointment service length and not the default service length
	var appSvcs []*api.ServiceAppointment
	if err := s.DB.Model(&appSvcs).Where("appointment_id = ?", a.ID).Select(); err != nil {
		return nil, err
	}

	for _, appSvc := range appSvcs {
		if appSvc.AppointmentID == a.ID {
			for _, svc := range a.Services {
				if svc.ID == appSvc.ServiceID {
					svc.Length = appSvc.Length
					break
				}
			}
		}

	}

	return &a, nil
}

// UpdateAppointment is
func (s *AppointmentService) UpdateAppointment(a *api.Appointment) error {
	if _, err := s.GetAppointment(a.ID); err != nil {
		return err
	}

	a.UpdatedAt = time.Now()

	// Appointment values
	values := []interface{}{
		newNullString(a.ID),
		newNullString(a.ProfessionalID),
		newNullString(a.CustomerID),
		newNullInt64(a.StartTime),
		newNullInt64(a.EndTime),
		newNullString(a.Notes),
		a.UpdatedAt,
		newNullString(a.ID),
	}

	q := `
      DELETE FROM appointment_service WHERE appointment_id = ?; 
	  WITH appointment as (UPDATE appointments SET professional_id = ?, customer_id = ?, start_time = ?, end_time = ?, notes = ?, updated_at = ? WHERE id = ? RETURNING *) 
	  INSERT INTO appointment_service(id, appointment_id, service_id, length) VALUES
	`
	for _, row := range a.Services {
		q += "(?, (select appointment.id from appointment), ?, ?),"
		values = append(values, newNullString(uuid.NewV4().String()), newNullString(row.ID), newNullInt(row.Length))
	}

	q = strings.TrimSuffix(q, ",")

	if _, err := s.DB.Query(&api.Appointment{}, q, values...); err != nil {
		return err
	}

	go func() {
		if err := s.NotificationService.NotifyUpdateAppointment(a); err != nil {
			log.Println("[ERROR] notifying users of new appointment", err)
		}
	}()

	return nil
}

// CancelAppointment is
func (s *AppointmentService) CancelAppointment(id string) error {
	a, err := s.GetAppointment(id)
	if err != nil {
		return err
	}

	a.Cancelled = true

	if err := s.DB.Update(a); err != nil {
		return err
	}

	var appointmentServices []*api.ServiceAppointment
	if err := s.DB.Model(&appointmentServices).Where("appointment_id = ?", a.ID).Select(); err != nil {
		return nil
	}

	for _, as := range appointmentServices {
		as.IsCancelled = true
	}

	if _, err := s.DB.Model(&appointmentServices).Column("is_cancelled", "updated_at").Update(); err != nil {
		return err
	}

	go func() {
		if err := s.NotificationService.NotifyCancelAppointment(a); err != nil {
			s.Logger.Error().Err(err).Msg("[ERROR] notifying users of cancelled appointment")
		}
	}()

	return nil
}

// CheckAppointmentIsAvailable is used primarily in validation. So instead of sending back the user an internal server error when they try and book an appointment that has already been taken they get a bad request validation error.
func (s *AppointmentService) CheckAppointmentIsAvailable(profID string, startTime, endTime int64) error {
	a := new(*api.ServiceAppointment)
	if err := s.DB.Model(&a).Where("professional_id = ? AND is_gap = ? AND is_cancelled = ? AND start_time BETWEEN ? AND ?", profID, false, false, startTime, endTime).WhereOr("professional_id = ? AND is_gap = ? AND is_cancelled = ? AND end_time BETWEEN ? and  ?", profID, false, false, startTime, endTime).First(); err != nil {
		if err == pg.ErrNoRows {
			return nil
		}
		return err
	}

	return nil
}

// CreatePostAppointment is
func (s *AppointmentService) CreatePostAppointment(p *api.PostAppointment) (*api.PostAppointment, error) {
	newP := p
	newP.ID = uuid.NewV4().String()
	newP.CreatedAt = time.Now()
	newP.UpdatedAt = time.Now()

	if err := s.DB.Insert(newP); err != nil {
		return nil, err
	}

	return newP, nil
}

// UpdatePostAppointment is
func (s *AppointmentService) UpdatePostAppointment(p *api.PostAppointment) (*api.PostAppointment, error) {
	newP := p
	newP.UpdatedAt = time.Now()

	r, err := s.DB.Model(newP).Column("color_formula", "highlight_formula", "total_service_length", "notes").Update()
	if err != nil {
		return nil, err
	}

	if r.RowsAffected() == 0 {
		return nil, api.ErrNotFound
	}

	return newP, nil
}

// DeletePostAppointment is
func (s *AppointmentService) DeletePostAppointment(id string) error {
	p := new(api.PostAppointment)
	if err := s.DB.Model(p).Where("id = ?", id).First(); err != nil {
		if err == pg.ErrNoRows {
			return api.ErrNotFound
		}
		return err
	}

	if _, err := s.DB.Model(p).Delete(); err != nil {
		return err
	}

	return nil
}

func getOpenCloseTimes(c *api.Company, tm time.Time) api.Time {
	day := strings.ToLower(tm.Weekday().String())

	switch day {
	case "monday":
		return c.Hours.Monday
	case "tuesday":
		return c.Hours.Tuesday
	case "wednesday":
		return c.Hours.Wednesday
	case "thursday":
		return c.Hours.Thursday
	case "friday":
		return c.Hours.Friday
	case "saturday":
		return c.Hours.Saturday
	case "sunday":
		return c.Hours.Sunday
	}

	return api.Time{}
}

func sanitizeTimes(times []string, appointments []*api.Appointment, length, closeStamp int64) ([]string, error) {
	for t := 0; t < len(times); t++ {
		tm := times[t]

		intTime, err := strconv.ParseInt(tm, 10, 64)
		if err != nil {
			return nil, err
		}

		// This will remove any services that would cause the salon pro to have to work passed their business hours.
		hourWithServicesLength := intTime + (length * 60)
		if hourWithServicesLength > closeStamp {
			times = append(times[:t], times[t+1:]...)
			t--
			continue
		}

		for _, appointment := range appointments {
			var svcLen int64
			shouldBreak := false
			for _, svc := range appointment.Services {
				svcStart := appointment.StartTime + svcLen
				svcEnd := appointment.StartTime + svcLen + int64(svc.Length*60)
				svcLen += int64(svc.Length * 60)
				if svc.IsGap {
					continue
				}

				// this removes all hours that are inbetween an appointment. For example if there is an appointment that starts at 7 and ends at 8 this if statement will remove all of the times between 7 and 8.
				if intTime >= svcStart && intTime <= svcEnd {
					times = append(times[:t], times[t+1:]...)
					t--
					shouldBreak = true
					break
				}

				// this removes all hours that fall between the appointment start time minus all the services total lenght. For example there is an appointment booked for 3pm. The customer selected a service that takes 30 minutes. That means that all the times between 2:30 (3pm - 30 mins) should not be available
				tt := appointment.StartTime - (length * 60)
				if intTime >= tt && intTime <= appointment.StartTime {
					times = append(times[:t], times[t+1:]...)
					t--
					shouldBreak = true
					break
				}
			}
			if shouldBreak {
				break
			}
		}
	}
	return times, nil
}
