package postgre

import (
	"testing"

	uuid "github.com/satori/go.uuid"
	"gitlab.com/vanoapp/api"
)

func TestCreateService(t *testing.T) {
	tests := []struct {
		c        *api.Service
		expected bool
	}{
		{&api.Service{ProfessionalID: testProfessional.ID, Name: "test service", Description: "test desc", Length: 20, Price: 4000}, true},
	}

	for _, test := range tests {
		err := testServiceSvc.CreateService(test.c)
		result := err == nil
		if result != test.expected {
			t.Errorf("Unexpected result. When service is %v, with an error: %v", test.c, err)
		}

		if err == nil {
			switch {
			case test.c.ID == "":
				t.Errorf("Unexpected result. expected  id to not be nil when err is nil")
			}
		}
	}
}

func TestUpdateService(t *testing.T) {
	tests := []struct {
		s        *api.Service
		err      error
		expected bool
	}{
		{testService, nil, true},
		{&api.Service{}, api.ErrNotFound, false},
		{&api.Service{ID: testService.ID}, nil, false},
	}

	for _, test := range tests {
		u := test.s.UpdatedAt

		err := testServiceSvc.UpdateService(test.s)
		result := err == nil

		if result != test.expected {
			t.Errorf("Unexpected result. When service is %v, with an error: %v", test.s, err)
		}

		if err != nil {
			switch {
			case test.err != nil && err != test.err:
				t.Errorf("Unexpected result. expecting error to not be %v, got %v", test.err, err)
			}
		}

		if err == nil {
			switch {
			case test.s.UpdatedAt == u:
				t.Errorf("Unexpected result, expecting updated_at column to be updated")
			}
		}
	}
}

func TestGetService(t *testing.T) {
	tests := []struct {
		id       string
		err      error
		expected bool
	}{
		{testService.ID, nil, true},
		{uuid.NewV4().String(), api.ErrNotFound, false},
	}

	for _, test := range tests {
		s, err := testServiceSvc.GetService(test.id)
		result := err == nil

		if result != test.expected {
			t.Errorf("Unexpected result. When id is %v, with an error: %v", test.id, err)
		}

		if err != nil {
			switch {
			case err != test.err:
				t.Errorf("Unexpected result. expecting error to not be %v, got %v", test.err, err)
			}
		}

		if err == nil {
			switch {
			case s == nil:
				t.Errorf("Unexpected result. expecting service to not be nil when err is nil")
			}
		}
	}
}

func TestRemoveService(t *testing.T) {
	tests := []struct {
		id       string
		err      error
		expected bool
	}{
		{testService.ID, nil, true},
		{uuid.NewV4().String(), api.ErrNotFound, false},
	}

	for _, test := range tests {
		err := testServiceSvc.RemoveService(test.id)
		result := err == nil

		if result != test.expected {
			t.Errorf("Unexpected result. When id is %v, with an error: %v", test.id, err)
		}

		if err != nil {
			switch {
			case err != test.err:
				t.Errorf("Unexpected result. expecting error to not be %v, got %v", test.err, err)
			}
		}
	}
}
