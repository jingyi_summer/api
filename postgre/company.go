package postgre

import (
	"fmt"
	"net/url"
	"time"

	"github.com/rs/zerolog"
	uuid "github.com/satori/go.uuid"

	"gitlab.com/vanoapp/api"
	pg "gopkg.in/pg.v5"
)

// CompanyService is
type CompanyService struct {
	DB            *pg.DB
	SearchService api.SearchService
	Logger        zerolog.Logger
}

// CreateCompany is
func (s *CompanyService) CreateCompany(c *api.Company) error {
	c.New()
	c.CreateInitialHours()
	c.LogoURL = "https://ui-avatars.com/api/?background=4786FF&color=fff&size=256&name=" + url.QueryEscape(c.Name)

	if err := s.DB.Insert(c); err != nil {
		return err
	}

	go func() {
		searchCompany := &api.SearchCompany{
			ID:       c.ID,
			Address:  fmt.Sprintf("%s, %s %s, %d, %s", c.AddressLine1, c.City, c.State, c.ZipCode, c.Country),
			Name:     c.Name,
			CoverURL: c.CoverURL,
		}

		if err := s.SearchService.AddCompany(searchCompany); err != nil {
			s.Logger.Error().Err(err).Msg("error adding company in algolia")
		}
	}()

	return nil
}

// GetCompany is
func (s *CompanyService) GetCompany(id string) (*api.Company, error) {
	var c api.Company

	if err := s.DB.Model(&c).Where("id = ?", id).First(); err != nil {
		if err == pg.ErrNoRows {
			return nil, api.ErrNotFound
		}

		return nil, err
	}

	return &c, nil
}

// ListCompanies is
func (s *CompanyService) ListCompanies(limit int) ([]*api.Company, error) {
	if limit == 0 {
		limit = 20
	}

	var c []*api.Company
	if err := s.DB.Model(&c).Limit(limit).Select(); err != nil {
		return nil, err
	}

	return c, nil
}

// UpdateCompany is
func (s *CompanyService) UpdateCompany(c *api.Company) error {
	if _, err := s.GetCompany(c.ID); err != nil {
		return err
	}

	c.UpdatedAt = time.Now()

	if _, err := s.DB.Model(c).Column("name", "address_line1", "address_line2", "city", "state", "zip_code", "hours", "updated_at", "merchant_integrated", "logo_url", "cover_url", "description", "phone_number", "email").Update(); err != nil {
		return err
	}

	go func() {
		searchCompany := &api.SearchCompany{
			ID:       c.ID,
			Address:  fmt.Sprintf("%s, %s %s, %d, %s", c.AddressLine1, c.City, c.State, c.ZipCode, c.Country),
			Name:     c.Name,
			CoverURL: c.CoverURL,
		}

		if err := s.SearchService.UpdateCompany(searchCompany); err != nil {
			s.Logger.Error().Err(err).Msg("error updating company in algolia")
		}
	}()

	return nil
}

//AddCompanyImages is
func (s *CompanyService) AddCompanyImages(imgs []*api.CompanyImage) ([]*api.CompanyImage, error) {
	var newImages []*api.CompanyImage
	for _, img := range imgs {
		img.ID = uuid.NewV4().String()
		img.CreatedAt = time.Now()
		img.UpdatedAt = time.Now()
		newImages = append(newImages, img)
	}

	if err := s.DB.Insert(&newImages); err != nil {
		return nil, err
	}

	return newImages, nil
}

//AddCompanyImage is
func (s *CompanyService) AddCompanyImage(img *api.CompanyImage) (*api.CompanyImage, error) {
	newImg := img
	newImg.ID = uuid.NewV4().String()
	newImg.CreatedAt = time.Now()
	newImg.UpdatedAt = time.Now()

	if err := s.DB.Insert(&newImg); err != nil {
		return nil, err
	}

	return newImg, nil
}

//RemoveCompanyImage is
func (s *CompanyService) RemoveCompanyImage(id string) error {
	img, err := s.getCompanyImage(id)
	if err != nil {
		return err
	}

	return s.DB.Delete(img)
}

func (s *CompanyService) getCompanyImage(id string) (*api.CompanyImage, error) {
	img := new(api.CompanyImage)
	if err := s.DB.Model(&img).Where("id = ?", id).First(); err != nil {
		if err == pg.ErrNoRows {
			return nil, api.ErrNotFound
		}

		return nil, err
	}

	return img, nil
}

//ListCompanyImages is
func (s *CompanyService) ListCompanyImages(companyID string) ([]*api.CompanyImage, error) {
	var imgs []*api.CompanyImage
	if err := s.DB.Model(&imgs).Where("company_id = ?", companyID).Select(); err != nil {
		return nil, err
	}

	return imgs, nil
}

//GetCompanyQuery is
func (s *CompanyService) GetCompanyQuery(query string, params ...interface{}) (*api.Company, error) {
	var c api.Company
	if _, err := s.DB.QueryOne(&c, query, params...); err != nil {
		if err == pg.ErrNoRows {
			return nil, api.ErrNotFound
		}
		return nil, err
	}
	return &c, nil
}
