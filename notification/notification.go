package notification

import (
	"bytes"
	"fmt"
	"html/template"
	"log"
	"os"
	"strings"
	"time"

	"gitlab.com/vanoapp/api/bitly"

	"gopkg.in/mgo.v2/bson"

	"gitlab.com/vanoapp/api"
)

// NOTE: this entire file is ugly and put togteher in 5 minutes defenitley revisit this and refactor it.

func fullName(f, l string) string {
	return strings.Title(fmt.Sprintf("%s %s", f, l))
}

// Service is
type Service struct {
	SMSService          api.SMSService
	EmailService        api.EmailService
	ProfessionalService api.ProfessionalService
	CustomerService     api.CustomerService
	CompanyService      api.CompanyService
}

// NotifyNewAppointment is
func (s *Service) NotifyNewAppointment(a *api.Appointment) error {
	staff, err := s.ProfessionalService.GetProfessional(a.ProfessionalID)
	if err != nil {
		return err
	}

	company, err := s.CompanyService.GetCompany(staff.CompanyID)
	if err != nil {
		return err
	}

	zone, err := time.LoadLocation(company.Timezone)
	if err != nil {
		return err
	}

	customer, err := s.CustomerService.GetCustomer(a.CustomerID)
	if err != nil {
		return err
	}

	t := time.Unix(a.StartTime, 0).In(zone).Format("at 3:04pm on Monday, January 2")

	if customer.Notification.SMS {
		msg := fmt.Sprintf("Hey from %s! You scheduled an appointment with %s %s", strings.Title(company.Name), strings.Title(staff.FirstName), t)
		if err := s.SMSService.SendSMS(customer.PhoneNumber, "15024389622", msg); err != nil {
			return err
		}
	}

	switch {
	case staff.Notification.SMS:
		msg := fmt.Sprintf("New Appointment! %s booked a %s %s", fullName(customer.FirstName, customer.LastName), a.Services[0].Name, t)

		if len(a.Services) > 1 {
			var svcs string
			for _, s := range a.Services {
				svcs += fmt.Sprintf("%s, ", s.Name)
			}
			svcs = strings.TrimSuffix(svcs, ", ")
			msg = fmt.Sprintf("New Appointment! %s booked %s for %s", fullName(customer.FirstName, customer.LastName), svcs, t)
		}
		if err := s.SMSService.SendSMS(staff.PhoneNumber, "15024389622", msg); err != nil {
			return err
		}
	}
	return nil
}

// NotifyCancelAppointment is
func (s *Service) NotifyCancelAppointment(a *api.Appointment) error {
	staff, err := s.ProfessionalService.GetProfessional(a.ProfessionalID)
	if err != nil {
		return err
	}

	company, err := s.CompanyService.GetCompany(staff.CompanyID)
	if err != nil {
		return err
	}

	zone, err := time.LoadLocation(company.Timezone)
	if err != nil {
		return err
	}

	customer, err := s.CustomerService.GetCustomer(a.CustomerID)
	if err != nil {
		return err
	}

	t := time.Unix(a.StartTime, 0).In(zone).Format("Monday January 2 3:04pm")
	longURL := fmt.Sprintf("https://clients.vanoapp.com/app/salons/%s", company.ID)

	shortURL, err := bitly.GetShortURL(longURL)
	if err != nil {
		return err
	}

	if customer.Notification.SMS {
		// Hey (Customer)!  (SP) has asked to reschedule your appointment. Please visit (vanoapp.com/spsalon) and book at your earliest convenience. Thank you!
		msg := fmt.Sprintf("Hey %s! %s has cancelled your appointment. You can visit %s to re-book at your earliest convenience. Thank you!", strings.Title(customer.FirstName), strings.Title(staff.FirstName), shortURL)

		if err := s.SMSService.SendMMS(customer.PhoneNumber, "15024389622", msg, []string{"https://res.cloudinary.com/solabuddy/image/upload/v1508552278/h1sf0iggnnmt5nbvc1si.png"}); err != nil {
			return err
		}
	}

	switch {
	case staff.Notification.SMS:
		msg := fmt.Sprintf("Cancelled Appointment, %s cancelled a %s for %s", fullName(customer.FirstName, customer.LastName), a.Services[0].Name, t)

		if len(a.Services) > 1 {
			var svcs string
			for _, s := range a.Services {
				svcs += fmt.Sprintf("%s, ", s.Name)
			}
			svcs = strings.TrimSuffix(svcs, ", ")
			msg = fmt.Sprintf("Cancelled Appointment, %s cancelled %s for %s", fullName(customer.FirstName, customer.LastName), svcs, t)
		}
		if err := s.SMSService.SendSMS(staff.PhoneNumber, "15024389622", msg); err != nil {
			return err
		}
	}

	return nil
}

// NotifyUpdateAppointment is
func (s *Service) NotifyUpdateAppointment(a *api.Appointment) error {
	staff, err := s.ProfessionalService.GetProfessional(a.ProfessionalID)
	if err != nil {
		return err
	}

	company, err := s.CompanyService.GetCompany(staff.CompanyID)
	if err != nil {
		return err
	}

	zone, err := time.LoadLocation(company.Timezone)
	if err != nil {
		return err
	}

	customer, err := s.CustomerService.GetCustomer(a.CustomerID)
	if err != nil {
		return err
	}

	t := time.Unix(a.StartTime, 0).In(zone).Format("Monday January 2 3:04pm")

	if customer.Notification.SMS {
		msg := fmt.Sprintf("Updated Appointment! you updated a %s for %s", a.Services[0].Name, t)

		if len(a.Services) > 1 {
			var svcs string
			for _, s := range a.Services {
				svcs += fmt.Sprintf("%s, ", s.Name)
			}
			svcs = strings.TrimSuffix(svcs, ", ")
			msg = fmt.Sprintf("Updated Appointment! you updated %s for %s", svcs, t)
		}
		if err := s.SMSService.SendSMS(customer.PhoneNumber, "15024389622", msg); err != nil {
			return err
		}
	}

	switch {
	case staff.Notification.SMS:
		msg := fmt.Sprintf("Updated Appointment! %s updated a %s for %s", fullName(customer.FirstName, customer.LastName), a.Services[0].Name, t)

		if len(a.Services) > 1 {
			var svcs string
			for _, s := range a.Services {
				svcs += fmt.Sprintf("%s, ", s.Name)
			}
			svcs = strings.TrimSuffix(svcs, ", ")
			msg = fmt.Sprintf("Updated Appointment! %s updated %s for %s", fullName(customer.FirstName, customer.LastName), svcs, t)
		}
		if err := s.SMSService.SendSMS(staff.PhoneNumber, "15024389622", msg); err != nil {
			return err
		}
	}

	return nil
}

// NotifyNewAdminProfessional is
func (s *Service) NotifyNewAdminProfessional(p *api.Professional) error {
	hash := bson.NewObjectId().Hex()

	company, err := s.CompanyService.GetCompany(p.CompanyID)
	if err != nil {
		return err
	}

	templateData := struct {
		URL         string
		Name        string
		CompanyName string
	}{
		URL:         "https://professionals.vanoapp.com/activate?rid=" + hash,
		Name:        strings.Title(p.FirstName),
		CompanyName: company.Name,
	}
	t, err := template.ParseFiles("./templates/email/new_admin_sp_activate.html")
	if err != nil {
		return fmt.Errorf("error parsing html template. %v", err)
	}
	buf := new(bytes.Buffer)
	if err = t.Execute(buf, templateData); err != nil {
		return err
	}

	to := fmt.Sprintf("%s %s <%s>", p.FirstName, p.LastName, p.Email)
	from := "VANO Client Services <services@vano.ai>"
	subject := "Activate Your VANO Account"
	body := buf.Bytes()
	if err := s.EmailService.SendMail(to, from, subject, body); err != nil {
		return fmt.Errorf("error sending mail. %v", err)
	}

	return nil
}

// NotifyManagedProfessionalInvite is
func (s *Service) NotifyManagedProfessionalInvite(managee, manager *api.Professional) error {
	url := "https://professionals.vanoapp.com"
	if strings.Contains(os.Getenv("BASE_URL"), "localhost") {
		url = "http://localhost:9000"
	}
	log.Println("managee", managee.FirstName)
	log.Println("manager", manager.FirstName)
	fullURL := fmt.Sprintf("%s/manage-professionals/invitations", url)
	templateData := struct {
		URL         string
		Name        string
		ManagerName string
	}{
		URL:         fullURL,
		Name:        strings.Title(managee.FirstName),
		ManagerName: strings.Title(manager.FirstName + " " + manager.LastName),
	}
	t, err := template.ParseFiles("./templates/email/managed_professional_invite.html")
	if err != nil {
		return fmt.Errorf("error parsing html template. %v", err)
	}
	buf := new(bytes.Buffer)
	if err = t.Execute(buf, templateData); err != nil {
		return err
	}

	to := fmt.Sprintf("%s %s <%s>", managee.FirstName, managee.LastName, managee.Email)
	from := "VANO Client Services <services@vano.ai>"
	subject := "New request to manage your account"
	body := buf.Bytes()
	if err := s.EmailService.SendMail(to, from, subject, body); err != nil {
		return fmt.Errorf("error sending mail. %v", err)
	}

	return nil
}

//NotifyProfessionalResetPassword is
func (s *Service) NotifyProfessionalResetPassword(p *api.Professional, req *api.ForgotPasswordRequestTable) error {
	url := "https://professionals.vanoapp.com"
	if strings.Contains(os.Getenv("BASE_URL"), "localhost") {
		url = "http://localhost:9000"
	}
	templateData := struct {
		URL  string
		Hash string
	}{
		URL:  url,
		Hash: req.TextHash,
	}
	t, err := template.ParseFiles("./templates/email/sp_forgot_password.html")
	if err != nil {
		return fmt.Errorf("error parsing html template. %v", err)
	}
	buf := new(bytes.Buffer)
	if err = t.Execute(buf, templateData); err != nil {
		return err
	}

	to := fmt.Sprintf("%s %s <%s>", p.FirstName, p.LastName, p.Email)
	from := "VANO Auth Services <services@vano.ai>"
	subject := "New request reset password"
	body := buf.Bytes()
	if err := s.EmailService.SendMail(to, from, subject, body); err != nil {
		return fmt.Errorf("error sending mail. %v", err)
	}

	return nil
}

//NotifyCustomerResetPassword is
func (s *Service) NotifyCustomerResetPassword(c *api.Customer, req *api.ForgotPasswordRequestTable) error {
	url := "https://clients.vanoapp.com"
	if strings.Contains(os.Getenv("BASE_URL"), "localhost") {
		url = "http://localhost:9001"
	}
	templateData := struct {
		URL  string
		Hash string
	}{
		URL:  url,
		Hash: req.TextHash,
	}
	t, err := template.ParseFiles("./templates/email/sp_forgot_password.html")
	if err != nil {
		return fmt.Errorf("error parsing html template. %v", err)
	}
	buf := new(bytes.Buffer)
	if err = t.Execute(buf, templateData); err != nil {
		return err
	}

	to := fmt.Sprintf("%s %s <%s>", c.FirstName, c.LastName, c.Email)
	from := "VANO Auth Services <services@vano.ai>"
	subject := "New request reset password"
	body := buf.Bytes()
	if err := s.EmailService.SendMail(to, from, subject, body); err != nil {
		return fmt.Errorf("error sending mail. %v", err)
	}

	return nil
}
