package api

import (
	"time"

	uuid "github.com/satori/go.uuid"
)

// MerchantService deals with the storing of merchant info. Usually dealing with some sort of database.
type MerchantService interface {
	CreateMerchant(m *Merchant) error
	GetCompanyMerchant(companyID string) (*Merchant, error)
	GetProfessionalMerchant(professionalID string) (*Merchant, error)
}

// Merchant is
type Merchant struct {
	ID             string    `json:"id"`
	CompanyID      string    `json:"company_id"`
	ProfessionalID string    `json:"professional_id"`
	SplashID       string    `json:"splash_id"`
	EIN            string    `json:"ein"`
	SSN            string    `json:"ssn" sql:"-"`
	AnnualCCSales  int       `json:"annual_cc_sales"`
	AccountNumber  string    `json:"account_number"`
	RoutingNumber  string    `json:"routing_number"`
	AccountType    string    `json:"account_type"`
	DLNumber       string    `json:"dl_number"`
	DLState        string    `json:"dl_state"`
	CreatedAt      time.Time `json:"-"`
	UpdatedAt      time.Time `json:"-"`
}

// New is
func (m *Merchant) New() {
	m.ID = uuid.NewV4().String()
	m.CreatedAt = time.Now()
	m.UpdatedAt = time.Now()
}
