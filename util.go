package api

import (
	"crypto/md5"
	"encoding/hex"
)

//GenerateMD5Hash is
func GenerateMD5Hash(text string) string {
	hasher := md5.New()
	hasher.Write([]byte(text))
	return hex.EncodeToString(hasher.Sum(nil))
}
