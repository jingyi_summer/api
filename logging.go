package api

import "net/http"

//ReqIDContextKey is
type ReqIDContextKey string

//GetReqID is
func GetReqID(r *http.Request) string {
	return r.Context().Value(ReqIDContextKey("req_id")).(string)
}
