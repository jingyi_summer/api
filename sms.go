package api

//SMSService is
type SMSService interface {
	SendSMS(to, from, txt string) error
	SendMMS(to, from, txt string, media []string) error
}
