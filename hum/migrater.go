package hum

// Migrater is
type Migrater interface {
	Migrate() error
	RollBack() error
}

// Migrate is
func Migrate(m Migrater) error {
	return m.Migrate()
}
