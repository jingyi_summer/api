package data

// EmailExtensions is
var EmailExtensions = []string{
	"email",
	"gmail",
	"hotmail",
	"zoho",
	"yahoo",
	"msn",
	"outlook",
}

// EmailTLD is
var EmailTLD = []string{
	"gov",
	"com",
	"net",
	"co",
	"me",
	"we",
	"ai",
}
