package hum

import (
	"fmt"

	"gitlab.com/vanoapp/api/hum/data"
)

// FirstName generates a random first name
func FirstName() string {
	fn := append(data.FemaleFirstName, data.MaleFirstName...)
	return fn[index(fn)]
}

// FullName generates a random full name
func FullName() string {
	fn := append(data.FemaleFirstName, data.MaleFirstName...)
	return fmt.Sprintf("%s %s", fn[index(fn)], data.LastName[index(data.LastName)])
}

// LastName generates a random last name
func LastName() string {
	return data.LastName[index(data.LastName)]
}

// Company is
func Company() string {
	cs := data.CatchPhrase[index(data.CatchPhrase)]
	bs := data.BSWord[index(data.BSWord)]
	return cs + " " + bs
}

// Email generates a random email
func Email() string {
	fn := FirstName()
	ln := LastName()
	emailExt := data.EmailExtensions[index(data.EmailExtensions)]
	emailTLD := data.EmailTLD[index(data.EmailTLD)]
	return fmt.Sprintf("%s.%s@%s.%s", fn, ln, emailExt, emailTLD)
}

// PhoneNumber is
func PhoneNumber() string {
	return fmt.Sprintf("%d%d%d", rangeInt(100, 999), rangeInt(100, 999), rangeInt(1000, 9999))
}

// RangeInt is
func RangeInt(min, max int) int {
	return rangeInt(min, max)
}

// Address is
func Address() string {
	strtNum := rangeInt(100, 9999)
	strtName := LastName()
	strtExt := data.AddressExt[index(data.AddressExt)]
	return fmt.Sprintf("%d %s %s", strtNum, strtName, strtExt)
}

// City is
func City() string {
	return data.Cities[len(data.Cities)-1]
}

// Country is
func Country() string {
	return data.Countries[index(data.Countries)]
}

// ZipCode is
func ZipCode() int {
	return rangeInt(10000, 999999)
}

// State is
func State() string {
	return data.States[index(data.States)]
}

// StateAbbr is
func StateAbbr() string {
	return data.StatesAbbr[index(data.StatesAbbr)]
}

// FullAddress is
func FullAddress() string {
	return fmt.Sprintf("%s, %s, %s, %d, %s", Address(), City(), State(), ZipCode(), Country())
}
