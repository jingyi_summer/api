package postgre

const (
	Text                   = "text"
	UUID                   = "uuid"
	Int                    = "integer"
	BigInt                 = "bigint"
	Bool                   = "boolean"
	JSON                   = "json"
	JSONB                  = "jsonb"
	Timestamp              = "timestamp"
	TimestampWtTimezone    = "timestamp with time zone"
	TimestampWtOutTimezone = "timestamp without time zone"
	Serial                 = "serial"
)
