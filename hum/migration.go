package hum

// Migration is
type Migration struct {
	Name string
	Up   string
	Down string
}

// // UpAction is
// func (m *Migration) UpAction() string {
// 	return m.Up.Action
// }

// // UpColumns is
// func (m *Migration) UpColumns() []*Column {
// 	return m.Up.Columns
// }

// // UpTable is
// func (m *Migration) UpTable() string {
// 	return m.Up.Table
// }

// //Schema is
// type Schema struct {
// 	Action  string
// 	Table   string
// 	Columns []*Column
// }

// // Column is
// type Column struct {
// 	Name          string
// 	Type          string
// 	Unique        bool
// 	Index         bool
// 	Default       string
// 	PrimaryKey    bool
// 	NotNull       bool
// 	Length        int
// 	AutoIncrement bool
// 	ForeignKey    *ForeignKey
// 	Check         string
// }

// // ForeignKey is
// type ForeignKey struct {
// 	Table string
// }

// MigrationTable is
type MigrationTable struct {
	TableName struct{} `sql:"migrations" json:"-"`
	ID        int
	Name      string
	Batch     int
}
