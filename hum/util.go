package hum

import (
	"math/rand"
	"time"
)

func index(s []string) int {
	return rangeInt(0, len(s)-1)
}

func rangeInt(min, max int) int {
	rand.Seed(time.Now().UnixNano())
	return rand.Intn(max-min) + min
}

// StringInSlice is
func StringInSlice(list []string, a string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}
