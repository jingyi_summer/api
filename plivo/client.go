package plivo

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
)

// DefaultBaseURL is where govtrack expects its api calls
const DefaultBaseURL = "https://api.plivo.com/v1"

//Client is
type Client struct {
	HTTPClient *http.Client
	BaseURL    string
	AuthToken  string
	AuthID     string
}

// ErrBadStatusCode is returned when the API returns a non 200 error code
type ErrBadStatusCode struct {
	OriginalBody string
	Code         int
}

func (e *ErrBadStatusCode) Error() string {
	return fmt.Sprintf("Invalid status code: %d, %s", e.Code, e.OriginalBody)
}

// ErrNotExpectedJSON is returned by API calls when the response isn't expected JSON
type ErrNotExpectedJSON struct {
	OriginalBody string
	Err          error
}

func (e *ErrNotExpectedJSON) Error() string {
	return fmt.Sprintf("Unexpected JSON: %s from %s", e.Err.Error(), e.OriginalBody)
}

//SuccessResponse is
type SuccessResponse struct {
	APIID       string `json:"api_id"`
	Message     string `json:"message"`
	RequestUUID string `json:"request_uuid"`
}

type errResponse struct {
	APIID       string `json:"api_id"`
	Error       string `json:"error"`
	RequestUUID string `json:"request_uuid"`
}

func (c *Client) urlBase(resource string) string {
	base := c.BaseURL
	if c.BaseURL == "" {
		base = DefaultBaseURL
	}
	return fmt.Sprintf("%s/Account/%s/%s", base, c.AuthID, resource)
}

func (c *Client) postRequest(u string, jsonReq, jsonResp interface{}, httpStatusCode int) error {
	j, err := json.Marshal(jsonReq)
	if err != nil {
		return err
	}

	req, err := http.NewRequest("POST", u, bytes.NewReader(j))
	if err != nil {
		return err
	}
	req.SetBasicAuth(c.AuthID, c.AuthToken)
	req.Header.Add("Content-Type", "application/json")

	resp, err := c.HTTPClient.Do(req)
	if err != nil {
		return err
	}

	var b bytes.Buffer
	if _, err := io.Copy(&b, resp.Body); err != nil {
		return err
	}
	defer resp.Body.Close()
	debug := b.String()

	log.Println(debug)

	if resp.StatusCode != httpStatusCode {
		return &ErrBadStatusCode{
			OriginalBody: debug,
			Code:         resp.StatusCode,
		}
	}

	if err := json.NewDecoder(&b).Decode(jsonResp); err != nil {
		return &ErrNotExpectedJSON{
			OriginalBody: "",
			Err:          err,
		}
	}

	return nil

}
