package plivo

import (
	"fmt"
	"log"
)

//SMSService is
type SMSService struct {
	Client *Client
}

//SendSMS is
func (s *SMSService) SendSMS(to, from, txt string) error {
	req := &SendMessageReq{
		Src: to,
		Dst: fmt.Sprintf("1%s", from),
		Txt: txt,
	}
	log.Println("req", req)
	if _, err := s.Client.SendMessage(req); err != nil {
		return err
	}

	return nil
}
