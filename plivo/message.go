package plivo

import (
	"log"
	"net/http"
)

//SendMessageReq is
type SendMessageReq struct {
	Src    string `json:"src"`
	Dst    string `json:"dst"`
	Txt    string `json:"text"`
	Type   string `json:"type"`
	URL    string `json:"url"`
	Method string `json:"method"`
	Log    bool   `json:"log"`
}

//SendMessage is
func (c *Client) SendMessage(req *SendMessageReq) (*SuccessResponse, error) {
	var resp SuccessResponse
	if err := c.postRequest(c.urlBase("Message/"), req, &resp, http.StatusAccepted); err != nil {
		return nil, err
	}
	log.Println(resp)
	return &resp, nil
}
