package api

import "testing"

func TestCustomer_New(t *testing.T) {
}

func TestCustomer_HashPassword(t *testing.T) {
	tests := []struct {
		password string
		isHashed bool
	}{
		{"unhash", true},
		{"", false},
	}

	for _, test := range tests {
		testCustomer.Password = test.password
		if err := testCustomer.HashPassword(); err != nil {
			t.Error(err)
			continue
		}

		if len(test.password) == 0 {
			switch {
			case testCustomer.Password != "":
				t.Error("expecetd password to not be hashed if empty")
			}
		}

		if len(test.password) > 0 {
			switch {
			case test.password == testCustomer.Password:
				t.Error("Expected password to be hashed")
			}
		}
	}
}
