package bandwidth

import (
	bandwidth "github.com/bandwidthcom/go-bandwidth"
)

//SMSService is
type SMSService struct {
	Client *bandwidth.Client
}

// New is
func New(userID, apiToken, apiSecret string, other ...string) (*bandwidth.Client, error) {
	return bandwidth.New(userID, apiToken, apiSecret)
}

//SendSMS is
func (s *SMSService) SendSMS(to, from, txt string) error {
	req := bandwidth.CreateMessageData{
		To:   to,
		From: from,
		Text: txt,
	}

	if _, err := s.Client.CreateMessage(&req); err != nil {
		return err
	}

	return nil
}

//SendMMS is
func (s *SMSService) SendMMS(to, from, txt string, media []string) error {
	req := bandwidth.CreateMessageData{
		To:    to,
		From:  from,
		Text:  txt,
		Media: media,
	}

	if _, err := s.Client.CreateMessage(&req); err != nil {
		return err
	}

	return nil
}
