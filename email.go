package api

// EmailService is
type EmailService interface {
	SendMail(to, from, subject string, body []byte) error
}
