package api

import (
	"errors"
	"time"

	uuid "github.com/satori/go.uuid"
)

// Reminder is
type Reminder struct {
	ID             string    `json:"id"`
	ProfessionalID string    `json:"professional_id"`
	Text           string    `json:"text"`
	DueDate        int64     `json:"due_date"`
	Completed      bool      `json:"completed" sql:",notnull"`
	Description    string    `json:"description"`
	Notified       bool      `json:"notified" sql:",notnull"`
	CreatedAt      time.Time `json:"-"`
	UpdatedAt      time.Time `json:"-"`
}

// UpdateReminder is
type UpdateReminder struct {
	ID          string `json:"id"`
	Text        string `json:"text"`
	DueDate     int64  `json:"due_date"`
	Description string `json:"description"`
	Completed   bool   `json:"completed" sql:",notnull"`
}

// New is
func (r *Reminder) New() {
	r.ID = uuid.NewV4().String()
	r.CreatedAt = time.Now()
	r.UpdatedAt = time.Now()
}

// ValidateCreate is
func (r *Reminder) ValidateCreate() error {
	switch {
	case r.DueDate == 0:
		return errors.New("due date is required")
	case r.ProfessionalID == "":
		return errors.New("professional id is required")
	case r.Text == "":
		return errors.New("text is required")
	}

	return nil
}

// ValidateUpdate is
func (r *UpdateReminder) ValidateUpdate() error {
	switch {
	case r.DueDate == 0:
		return errors.New("due date is required")
	case r.ID == "":
		return errors.New("reminder id is required")
	case r.Text == "":
		return errors.New("text is required")
	}

	return nil
}

// ReminderService is
type ReminderService interface {
	CreateReminder(r *Reminder) error
	UpdateReminder(r *UpdateReminder) (*Reminder, error)
	GetProfessionalReminders(professionalID string, limit int, completed bool) ([]*Reminder, error)
	DeleteReminder(id string) error
	GetReminder(id string) (*Reminder, error)
}
