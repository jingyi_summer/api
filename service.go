package api

import (
	"errors"
	"time"
)

// ServiceService is
type ServiceService interface {
	CreateService(o *Service) (*Service, error)
	GetService(id string) (*Service, error)
	RemoveService(id string) error
	UpdateService(o *Service) error
}

// Service is
type Service struct {
	ID               string     `json:"id"`
	ProfessionalID   string     `json:"professional_id"`
	Steps            []*Service `json:"steps"`
	ServiceID        string     `json:"-"`
	Name             string     `json:"name"`
	Description      string     `json:"description"`
	Length           int        `json:"length"`
	Price            int        `json:"price"` // price in cents
	RequirePhoneCall bool       `json:"require_phone_call" sql:",notnull"`
	IsMaster         bool       `json:"is_master" sql:",notnull"`
	IsGap            bool       `json:"is_gap" sql:",notnull"`
	CreatedAt        time.Time  `json:"-"`
	UpdatedAt        time.Time  `json:"-"`
}

// ValidateUpdate is
func (s *Service) ValidateUpdate() error {
	switch {
	case s.ID == "":
		return errors.New("id is required")
	case s.ProfessionalID == "":
		return errors.New("professional id required")
	case s.Name == "":
		return errors.New("name is required")
	case s.Length == 0:
		return errors.New("length is required")
	case s.Price == 0:
		return errors.New("price is required")
	}

	return nil
}
