package api

import (
	"errors"
	"os"
	"time"

	multierror "github.com/hashicorp/go-multierror"
	uuid "github.com/satori/go.uuid"

	"strconv"

	"golang.org/x/crypto/bcrypt"
)

// CustomerService is
type CustomerService interface {
	CreateCustomer(c *CreateCustomer) (*Customer, error)
	AddCustomerCard(c *CustomerCard) error
	RemoveCustomerCard(id string) error
	GetCustomerCards(customerID string) ([]*CustomerCard, error)
	GetCustomer(id string) (*Customer, error)
	GetCustomerQuery(query string, params ...interface{}) (*Customer, error)
	ListCompanyCustomers(companyID string, page int) ([]*Customer, error)
	ListProfessionalCustomers(professionalID string, page int) ([]*Customer, error)
	UpdateCustomer(c *Customer) error
	AddCustomerNote(n *CustomerNote) error
	RemoveCustomerNote(id string) error
	UpdateCustomerNote(n *CustomerNote) error
	CheckCustomerExists(email, phoneNumber string) (*Customer, error)
	AddCustomerCompany(customerID, companyID string) error
	AddProfessionalCustomer(customerID, professionalID string) error
	RemoveCustomerCompany(customerID, companyID string) error
	ResetPassword(id, password, newPassword string) error
	SetAccount(id, password string) error
	SetSplashID(id, splashID string) (*Customer, error)
	GetCustomerComapanies(id string) ([]string, error)
	AddForgotPasswordRequest(id string) (*ForgotPasswordRequestTable, error)
	NoAuthResetRequest(planHash, newPassword string) error
	GetCustomerCardByToken(t string) (*CustomerCard, error)
	AddServiceToCustomer(customerID, serviceID string, length int) (*CustomerServicesTable, error)
	RemoveServiceFromCustomer(id string) error
	ListCustomerServices(customerID string) ([]*CustomerServicesTable, error)
	// AddProfessionalCustomer(customerID, professionalID string) error
	// removeProfessionalCustomer(customerID, professionalID string) error
}

// SanitizedCustomer is
type SanitizedCustomer struct {
	ID           string          `json:"id"`
	Companies    []string        `json:"companies" pg:",many2many:company_customer"`
	FirstName    string          `json:"first_name"`
	LastName     string          `json:"last_name"`
	Email        string          `json:"email"`
	PhoneNumber  string          `json:"phone_number"`
	PasswordSet  bool            `json:"password_set"`
	Notes        []*CustomerNote `json:"notes"`
	Cards        []*CustomerCard `json:"cards"`
	Notification *Notification   `json:"notification"`
}

// Customer is
type Customer struct {
	ID           string          `json:"id"`
	SplashID     string          `json:"-"`
	Companies    []*Company      `json:"companies" pg:",many2many:company_customer"`
	FirstName    string          `json:"first_name"`
	LastName     string          `json:"last_name"`
	Email        string          `json:"email"`
	Password     string          `json:"password"`
	PhoneNumber  string          `json:"phone_number"`
	Notes        []*CustomerNote `json:"notes"`
	Cards        []*CustomerCard `json:"cards"`
	IPAddress    string          `json:"-"`
	LastLogin    time.Time       `json:"-"`
	Notification *Notification   `json:"notification"`
	CreatedAt    time.Time       `json:"-"`
	UpdatedAt    time.Time       `json:"-"`
}

// CreateCustomer is
type CreateCustomer struct {
	Fresh          bool   `json:"fresh"` // If true it means this is a completly new customer does not belong to a company or professional
	CompanyID      string `json:"company_id"`
	ProfessionalID string `json:"professional_id"`
	FirstName      string `json:"first_name"`
	LastName       string `json:"last_name"`
	Email          string `json:"email"`
	Password       string `json:"password"`
	PhoneNumber    string `json:"phone_number"`
	SplashID       string `json:"-"`
}

// NewCustomerRequest is
type NewCustomerRequest struct {
	Customer *CreateCustomer `json:"customer"`
}

//Validate is
func (r *NewCustomerRequest) Validate() error {
	var result error
	if r.Customer.FirstName == "" {
		result = multierror.Append(result, errors.New("first name is required"))
	}
	if r.Customer.LastName == "" {
		result = multierror.Append(result, errors.New("last name is required"))
	}
	if r.Customer.PhoneNumber == "" {
		result = multierror.Append(result, errors.New("phone number is required"))
	}
	if !r.Customer.Fresh {
		if r.Customer.CompanyID == "" && r.Customer.ProfessionalID == "" {
			result = multierror.Append(result, errors.New("either professional_id or company_id is required"))
		}
	} else {
		if r.Customer.Password == "" {
			result = multierror.Append(result, errors.New("password is required"))
		}
	}
	return result
}

// New creates the fields needed to create a new customer and add them to the db
func (c *Customer) New() {
	c.ID = uuid.NewV4().String()
	c.Notification = &Notification{
		SMS:   true,
		Email: true,
		Phone: false,
	}
	c.CreatedAt = time.Now()
	c.UpdatedAt = time.Now()
}

// Update is
func (c *Customer) Update() {
	c.UpdatedAt = time.Now()
}

// ValidateCreate is
func (c *Customer) ValidateCreate() error {
	switch {
	case c.FirstName == "":
		return errors.New("first name is required")
	case c.LastName == "":
		return errors.New("last name is required")
	case c.PhoneNumber == "":
		return errors.New("phone number is required")
	}

	return nil
}

// ValidateUpdate is
func (c *Customer) ValidateUpdate() error {
	switch {
	case c.ID == "":
		return errors.New("id is required")
	case c.FirstName == "":
		return errors.New("first name is required")
	case c.LastName == "":
		return errors.New("last name is required")
	case c.PhoneNumber == "":
		return errors.New("phone number is required")
	case c.Notification == nil:
		return errors.New("notification is required")
	}

	return nil
}

// HashPassword hashes a customer's password. If the customer is created by a Professional member then they will not have a password thats why we do that initial check to see if the password is empty
func (c *Customer) HashPassword() error {
	if c.Password == "" {
		return nil
	}

	p, err := bcrypt.GenerateFromPassword([]byte(c.Password), bcrypt.DefaultCost)
	if err != nil {
		return err
	}

	c.Password = string(p)

	return nil
}

// SanitizedCustomerCard is
type SanitizedCustomerCard struct {
	ID         string `json:"id"`
	CustomerID string `json:"customer_id"`
	LastFour   int    `json:"last_four"`
	Exp        string `json:"exp"`
	Name       string `json:"name"`
	Token      string `json:"token"`
	Method     string `json:"method"`
}

// CustomerCard is
type CustomerCard struct {
	ID         string    `json:"id"`
	CustomerID string    `json:"customer_id"`
	LastFour   int       `json:"last_four"`
	Number     string    `json:"number" sql:"-"`
	CVV        string    `json:"cvv" sql:"-"`
	Exp        string    `json:"exp"`
	Name       string    `json:"name"`
	Token      string    `json:"token"`
	Method     string    `json:"method"`
	CreatedAt  time.Time `json:"-"`
	UpdatedAt  time.Time `json:"-"`
}

// New is
func (c *CustomerCard) New() error {
	c.ID = uuid.NewV4().String()
	c.CreatedAt = time.Now()
	c.UpdatedAt = time.Now()
	if len(c.Number) < 4 {
		return errors.New("invalid card")
	}
	lastFour, err := strconv.Atoi(c.Number[len(c.Number)-4:])
	if err != nil {
		return err
	}
	c.LastFour = lastFour
	return nil
}

// ValidateCreate is
func (c *CustomerCard) ValidateCreate() error {
	switch {
	case c.CustomerID == "":
		return errors.New("customer id is required")
	case c.Exp == "":
		return errors.New("card expiration is required")
	case c.Name == "":
		return errors.New("card holder name is required")
	case !c.validateNumber(os.Getenv("DEBUG") == "true"):
		return errors.New("number is invalid")
	case c.Method == "":
		return errors.New("method is required")
	}

	return nil
}

func (c *CustomerCard) validateNumber(test bool) bool {
	switch c.Number {
	// test cards: https://stripe.com/docs/testing
	case "4242424242424242",
		"4012888888881881",
		"4000056655665556",
		"5555555555554444",
		"5200828282828210",
		"5105105105105100",
		"378282246310005",
		"371449635398431",
		"6011111111111117",
		"6011000990139424",
		"30569309025904",
		"38520000023237",
		"3530111333300000",
		"4444444444444444",
		"3566002020360505":
		if test {
			return true
		}

		return false
	}

	var sum int
	var alternate bool

	numberLen := len(c.Number)

	if numberLen < 13 || numberLen > 19 {
		return false
	}

	for i := numberLen - 1; i > -1; i-- {
		mod, _ := strconv.Atoi(string(c.Number[i]))
		if alternate {
			mod *= 2
			if mod > 9 {
				mod = (mod % 10) + 1
			}
		}

		alternate = !alternate

		sum += mod
	}

	return sum%10 == 0
}

// CompanyCustomer is
type CompanyCustomer struct {
	TableName  struct{} `sql:"company_customer" json:"-"`
	ID         string
	CompanyID  string
	CustomerID string
}

// CustomerNote is
type CustomerNote struct {
	ID         string    `json:"id"`
	CustomerID string    `json:"customer_id"`
	Value      string    `json:"value"`
	CompanyID  string    `json:"company_id"`
	CreatedAt  time.Time `json:"-"`
	UpdatedAt  time.Time `json:"-"`
}

// ValidateCreate is
func (n *CustomerNote) ValidateCreate() error {
	switch {
	case n.CustomerID == "":
		return errors.New("customer is required")
	case n.Value == "":
		return errors.New("note value is required")
	case n.CompanyID == "":
		return errors.New("company id is required")
	}

	return nil
}

// ValidateUpdate is
func (n *CustomerNote) ValidateUpdate() error {
	switch {
	case n.ID == "":
		return errors.New("id is required")
	case n.Value == "":
		return errors.New("note value is required")
	}

	return nil
}

// New is
func (n *CustomerNote) New() {
	n.ID = uuid.NewV4().String()
	n.CreatedAt = time.Now()
	n.UpdatedAt = time.Now()
}

// CustomerServicesTable is
type CustomerServicesTable struct {
	TableName  struct{} `sql:"customer_services" json:"-"`
	ID         string   `json:"id"`
	CustomerID string   `json:"customer_id"`
	ServiceID  string   `json:"service_id"`
	Length     int      `json:"length"`
}

// CustomerProfessional is
type CustomerProfessional struct {
	TableName      struct{} `sql:"customer_professional" json:"-"`
	ID             string   `json:"id"`
	CustomerID     string   `json:"customer_id"`
	ProfessionalID string   `json:"professional_id"`
}
