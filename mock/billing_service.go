package mock

import "gitlab.com/vanoapp/api"

// BillingService is
type BillingService struct {
}

// OnboardMerchant is
func (s *BillingService) OnboardMerchant(m *api.Merchant, c *api.Company, professional *api.Professional) error {
	return nil
}

// Charge is
func (s *BillingService) Charge(t *api.Transaction) error {
	return nil
}

// CreateMerchantCustomer is
func (s *BillingService) CreateMerchantCustomer(c *api.Customer) error {
	return nil
}

// CreateToken is
func (s *BillingService) CreateToken(c *api.CustomerCard) error {
	return nil
}

// GetCompanyTransactions is
func (s *BillingService) GetCompanyTransactions(companyID, date string) (interface{}, error) {
	return nil, nil
}

// Refund is
func (s *BillingService) Refund(transactionID string) error {
	return nil
}
