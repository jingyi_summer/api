package mock

import "gitlab.com/vanoapp/api"

// AnalyticsService is
type AnalyticsService struct {
}

// GetProfessionalAnalytics is
func (s *AnalyticsService) GetProfessionalAnalytics(professionalID string) (*api.Analytics, error) {
	if professionalID != "correctid" {
		return nil, api.ErrNotFound
	}

	a := new(api.Analytics)

	return a, nil
}
