package mock

import (
	"time"

	"gopkg.in/mgo.v2/bson"

	uuid "github.com/satori/go.uuid"
	"gitlab.com/vanoapp/api"
)

var testCorrectCustomerID = "correctid"
var testCorrectCustomerNoteID = "correctid"

// CustomerService is
type CustomerService struct {
}

// CreateCustomer is
func (s *CustomerService) CreateCustomer(c *api.CreateCustomer) (*api.Customer, error) {
	return &api.Customer{}, nil
}

// RemoveCustomerCard is
func (s *CustomerService) RemoveCustomerCard(id string) error {
	if testCorrectCustomerID != id {
		return api.ErrNotFound
	}
	return nil
}

// AddCustomerCard is
func (s *CustomerService) AddCustomerCard(c *api.CustomerCard) error {
	c.ID = uuid.NewV4().String()
	c.CreatedAt = time.Now()
	c.UpdatedAt = time.Now()

	return nil
}

// GetCustomerCards is
func (s *CustomerService) GetCustomerCards(customerID string) ([]*api.CustomerCard, error) {
	if testCorrectCustomerID != customerID {
		return nil, api.ErrNotFound
	}
	var cards []*api.CustomerCard

	return cards, nil
}

// GetCustomer is
func (s *CustomerService) GetCustomer(id string) (*api.Customer, error) {
	if testCorrectCustomerID != id {
		return nil, api.ErrNotFound
	}

	c := api.Customer{
		ID:          uuid.NewV4().String(),
		Companies:   []*api.Company{},
		FirstName:   "test first name",
		LastName:    "test last name",
		Email:       "sup@email.com",
		PhoneNumber: "9234923943",
		Password:    "testpasswordfrommain!",
		CreatedAt:   time.Now(),
		UpdatedAt:   time.Now(),
	}

	return &c, nil
}

// CheckCustomerExists is
func (s *CustomerService) CheckCustomerExists(email, phoneNumber string) (*api.Customer, error) {
	if email != "correctemail" && phoneNumber != "correctnumber" {
		return nil, api.ErrNotFound
	}

	c := api.Customer{
		ID:          uuid.NewV4().String(),
		Companies:   []*api.Company{},
		FirstName:   "test first name",
		LastName:    "test last name",
		Email:       "sup@email.com",
		PhoneNumber: "9234923943",
		Password:    "testpasswordfrommain!",
		CreatedAt:   time.Now(),
		UpdatedAt:   time.Now(),
	}

	return &c, nil
}

// SetAccount is
func (s *CustomerService) SetAccount(id, password string) error {
	return nil
}

// GetCustomerQuery is
func (s *CustomerService) GetCustomerQuery(query string, params ...interface{}) (*api.Customer, error) {
	c := api.Customer{
		ID:          uuid.NewV4().String(),
		Companies:   []*api.Company{},
		FirstName:   "test first name",
		LastName:    "test last name",
		Email:       "sup@email.com",
		PhoneNumber: "9234923943",
		Password:    "testpasswordfrommain!",
		CreatedAt:   time.Now(),
		UpdatedAt:   time.Now(),
	}

	return &c, nil
}

// ListCompanyCustomers is
func (s *CustomerService) ListCompanyCustomers(companyID string, limit int) ([]*api.Customer, error) {
	c := []*api.Customer{
		&api.Customer{
			ID:          uuid.NewV4().String(),
			Companies:   []*api.Company{},
			FirstName:   "test first name",
			LastName:    "test last name",
			Email:       "sup@email.com",
			PhoneNumber: "9234923943",
			Password:    "testpasswordfrommain!",
			CreatedAt:   time.Now(),
			UpdatedAt:   time.Now(),
		},
		&api.Customer{
			ID:          uuid.NewV4().String(),
			Companies:   []*api.Company{},
			FirstName:   "test first name",
			LastName:    "test last name",
			Email:       "sup@email.com",
			PhoneNumber: "9234923943",
			Password:    "testpasswordfrommain!",
			CreatedAt:   time.Now(),
			UpdatedAt:   time.Now(),
		},
	}

	return c, nil
}

// UpdateCustomer is
func (s *CustomerService) UpdateCustomer(c *api.Customer) error {
	return nil
}

// AddCustomerNote is
func (s *CustomerService) AddCustomerNote(n *api.CustomerNote) error {
	return nil
}

// RemoveCustomerNote is
func (s *CustomerService) RemoveCustomerNote(id string) error {
	if testCorrectCustomerNoteID != id {
		return api.ErrNotFound
	}

	return nil
}

// UpdateCustomerNote is
func (s *CustomerService) UpdateCustomerNote(n *api.CustomerNote) error {
	return nil
}

// AddCustomerCompany is
func (s *CustomerService) AddCustomerCompany(customerID, companyID string) error {
	return nil
}

// RemoveCustomerCompany is
func (s *CustomerService) RemoveCustomerCompany(customerID, companyID string) error {
	return nil
}

// ResetPassword is
func (s *CustomerService) ResetPassword(id, password, newPassword string) error {
	if id != testCorrectCustomerID {
		return api.ErrNotFound
	}
	return nil
}

// SetSplashID is
func (s *CustomerService) SetSplashID(id, splashID string) (*api.Customer, error) {
	var c *api.Customer
	return c, nil
}

//GetCustomerComapanies is
func (s *CustomerService) GetCustomerComapanies(id string) ([]string, error) {
	resp := []string{}
	return resp, nil
}

//AddForgotPasswordRequest is
func (s *CustomerService) AddForgotPasswordRequest(id string) (*api.ForgotPasswordRequestTable, error) {
	req := new(api.ForgotPasswordRequestTable)
	req.ID = uuid.NewV4().String()
	req.CustomerID = id
	req.CreatedAt = time.Now()
	req.UpdatedAt = time.Now()
	req.TextHash = bson.NewObjectId().Hex()
	req.Hash = api.GenerateMD5Hash(req.TextHash)
	return req, nil
}

//NoAuthResetRequest is
func (s *CustomerService) NoAuthResetRequest(plainHash, newPassword string) error {
	return nil
}

// GetCustomerCardByToken is
func (s *CustomerService) GetCustomerCardByToken(t string) (*api.CustomerCard, error) {
	c := new(api.CustomerCard)

	return c, nil
}

// AddServiceToCustomer is
func (s *CustomerService) AddServiceToCustomer(customerID, serviceID string, length int) (*api.CustomerServicesTable, error) {
	return nil, nil
}

// RemoveServiceFromCustomer is
func (s *CustomerService) RemoveServiceFromCustomer(id string) error {
	return nil
}

// ListCustomerServices is
func (s *CustomerService) ListCustomerServices(customerID string) ([]*api.CustomerServicesTable, error) {
	var cs []*api.CustomerServicesTable

	return cs, nil
}

// ListProfessionalCustomers is
func (s *CustomerService) ListProfessionalCustomers(professionalID string, page int) ([]*api.Customer, error) {
	var c []*api.Customer

	return c, nil
}

//AddProfessionalCustomer is
func (s *CustomerService) AddProfessionalCustomer(customerID, professionalID string) error {

	return nil
}
