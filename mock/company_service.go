package mock

import (
	"time"

	uuid "github.com/satori/go.uuid"

	"gitlab.com/vanoapp/api"
)

var testCompanyID = "correctid"

// CompanyService is
type CompanyService struct {
}

// CreateCompany is
func (s *CompanyService) CreateCompany(c *api.Company) error {
	c.ID = uuid.NewV4().String()
	c.CreatedAt = time.Now()
	c.UpdatedAt = time.Now()
	return nil
}

// GetCompany is
func (s *CompanyService) GetCompany(id string) (*api.Company, error) {
	if id != testCompanyID {
		return nil, api.ErrNotFound
	}
	c := api.Company{
		ID:        uuid.NewV4().String(),
		Name:      "test company",
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}
	return &c, nil
}

// ListCompanies is
func (s *CompanyService) ListCompanies(limit int) ([]*api.Company, error) {
	c := []*api.Company{
		&api.Company{
			ID:        uuid.NewV4().String(),
			Name:      "test company",
			CreatedAt: time.Now(),
			UpdatedAt: time.Now(),
		},
		&api.Company{
			ID:        uuid.NewV4().String(),
			Name:      "test company",
			CreatedAt: time.Now(),
			UpdatedAt: time.Now(),
		},
		&api.Company{
			ID:        uuid.NewV4().String(),
			Name:      "test company",
			CreatedAt: time.Now(),
			UpdatedAt: time.Now(),
		},
	}

	return c, nil
}

// UpdateCompany is
func (s *CompanyService) UpdateCompany(c *api.Company) error {
	if c.ID != testCompanyID {
		return api.ErrNotFound
	}
	c.Name = "updated company"
	return nil
}

//AddCompanyImages is
func (s *CompanyService) AddCompanyImages(imgs []*api.CompanyImage) ([]*api.CompanyImage, error) {
	var newImages []*api.CompanyImage
	for _, img := range imgs {
		img.ID = uuid.NewV4().String()
		newImages = append(newImages, img)
	}

	return newImages, nil
}

//AddCompanyImage is
func (s *CompanyService) AddCompanyImage(img *api.CompanyImage) (*api.CompanyImage, error) {
	newImg := img
	newImg.ID = uuid.NewV4().String()

	return newImg, nil
}

//RemoveCompanyImage is
func (s *CompanyService) RemoveCompanyImage(id string) error {
	if id != testCompanyID {
		return api.ErrNotFound
	}

	return nil
}

//ListCompanyImages is
func (s *CompanyService) ListCompanyImages(companyID string) ([]*api.CompanyImage, error) {
	var imgs []*api.CompanyImage

	return imgs, nil
}
