package mock

import "gitlab.com/vanoapp/api"

// TransactionService is
type TransactionService struct {
}

// CreateTransaction is
func (s *TransactionService) CreateTransaction(t *api.Transaction) error {
	t.New()
	return nil
}

// ListProfessionalTransactions is
func (s *TransactionService) ListProfessionalTransactions(id string, fromDate int64) ([]*api.Transaction, error) {
	var t []*api.Transaction
	return t, nil
}

// GetTransactionByID is
func (s *TransactionService) GetTransactionByID(id string) (*api.Transaction, error) {
	if id != "correctid" {
		return nil, api.ErrNotFound
	}
	t := new(api.Transaction)
	return t, nil
}

// RefundTransaction is
func (s *TransactionService) RefundTransaction(id string) error {
	_, err := s.GetTransactionByID(id)
	if err != nil {
		return err
	}

	return nil
}
