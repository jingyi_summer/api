package mock

import (
	"time"

	"gopkg.in/mgo.v2/bson"

	uuid "github.com/satori/go.uuid"
	"gitlab.com/vanoapp/api"
)

var testProfessionalID = "correctid"
var testServiceID = "correctid"

// ProfessionalService is
type ProfessionalService struct {
}

// CreateProfessional is
func (s *ProfessionalService) CreateProfessional(professional *api.Professional) error {
	professional.ID = uuid.NewV4().String()
	professional.CreatedAt = time.Now()
	professional.UpdatedAt = time.Now()

	return nil
}

// AddService is
func (s *ProfessionalService) AddService(professionalID, serviceID string) error {
	switch {
	case serviceID != testServiceID:
		return api.ErrNotFound
	case professionalID != testProfessionalID:
		return api.ErrNotFound
	}
	return nil
}

// RemoveService is
func (s *ProfessionalService) RemoveService(professionalID, serviceID string) error {
	switch {
	case serviceID != testServiceID:
		return api.ErrNotFound
	case professionalID != testProfessionalID:
		return api.ErrNotFound
	}
	return nil
}

// ListProfessionals is
func (s *ProfessionalService) ListProfessionals(limit int) ([]*api.Professional, error) {
	return nil, nil
}

// GetProfessional is
func (s *ProfessionalService) GetProfessional(id string) (*api.Professional, error) {
	if id != testProfessionalID {
		return nil, api.ErrNotFound
	}

	professional := api.Professional{
		CompanyID:   "asass",
		FirstName:   "test",
		LastName:    "more test",
		Email:       "wazaa",
		PhoneNumber: "nop",
		Password:    "test",
	}

	return &professional, nil
}

// GetProfessionalQuery is
func (s *ProfessionalService) GetProfessionalQuery(query string, params ...interface{}) (*api.Professional, error) {
	professional := api.Professional{
		CompanyID:   "asass",
		FirstName:   "test",
		LastName:    "more test",
		Email:       "wazaa",
		PhoneNumber: "nop",
		Password:    "test",
	}

	return &professional, nil
}

// UpdateProfessional is
func (s *ProfessionalService) UpdateProfessional(us *api.UpdateProfessional) (*api.Professional, error) {
	if us.ID != testProfessionalID {
		return nil, api.ErrNotFound
	}

	professional := api.Professional{
		CompanyID:   "asass",
		FirstName:   "test",
		LastName:    "more test",
		Email:       "wazaa",
		PhoneNumber: "nop",
		Password:    "test",
	}

	return &professional, nil
}

// ListCompanyProfessionals is
func (s *ProfessionalService) ListCompanyProfessionals(companyID string) ([]*api.Professional, error) {
	if companyID != testCompanyID {
		return nil, api.ErrNotFound
	}
	return nil, nil
}

// ResetPassword is
func (s *ProfessionalService) ResetPassword(id, password, newPassword string) error {
	if id != testProfessionalID {
		return api.ErrNotFound
	}
	return nil
}

// Onboard is
func (s *ProfessionalService) Onboard(id string) error {
	if id != testProfessionalID {
		return api.ErrNotFound
	}
	return nil
}

// CheckProfessionalExists is
func (s *ProfessionalService) CheckProfessionalExists(email, phoneNumber string) (*api.Professional, error) {
	if email != "correctemail" && phoneNumber != "correctnumber" {
		return nil, api.ErrNotFound
	}
	var professional api.Professional

	return &professional, nil
}

// UpdateAuth is
func (s *ProfessionalService) UpdateAuth(id, ipAddress string, lastLogin time.Time) error {
	return nil
}

// ActivateProfessional is
func (s *ProfessionalService) ActivateProfessional(id, password string) error {
	if id != testProfessionalID {
		return api.ErrNotFound
	}
	return nil
}

//AddManagedProfessional is
func (s *ProfessionalService) AddManagedProfessional(m *api.ManagedProfessional) (*api.ManagedProfessional, error) {
	me := &api.ManagedProfessional{
		ID:         uuid.NewV4().String(),
		Permission: m.Permission,
		Managee:    m.Managee,
		Manager:    m.Manager,
		CreatedAt:  time.Now(),
		UpdatedAt:  time.Now(),
	}

	return me, nil
}

//UpdateManagedProfessional is
func (s *ProfessionalService) UpdateManagedProfessional(m *api.ManagedProfessional) (*api.ManagedProfessional, error) {
	me := &api.ManagedProfessional{
		ID:         uuid.NewV4().String(),
		Permission: m.Permission,
		Managee:    m.Managee,
		Manager:    m.Manager,
		CreatedAt:  time.Now(),
		UpdatedAt:  time.Now(),
	}

	return me, nil
}

// DeleteProfessional is
func (s *ProfessionalService) DeleteProfessional(id string) error {
	if id != "correctid" {
		return api.ErrNotFound
	}
	return nil
}

//ListProfessionalManagedProfessionals is
func (s *ProfessionalService) ListProfessionalManagedProfessionals(spID, status string) ([]*api.ManagedProfessional, error) {
	var sps []*api.ManagedProfessional

	return sps, nil
}

// ListProfessionalManageeInvitations is
func (s *ProfessionalService) ListProfessionalManageeInvitations(spID, status string) ([]*api.ManagedProfessional, error) {
	var sps []*api.ManagedProfessional
	return sps, nil
}

// RemoveManagedProfessional is
func (s *ProfessionalService) RemoveManagedProfessional(id string) error {
	return nil
}

//AddForgotPasswordRequest is
func (s *ProfessionalService) AddForgotPasswordRequest(id string) (*api.ForgotPasswordRequestTable, error) {
	req := new(api.ForgotPasswordRequestTable)
	req.ProfessionalID = id
	req.CreatedAt = time.Now()
	req.UpdatedAt = time.Now()
	req.TextHash = bson.NewObjectId().Hex()
	req.Hash = req.TextHash

	return req, nil
}

//NoAuthResetRequest is
func (s *ProfessionalService) NoAuthResetRequest(plainHash, newPassword string) error {
	return nil
}

// ClocIn is
func (s *ProfessionalService) ClocIn(professionalID string) (*api.ClocIn, error) {
	newClocIn := &api.ClocIn{
		ID:             uuid.NewV4().String(),
		ProfessionalID: professionalID,
		ClocInTime:     time.Now(),
		CreatedAt:      time.Now(),
		UpdatedAt:      time.Now(),
	}

	return newClocIn, nil
}

// ClocOut is
func (s *ProfessionalService) ClocOut(clocinID string) error {
	return nil
}

//ListProfessionalClocIns is
func (s *ProfessionalService) ListProfessionalClocIns(id string) ([]*api.ClocIn, error) {
	var c []*api.ClocIn

	return c, nil
}

// UpdateProfessionalFeature is
func (s *ProfessionalService) UpdateProfessionalFeature(id, featureID string, enabled bool) error {
	return nil
}
