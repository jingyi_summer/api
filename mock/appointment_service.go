package mock

import (
	"time"

	uuid "github.com/satori/go.uuid"
	"gitlab.com/vanoapp/api"
)

// AppointmentService is
type AppointmentService struct {
}

// CreateAppointment is
func (s *AppointmentService) CreateAppointment(a *api.Appointment) error {
	a.New()

	return nil
}

// GetAppointment is
func (s *AppointmentService) GetAppointment(id string) (*api.Appointment, error) {
	var a api.Appointment

	return &a, nil
}

// ListProfessionalAppointments is
func (s *AppointmentService) ListProfessionalAppointments(professionalID string, startTime, endTime int64, limit int) ([]*api.Appointment, error) {
	var appointments []*api.Appointment

	return appointments, nil
}

// ListCustomerAppointments is
func (s *AppointmentService) ListCustomerAppointments(professionalID string, startTime, endTime int64, limit int) ([]*api.Appointment, error) {
	var appointments []*api.Appointment

	return appointments, nil
}

// UpdateAppointment is
func (s *AppointmentService) UpdateAppointment(a *api.Appointment) error {
	return nil
}

// CancelAppointment is
func (s *AppointmentService) CancelAppointment(id string) error {
	return nil
}

// CheckAppointmentIsAvailable is
func (s *AppointmentService) CheckAppointmentIsAvailable(profID string, startTime, endTime int64) error {
	return nil
}

// ListProfessionalAvaibaleAppointmentTimes is
func (s *AppointmentService) ListProfessionalAvaibaleAppointmentTimes(professionalID string, serviceLengths, date int64) ([]string, error) {
	return []string{}, nil
}

// CreatePostAppointment is
func (s *AppointmentService) CreatePostAppointment(p *api.PostAppointment) (*api.PostAppointment, error) {
	newP := p
	newP.ID = uuid.NewV4().String()
	newP.CreatedAt = time.Now()
	newP.UpdatedAt = time.Now()

	return newP, nil
}

// UpdatePostAppointment is
func (s *AppointmentService) UpdatePostAppointment(p *api.PostAppointment) (*api.PostAppointment, error) {
	newP := p
	newP.UpdatedAt = time.Now()

	return newP, nil
}

// DeletePostAppointment is
func (s *AppointmentService) DeletePostAppointment(id string) error {
	return nil
}

// ListProfessionalBlockoffs is
func (s *AppointmentService) ListProfessionalBlockoffs(id string) ([]*api.Appointment, error) {
	var a []*api.Appointment
	return a, nil
}
