package mock

import "gitlab.com/vanoapp/api"

// MerchantService is
type MerchantService struct {
}

// CreateMerchant is
func (s *MerchantService) CreateMerchant(m *api.Merchant) error {
	m.New()

	return nil
}

// GetCompanyMerchant is
func (s *MerchantService) GetCompanyMerchant(companyID string) (*api.Merchant, error) {
	m := new(api.Merchant)

	return m, nil
}
