package api

import (
	"errors"
	"time"

	uuid "github.com/satori/go.uuid"
)

// AppointmentService is
type AppointmentService interface {
	CreateAppointment(a *Appointment) error
	GetAppointment(id string) (*Appointment, error)
	CancelAppointment(id string) error
	ListProfessionalAppointments(professionalID string, startTime, endTime int64, limit int) ([]*Appointment, error)
	ListCustomerAppointments(customerID string, startTime, endTime int64, limit int) ([]*Appointment, error)
	UpdateAppointment(a *Appointment) error
	CheckAppointmentIsAvailable(profID string, startTime, endTime int64) error
	ListProfessionalAvaibaleAppointmentTimes(professionalID string, serviceLength, date int64) ([]string, error)
	CreatePostAppointment(p *PostAppointment) (*PostAppointment, error)
	DeletePostAppointment(id string) error
	ListProfessionalBlockoffs(id string) ([]*Appointment, error)
	UpdatePostAppointment(p *PostAppointment) (*PostAppointment, error)
}

// Appointment is
type Appointment struct {
	ID                   string           `json:"id"`
	CustomerID           string           `json:"customer_id"`
	ProfessionalID       string           `json:"professional_id"`
	Services             []*Service       `json:"services" pg:",many2many:appointment_service"`
	StartTime            int64            `json:"start_time"`
	EndTime              int64            `json:"end_time"`
	Notes                string           `json:"notes"`
	Cancelled            bool             `json:"cancelled" sql:",notnull"`
	NoShow               bool             `json:"no_show" sql:",notnull"`
	NotifiedCustomer     bool             `json:"notified_customer" sql:",notnull"`
	NotifiedProfessional bool             `json:"notified_professional" sql:",notnull"`
	PostAppointment      *PostAppointment `json:"post_appointment"`
	IsBlockoff           bool             `json:"is_blockoff" sql:",notnull"`
	CreatedAt            time.Time        `json:"created_at"`
	UpdatedAt            time.Time        `json:"updated_at"`
}

// New is
func (a *Appointment) New() {
	a.ID = uuid.NewV4().String()
	a.CreatedAt = time.Now()
	a.UpdatedAt = time.Now()
}

// ValidateUpdate is
func (a *Appointment) ValidateUpdate() error {
	switch {
	case a.ID == "":
		return errors.New("id is required")
	case a.ProfessionalID == "":
		return errors.New("stylist is required")
	case a.CustomerID == "":
		return errors.New("client is required")
	case a.StartTime == 0:
		return errors.New("start time is required")
	case a.EndTime == 0:
		return errors.New("end time is required")
	case a.Services == nil:
		return errors.New("service is required")
	}

	return nil
}

// ServiceAppointment is
type ServiceAppointment struct {
	TableName      struct{} `sql:"appointment_service" json:"-"`
	ID             string
	AppointmentID  string
	ServiceID      string
	Length         int
	ProfessionalID string `json:"professional_id"`
	StartTime      int64  `json:"start_time"`
	EndTime        int64  `json:"end_time"`
	IsGap          bool   `json:"is_gap" sql:",notnull"`
	IsCancelled    bool   `json:"is_cancelled" sql:",notnull"`
}

//PostAppointment is
type PostAppointment struct {
	ID                 string    `json:"id"`
	AppointmentID      string    `json:"appointment_id"`
	ColorFormula       string    `json:"color_formula"`
	HighlightFormula   string    `json:"highlight_formula"`
	TotalServiceLength string    `json:"total_service_length"`
	Notes              string    `json:"notes"`
	CreatedAt          time.Time `json:"-"`
	UpdatedAt          time.Time `json:"-"`
}
