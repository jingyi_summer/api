package jwt

import (
	"fmt"
	"log"
	"strings"
	"time"

	jwt "github.com/dgrijalva/jwt-go"

	"os"

	"gitlab.com/vanoapp/api"
	"golang.org/x/crypto/bcrypt"
)

// AuthService is
type AuthService struct {
	CustomerService     api.CustomerService
	ProfessionalService api.ProfessionalService
	NotificationService api.NotificationService
}

// Authenticate is
func (s *AuthService) Authenticate(c *api.Credentials) (api.JWTToken, error) {
	if c.Role == "customer" {
		return s.authenticateCustomer(c)
	}

	return s.authenticateProfessional(c)
}

// RefreshToken is
func (s *AuthService) RefreshToken(hashedToken string) (api.JWTToken, error) {
	// Decode jwt token
	token, err := jwt.Parse(hashedToken, func(token *jwt.Token) (interface{}, error) {
		// Valid alg is what we expect
		if token.Method != jwt.SigningMethodHS256 {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(os.Getenv("JWT_TOKEN")), nil
	})
	if err != nil {
		return "", err
	}

	if !token.Valid {
		return "", api.ErrInvalidJWT
	}

	claims, ok := token.Claims.(jwt.MapClaims)
	if !ok {
		return "", api.ErrInvalidJWT
	}

	jwtToken, err := api.GenerateJWT(claims["sub"].(string), claims["role"].(string))
	if err != nil {
		return "", err
	}

	return jwtToken, nil
}

// ForgotPassword is
func (s *AuthService) ForgotPassword(authenticator, role string) error {
	if role == "professional" {
		return s.forgotProfessionalPassword(authenticator)
	}

	return s.forgotCustomerPassword(authenticator)
}

// ResetPassword is
func (s *AuthService) ResetPassword(plainHash, newPassword, role string) error {
	if role == "professional" {
		return s.resetProfessionalPassword(plainHash, newPassword)
	}
	return s.resetCustomerPassword(plainHash, newPassword)
}

func (s *AuthService) authenticateCustomer(c *api.Credentials) (api.JWTToken, error) {
	customer, err := s.CustomerService.GetCustomerQuery(`SELECT * FROM customers WHERE email = ? OR phone_number = ?`, strings.ToLower(c.Email), c.PhoneNumber)
	if err != nil {
		return "", err
	}

	if err := bcrypt.CompareHashAndPassword([]byte(customer.Password), []byte(c.Password)); err != nil {
		return "", api.ErrNotFound
	}

	token, err := api.GenerateJWT(customer.ID, "customer")
	if err != nil {
		return "", err
	}

	go func() {
		customer.LastLogin = time.Now()
		customer.IPAddress = c.IPAddress
		if err := s.CustomerService.UpdateCustomer(customer); err != nil {
			log.Println(err)
		}
	}()

	return token, nil
}

func (s *AuthService) authenticateProfessional(c *api.Credentials) (api.JWTToken, error) {
	professional, err := s.ProfessionalService.GetProfessionalQuery(`SELECT * FROM professionals WHERE email = ? OR phone_number = ?`, strings.ToLower(c.Email), c.PhoneNumber)
	if err != nil {
		return "", err
	}

	log.Println("professional picked for auth", professional)

	if err := bcrypt.CompareHashAndPassword([]byte(professional.Password), []byte(c.Password)); err != nil {
		return "", api.ErrNotFound
	}

	token, err := api.GenerateJWT(professional.ID, "professional")
	if err != nil {
		return "", err
	}

	go func() {
		if err := s.ProfessionalService.UpdateAuth(professional.ID, c.IPAddress, time.Now()); err != nil {
			log.Println(err)
		}
	}()

	return token, nil
}

func (s *AuthService) forgotProfessionalPassword(authenticator string) error {
	p, err := s.ProfessionalService.CheckProfessionalExists(authenticator, authenticator)
	if err != nil {
		return err
	}

	req, err := s.ProfessionalService.AddForgotPasswordRequest(p.ID)
	if err != nil {
		return err
	}

	go func(prof *api.Professional, r *api.ForgotPasswordRequestTable) {
		if err := s.NotificationService.NotifyProfessionalResetPassword(p, req); err != nil {
			log.Println("error sending sp forgot password email", err)
		}
	}(p, req)

	return nil
}

func (s *AuthService) resetProfessionalPassword(plainHash, password string) error {
	if err := s.ProfessionalService.NoAuthResetRequest(plainHash, password); err != nil {
		return err
	}

	return nil
}

func (s *AuthService) forgotCustomerPassword(authenticator string) error {
	c, err := s.CustomerService.CheckCustomerExists(authenticator, authenticator)
	if err != nil {
		return err
	}

	req, err := s.CustomerService.AddForgotPasswordRequest(c.ID)
	if err != nil {
		return err
	}

	go func(cust *api.Customer, r *api.ForgotPasswordRequestTable) {
		if err := s.NotificationService.NotifyCustomerResetPassword(cust, req); err != nil {
			log.Println("error sending sp forgot password email", err)
		}
	}(c, req)

	return nil
}

func (s *AuthService) resetCustomerPassword(plainHash, password string) error {
	if err := s.CustomerService.NoAuthResetRequest(plainHash, password); err != nil {
		return err
	}

	return nil
}
