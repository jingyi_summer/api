package api

import (
	"os"
	"testing"
)

var testCustomer *Customer

func TestMain(m *testing.M) {
	testCustomer = &Customer{
		FirstName: "test",
		LastName:  "test",
	}

	t := m.Run()

	os.Exit(t)
}
